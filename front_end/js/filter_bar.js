/**
 * JS for the filter bar - used on the sidebar for the search + filter page
 */
/* define $ as jQuery just in case */
( function( $ ){

	/* doc ready */
	$( function( ){
		
		/* init the toggle box function */
		toggle_box( 300 );
		
		/* toggle / accordian events */
		function toggle_box( speed ) {
		
			/* set the default speed var */
			var speed = ( ! speed ) ? 300 : speed;
			
			/** 
			 * slide up all of the toggle targets and open the ones with the parent toggle box set to open - very fast so as to not be visible 
			 * - we do this to eliminate the hitch in the animation by animating up first instead of using "display:none;"
			 */
			$( '.toggle_box' ).hide( );
			$( '.toggle_box .toggle_target, .mini_toggle .mini_target' ).slideUp( 0 );
			$( '.toggle_box.open .toggle_target, .mini_toggle.open .mini_target' ).slideDown( 0, function( ) { 
				$( '.toggle_box' ).show( );
			});
			
			/* toggle control click action */
			$( '.toggle_box' ).on( 'click', '.toggle_control', function( e ) {
				var parent_el 	= $( this ).closest( '.toggle_box' );
				var target 		= parent_el.find( '.toggle_target' );
				
				if ( parent_el.hasClass( 'open' ) )
				{
					parent_el.removeClass( 'open' );
					target.slideUp( speed );
				} 
				else
				{
					parent_el.addClass( 'open' );
					target.slideDown( speed );
				}
				e.preventDefault( );
			});
			
			/* mini toggle control click action */
			$( '.mini_toggle' ).on( 'click', '.mini_control', function( e ) {
				var parent_el 	= $( this ).closest( '.mini_toggle' );
				var target 		= parent_el.find( '.mini_target' );
				
				if ( parent_el.hasClass( 'open' ) )
				{
					parent_el.removeClass( 'open' );
					target.slideUp( speed );
				} 
				else
				{
					parent_el.addClass( 'open' );
					target.slideDown( speed );
				}
				e.preventDefault( );
			});
		}
		
	});
	
})( jQuery );