<?php 
/**
 * NOTE: do not convert to PHP open_short_tags - <?= ?> 
 * - use the full "<?php echo" syntax instead 
 * - the full syntax is best practices when working with distributed code
 * - plus some packages require open_short_tags to be turned OFF (e.g. Symfony)
 */

/* set the page vars */
$body_class = 'product category';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' );?>
	
<div id="content">

	<div class="fixed_wrap white_box clearfix">
	
		<div id="sidebar" class="pull-left">
		
			<?php include_once( 'partials/filter_bar.php' ); ?>
			
		</div>
		<div id="main" class="pull-right">
			<h1 class="headline">
				Foil and Film Tapes
			</h1>
			
			<div class="blue_bar">&nbsp;</div>
			
			<div id="marquee" class="clearfix">
				<div class="marquee_content pull-left">
					<span class="highlight">If you need an airtight, waterproof seal</span>, we have the tape for you. We provide a 
					full line of aluminum foil, composite, and film tapes for HVAC, construction, and electrical applications.
				</div>
				<div class="marquee_image pull-right">
					<img src="images/category_foil_tape.png" alt="" />
				</div>
			</div>
			
			<div id="featured_videos">
				<h3 class="intro pull-left">Featured Videos:</h3>
				<ul class="list-inline">
					<li><a href="#"><img src="images/fpo_featured_video.jpg" alt="" /></a></li>
					<li><a href="#"><img src="images/fpo_featured_video.jpg" alt="" /></a></li>
					<li><a href="#"><img src="images/fpo_featured_video.jpg" alt="" /></a></li>
				</ul>
			</div>
			
			<div id="product_grid" class="product_grid">
			
				<h3>Foil and Film Tape Products:</h3>
				
				<ul>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Foil &amp; Paper Composite</b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
	
</div>

<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
