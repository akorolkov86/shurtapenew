<?php 
/**
 * NOTE: do not convert to PHP open_short_tags - <?= ?> 
 * - use the full "<?php echo" syntax instead 
 * - the full syntax is best practices when working with distributed code
 * - plus some packages require open_short_tags to be turned OFF (e.g. Symfony)
 */

/* set the page vars */
$body_class = 'product search';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' );?>
	
<div id="content">

	<div class="fixed_wrap white_box clearfix">
	
		<div id="sidebar" class="pull-left">
		
			<?php include_once( 'partials/filter_bar.php' ); ?>
			<link rel="stylesheet" type="text/css" href="js/extjs/resources/css/ext-all.css">
			<script type="text/javascript" charset="utf-8" src="http://cdn.sencha.io/ext-4.2.0-gpl/ext-all.js"></script>
			<script type="text/javascript" charset="utf-8" src="js/extjs/app/shurtape/app.js"></script>
		</div>
		<div id="main" class="pull-right">
			<h1 class="headline">
				Product Results for "Foil Tape"
			</h1>
			
			<div class="blue_bar">
				<ul class="filter_tags">
					<li><a href="#" class="control_remove" data-value="123">Foil Tape</a></li>
					<li><a href="#" class="control_remove" data-value="123">Blue</a></li>
					<li><a href="#" class="control_remove" data-value="123">48mm x 55m</a></li>
				</ul>
			</div>
			
			<div id="top_filter_nav">
				<div class="clearfix">
					<ul class="tab_controls pull-left">
						<li><a href="#" class="tab_control active">Normal View</a></li>
						<li><a href="#" class="tab_control">Technical View</a></li>
					</ul>
					<div class="results_nav pull-right">
						<h4>Results per page</h4>
						<select class="form-control" name="per_page">
							<option value="12">12</option>
							<option value="24">24</option>
							<option value="36">30</option>
							<option value="36">60</option>
						</select>
					</div>
				</div>
				<div class="clearfix">
					<a href="#" class="compare_selected btn btn-orange btn-squared pull-left">COMPARE SELECTED PRODUCTS</a>
					<ul class="page_nav pull-right">
						<li><a href="#" class="page_num active" data-value="1">1</a></li>
						<li><a href="#" class="page_num" data-value="2">2</a></li>
						<li><a href="#" class="page_num" data-value="3">3</a></li>
						<li><a href="#" class="page_num" data-value="4">4</a></li>
						<li><a href="#" class="page_num" data-value="5">5</a></li>
						<li><a href="#" class="page_num" data-value="6">6</a></li>
						<li><a href="#" class="page_num" data-value="more">...</a></li>
						<li><a href="#" class="page_num" data-value="22">22</a></li>
						<li><a href="#" class="page_num" data-value="next">Next</a></li>
					</ul>
				</div>
			</div>
			<div id="product_grid" class="clearfix"></div>
			
			<div id="bottom_filter_nav">
				<div class="clearfix">
					<a href="#" class="compare_selected btn btn-orange btn-squared pull-left">COMPARE SELECTED PRODUCTS</a>
					<ul class="page_nav pull-right">
						<li><a href="#" class="page_num active" data-value="1">1</a></li>
						<li><a href="#" class="page_num" data-value="2">2</a></li>
						<li><a href="#" class="page_num" data-value="3">3</a></li>
						<li><a href="#" class="page_num" data-value="4">4</a></li>
						<li><a href="#" class="page_num" data-value="5">5</a></li>
						<li><a href="#" class="page_num" data-value="6">6</a></li>
						<li><a href="#" class="page_num" data-value="more">...</a></li>
						<li><a href="#" class="page_num" data-value="22">22</a></li>
						<li><a href="#" class="page_num" data-value="next">Next</a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</div>
	
</div>

<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
