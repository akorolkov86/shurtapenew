<?php 
/**
 * NOTE: do not convert to PHP open_short_tags - <?= ?> 
 * - use the full "<?php echo" syntax instead 
 * - the full syntax is best practices when working with distributed code
 * - plus some packages require open_short_tags to be turned OFF (e.g. Symfony)
 */

/* set the page vars */
$body_class = 'product detail';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' );?>
	
<div id="content">

	<div class="fixed_wrap white_box clearfix">
	
		<div id="main" class="clearfix">
			<h1 class="headline">
				AF 100 2 mil aluminum foil
			</h1>
			
			<div class="blue_bar">
				<div class="breadcrumbs">
					<a href="#">Home</a> &rsaquo; <a href="#">Browse by Type</a> &rsaquo; <a href="#">Foil &amp; Film Tapes</a> &rsaquo; <a href="#">Aluminum Foil Tapes</a>
				</div>
			</div>
			
			<div id="detail_content" class="row">
				
				<div class="detail_media col-md-6">
					<div id="thumb_slider" class="thumb_slider">
						<div class="slides">
							<div class="slide"><img src="images/fpo_detail_product.jpg" alt="" /></div>
							<div class="slide active"><img src="images/fpo_howto_thumb.jpg" alt="" /></div>
							<div class="slide"><img src="images/fpo_howto_thumb2.jpg" alt="" /></div>
							<div class="slide"><img src="images/fpo_howto_thumb3.jpg" alt="" /></div>
						</div>
						<ul class="controls">
							<li class="control"><a href="#"><img src="images/fpo_detail_product.jpg" alt="" /></a></li>
							<li class="control active"><a href="#"><img src="images/fpo_howto_thumb.jpg" alt="" /></a></li>
							<li class="control"><a href="#"><img src="images/fpo_howto_thumb2.jpg" alt="" /></a></li>
							<li class="control"><a href="#"><img src="images/fpo_howto_thumb3.jpg" alt="" /></a></li>
						</ul>
					</div>
				</div>
				<div class="detail_content col-md-6">
					<div class="detail_badge"><img src="images/shurtape_green_point.jpg" alt="" /></div>
					<div class="detail_title">AF 100</div>
					<div class="detail_subtitle">2 mil aluminum foil</div>
					<div class="detail_meta">
						Linered<br />
						UL 181A-P/B-FX Listed; FSI 25; SDI 50<br />
						Green Point Contributor Product
					</div>
					<div class="detail_desc">
						This 2 mil aluminum UL 181 foil tape has an acrylic adhesive used in the HVAC industry for joining and 
						sealing fiberglass ductboard and Class 1 Flex Duct. Provides an air-tight seal.
					</div>
					<div class="cta_box">
						<a href="#" class="btn btn-orange">Contact Us To Order</a>
						<a href="#" class="btn">Request a Sample</a>
						<a href="#" class="btn">Use the Product Locator</a>
					</div>
				</div>
			</div>
			
			<div id="tab_section" class="tab_group clearfix">
				<ul class="tab_controls pull-left">
					<li><a href="#" class="tab_control active" rel="market_tab">Markets/Applications</a></li>
					<li><a href="#" class="tab_control" rel="size_color_tab">Sizes &amp; Colors</a></li>
					<li><a href="#" class="tab_control" rel="physical_tab">Physical Properties</a></li>
					<li><a href="#" class="tab_control" rel="downloads_tab">Downloads</a></li>
					<li><a href="#" class="tab_control" rel="videos_tab">Videos</a></li>
					<li><a href="#" class="tab_control" rel="testimonials_tab">Testimonials</a></li>
				</ul>
				<div class="related_products">
					<h3>Related products</h3>
					<ul>
						<li><a href="#">AF 914CT</a></li>
						<li><a href="#">AF 975CT</a></li>
						<li><a href="#">AF 075</a></li>
					</ul>
				</div>
				<div class="tab_targets">
					<div id="market_tab" class="tab_target inline_grid">
						<div class="grid_group clearfix">
							<div class="grid_label text-right">
								<h4>MARKETS</h4>
							</div>
							<div class="grid_content">
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_market_hvac.png" alt="" />
									<span class="title">HVAC</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_market_construction.png" alt="" />
									<span class="title">Construction</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_market_industrial.png" alt="" />
									<span class="title">Industrial</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_market_hvac.png" alt="" />
									<span class="title">HVAC</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_market_construction.png" alt="" />
									<span class="title">Construction</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_market_industrial.png" alt="" />
									<span class="title">Industrial</span>
								</a>
							</div>
						</div>
						<div class="grid_item clearfix">
							<div class="grid_label text-right">
								<h4>TYPICAL APPLICATIONS</h4>
							</div>
							<div class="grid_content ">
								UL 181A-P and UL 181B-FX requirements<br />
								Join and seam fiberglass ductboard systems<br />
								Join and seal Class 1 Flex Duct
							</div>
						</div>
						
					</div>
					
					<div id="size_color_tab" class="tab_target">
						<h3>Sizes and Colors Available</h3>
						
						<table class="table">
							<thead>
								<tr>
									<th>Dimensions</th>
									<th class="text-center">Rolls Per Case</th>
									<th class="text-center">Weight lbs/case</th>
									<th class="text-center">Cases Per Pallet</th>
									<th>Available Colors</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>48mm x 100m</td>
									<td class="text-center">36</td>
									<td class="text-center">18.6</td>
									<td class="text-center">60</td>
									<td>
										<span class="color_item clr">
											<span class="box"></span>
											<span class="tag">CLR</span>
										</span>
										<span class="color_item tan">
											<span class="box"></span>
											<span class="tag">TAN</span>
										</span>
										<span class="color_item blk">
											<span class="box"></span>
											<span class="tag">BLK</span>
										</span>
										<span class="color_item blu">
											<span class="box"></span>
											<span class="tag">BLU</span>
										</span>
										<span class="color_item grn">
											<span class="box"></span>
											<span class="tag">GRN</span>
										</span>
										<span class="color_item org">
											<span class="box"></span>
											<span class="tag">ORG</span>
										</span>
										<span class="color_item yel">
											<span class="box"></span>
											<span class="tag">YEL</span>
										</span>
										<span class="color_item red">
											<span class="box"></span>
											<span class="tag">RED</span>
										</span>
									</td>
								</tr>
								<tr>
									<td>48mm x 100m</td>
									<td class="text-center">36</td>
									<td class="text-center">18.6</td>
									<td class="text-center">60</td>
									<td>
										<span class="color_item clr">
											<span class="box"></span>
											<span class="tag">CLR</span>
										</span>
										<span class="color_item tan">
											<span class="box"></span>
											<span class="tag">TAN</span>
										</span>
										<span class="color_item blk">
											<span class="box"></span>
											<span class="tag">BLK</span>
										</span>
										<span class="color_item blu">
											<span class="box"></span>
											<span class="tag">BLU</span>
										</span>
										<span class="color_item grn">
											<span class="box"></span>
											<span class="tag">GRN</span>
										</span>
										<span class="color_item org">
											<span class="box"></span>
											<span class="tag">ORG</span>
										</span>
										<span class="color_item yel">
											<span class="box"></span>
											<span class="tag">YEL</span>
										</span>
										<span class="color_item red">
											<span class="box"></span>
											<span class="tag">RED</span>
										</span>
									</td>
								</tr>
								<tr>
									<td>48mm x 100m</td>
									<td class="text-center">36</td>
									<td class="text-center">18.6</td>
									<td class="text-center">60</td>
									<td>
										<span class="color_item clr">
											<span class="box"></span>
											<span class="tag">CLR</span>
										</span>
										<span class="color_item tan">
											<span class="box"></span>
											<span class="tag">TAN</span>
										</span>
									</td>
								</tr>
								<tr>
									<td>48mm x 100m</td>
									<td class="text-center">36</td>
									<td class="text-center">18.6</td>
									<td class="text-center">60</td>
									<td>
										<span class="color_item clr">
											<span class="box"></span>
											<span class="tag">CLR</span>
										</span>
										<span class="color_item tan">
											<span class="box"></span>
											<span class="tag">TAN</span>
										</span>
									</td>
								</tr>
								<tr>
									<td>48mm x 100m</td>
									<td class="text-center">36</td>
									<td class="text-center">18.6</td>
									<td class="text-center">60</td>
									<td>
										<span class="color_item clr">
											<span class="box"></span>
											<span class="tag">CLR</span>
										</span>
										<span class="color_item tan">
											<span class="box"></span>
											<span class="tag">TAN</span>
										</span>
									</td>
								</tr>
								
							</tbody>
						</table>

					</div>
					
					<div id="physical_tab" class="tab_target">
					
						<h3>Physical Properties</h3>
						
						<table class="table">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th class="text-center">Standard</th>
									<th class="text-center">Metric</th>
									<th class="text-center">ASTM Test Method</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="gray_cell">TENSILE STRENGTH</td>
									<td class="text-center">25 lbs/in width</td>
									<td class="text-center">4.82  N/10 mm</td>
									<td class="text-center">D 3330, D 3330M</td>
								</tr>
								<tr>
									<td class="gray_cell">ADHESION TO STAINLESS STEEL</td>
									<td class="text-center">25 lbs/in width</td>
									<td class="text-center">4.82  N/10 mm</td>
									<td class="text-center">D 3330, D 3330M</td>
								</tr>
								<tr>
									<td class="gray_cell">ELONGATION</td>
									<td class="text-center">25 lbs/in width</td>
									<td class="text-center">4.82  N/10 mm</td>
									<td class="text-center">D 3330, D 3330M</td>
								</tr>
								<tr>
									<td class="gray_cell">APPLICATION TEMPERATURE MIN</td>
									<td class="text-center">25 lbs/in width</td>
									<td class="text-center">4.82  N/10 mm</td>
									<td class="text-center">D 3330, D 3330M</td>
								</tr>
								<tr>
									<td class="gray_cell">APPLICATION TEMPERATURE MAX</td>
									<td class="text-center">25 lbs/in width</td>
									<td class="text-center">4.82  N/10 mm</td>
									<td class="text-center">D 3330, D 3330M</td>
								</tr>
								<tr>
									<td class="gray_cell">THICKNESS - WITH LINER</td>
									<td class="text-center">25 lbs/in width</td>
									<td class="text-center">4.82  N/10 mm</td>
									<td class="text-center">D 3330, D 3330M</td>
								</tr>
								<tr>
									<td class="gray_cell">THICKNESS WITHOUT LINER</td>
									<td class="text-center">25 lbs/in width</td>
									<td class="text-center">4.82  N/10 mm</td>
									<td class="text-center">D 3330, D 3330M</td>
								</tr>
								
							</tbody>
						</table>
						
					</div>
					
					<div id="downloads_tab" class="tab_target inline_grid">
						<div class="grid_group clearfix">
							<div class="grid_label text-right">
								<h4>Downloads</h4>
							</div>
							<div class="grid_content">
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_pdf.png" alt="" />
									<span class="title">Technical Data Sheet</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_pdf.png" alt="" />
									<span class="title">Technical Data Sheet</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_pdf.png" alt="" />
									<span class="title">Technical Data Sheet</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_pdf.png" alt="" />
									<span class="title">Technical Data Sheet</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_pdf.png" alt="" />
									<span class="title">Technical Data Sheet</span>
								</a>
								<a href="#" class="grid_item pull-left">
									<img src="images/icon_pdf.png" alt="" />
									<span class="title">Technical Data Sheet</span>
								</a>
							</div>
						</div>
					</div>
					
					<div id="videos_tab" class="tab_target">
						<ul class="video_thumbs">
							<li>
								<a href="#">
									<img src="images/fpo_detail_video.jpg" alt="" />
									<span class="vid_title">Lorem Ipsum Dolore</span>
								</a>
							</li>
							<li>
								<a href="#">
									<img src="images/fpo_detail_video.jpg" alt="" />
									<span class="vid_title">Lorem Ipsum Dolore</span>
								</a>
							</li>
							<li>
								<a href="#">
									<img src="images/fpo_detail_video.jpg" alt="" />
									<span class="vid_title">Lorem Ipsum Dolore</span>
								</a>
							</li>
						</ul>
					</div>
					
					<div id="testimonials_tab" class="tab_target">
						
					</div>
				</div>
			
			</div>
			
		</div>
	</div>
	
</div>

<?php 

/* include the footer partials */
include_once( 'partials/footer.php' ); ?>
<!-- 
init the plugins here 
- between the footer (containing external JS refs, plugins, and site global footer) and the
- foot (containing body and html closing tags, global site JS like tracking scripts, etc.) 
-->
<script type="text/javascript">
	/* define $ as jQuery just in case */
	( function( $ ){

		/* doc ready */
		$( function( ){
			/* init the thumb slider */
			$( '#thumb_slider' ).thumb_slider( );
			
			/* init the tabs */
			$( '#tab_section' ).tabs( );
		});
	})( jQuery );
</script>
<?php include_once( 'partials/foot.php' ); ?>
