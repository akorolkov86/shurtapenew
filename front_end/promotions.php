<?php 
/**
 * NOTE: do not convert to PHP open_short_tags - <?= ?> 
 * - use the full "<?php echo" syntax instead 
 * - the full syntax is best practices when working with distributed code
 * - plus some packages require open_short_tags to be turned OFF (e.g. Symfony)
 */

/* set the page vars */
$body_class = 'promotions';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' );?>
	
<div id="content">

	<div id="marquee">

		<img src="images/fpo_promotion.png" alt="" />
			
	</div>
	
	<div id="subcontent" class="white_box clearfix">
	
		<div class="headline clearfix">

			UL-Listed HVAC Tapes
			
		</div>
		
		<div id="prev_viewed" class="clearfix">
		
			<a href="#" class="item">
				<span class="item_thumb"><img src="images/fpo_tout_product.jpg" alt="" /></span>
				<span class="item_title">AF 973</span>
				<span class="item_desc">2 mil dead-soft aluminum foil; Linered; Tested in accordance with UL 723</span>
			</a>
			<a href="#" class="item">
				<span class="item_thumb"><img src="images/fpo_tout_product.jpg" alt="" /></span>
				<span class="item_title">AF 973</span>
				<span class="item_desc">2 mil dead-soft aluminum foil; Linered; Tested in accordance with UL 723</span>
			</a>
			<a href="#" class="item">
				<span class="item_thumb"><img src="images/fpo_tout_product.jpg" alt="" /></span>
				<span class="item_title">AF 973</span>
				<span class="item_desc">2 mil dead-soft aluminum foil; Linered; Tested in accordance with UL 723</span>
			</a>
			<a href="#" class="item">
				<span class="item_thumb"><img src="images/fpo_tout_product.jpg" alt="" /></span>
				<span class="item_title">AF 973</span>
				<span class="item_desc">2 mil dead-soft aluminum foil; Linered; Tested in accordance with UL 723</span>
			</a>

		</div>

		<div id="promo_body">
			<h3>To browse our complete HVAC tape portfolio, please visit <a href="www.shurtape.com">www.shurtape.com</a></h3>
			<p>
				*When our tapes are combined with other reduced-VOC adhesives and sealants used throughout 
				a building, that building is eligible to receive credit toward LEED<sup>&reg;</sup> certification.
			</p>
			<p>
				LEED<sup>&reg;</sup> is a registered trademark owned by the U.S. Green Building Council. In commercial 
				buildings the credit is one point; in residential construction, the credit is one-half point.
			</p>
			
			<h4>GET A FREE ROLL</h4>
			
			<form id="promo_form" method="post" action="#" role="form">
	
				<div class="detail_badge"><img src="images/shurtape_green_point.jpg" alt="" /></div>
				
				<div class="form_wrapper">
				
					<div class="all_errors_container"></div>
		
					<div class="row">
						<div class="form-group field_group col-md-6">
							<label class="sr-only" for="promo_name">Name:</label>
							<input type="text" class="form-control" id="promo_name" name="promo_name" placeholder="Name:"
							error_required="NAME ERROR MESSAGE HERE">
						</div>
						<div class="form-group field_group col-md-6">
							<label class="sr-only" for="promo_email">Email</label>
							<input type="email" class="form-control" id="promo_email" name="promo_email" placeholder="Email:"
							error_required="EMAIL ERROR MESSAGE HERE"
							error_email="EMAIL NOT VALID">
						</div>
					</div>
					<div class="row">
						<div class="form-group field_group col-md-6">
							<label class="sr-only" for="promo_address">Address:</label>
							<input type="text" class="form-control" id="promo_address" name="promo_address" placeholder="Address:"
							error_required="ADDRESS ERROR MESSAGE HERE">
						</div>
						<div class="form-group field_group col-md-6">
							<label class="sr-only" for="promo_address2">Address:</label>
							<input type="text" class="form-control" id="promo_address" name="promo_address2" placeholder="Address 2:">
						</div>
					</div>
					<div class="form-inline clearfix">
						<div class="form-group field_group first">
							<label class="sr-only" for="promo_city">City:</label>
							<input type="text" class="form-control" id="promo_city" name="promo_city" placeholder="City:"
							error_required="CITY ERROR MESSAGE HERE">
						</div>
						<div class="form-group field_group middle">
							<label class="sr-only" for="promo_state">State</label>
							<select class="form-control" id="promo_state" name="promo_state" tabindex="-1" 
							error_required="STATE ERROR MESSAGE HERE">
								<option value="">State</option>
								<option value="AL">AL</option>
								<option value="AK">AK</option>
								<option value="AZ">AZ</option>
								<option value="AR">AR</option>
								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DE">DE</option>
								<option value="DC">DC</option>
								<option value="FL">FL</option>
								<option value="GA">GA</option>
								<option value="HI">HI</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>
								<option value="IA">IA</option>
								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="ME">ME</option>
								<option value="MD">MD</option>
								<option value="MA">MA</option>
								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MS">MS</option>
								<option value="MO">MO</option>
								<option value="MT">MT</option>
								<option value="NE">NE</option>
								<option value="NV">NV</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NY">NY</option>
								<option value="NC">NC</option>
								<option value="ND">ND</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PA">PA</option>
								<option value="RI">RI</option>
								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VT">VT</option>
								<option value="VA">VA</option>
								<option value="WA">WA</option>
								<option value="WV">WV</option>
								<option value="WI">WI</option>
								<option value="WY">WY</option>
							</select>
						</div>
						<div class="form-group field_group last">
							<label class="sr-only" for="promo_zip">Zip:</label>
							<input type="text" class="form-control" id="promo_zip" name="promo_zip" placeholder="Zip:"
							error_required="ZIP ERROR MESSAGE HERE">
						</div>
					</div>
					<div class="row">
						<div class="form-group field_group col-md-6">
							<label class="sr-only" for="promo_sample">Select a Tape Sample:</label>
							<select class="form-control" id="promo_sample" name="promo_sample" tabindex="-1"
							error_required="TAPE SAMPLE ERROR MESSAGE HERE">
								<option value="">Select a Tape Sample:</option>
								<option value="123">Sample 1</option>
								<option value="123">Sample 2</option>
							</select>
						</div>
						<div class="form-group field_group col-md-6">
							<label class="sr-only" for="promo_job">Select a Job Function:</label>
							<select class="form-control" id="promo_job" name="promo_job" tabindex="-1"
							error_required="JOB FUNCTION ERROR MESSAGE HERE">
								<option value="">Select a Job Function:</option>
								<option value="123">Job 1</option>
								<option value="123">Job 2</option>
							</select>
						</div>
					</div>
				</div>
				<div class="button_bar">
					<a href="#" class="btn btn-orange btn-wide validate_this_form">Submit<span class="pointer"></span></a>
				</div>

			</form>
			
		</div>
	</div>
	
</div>

<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
