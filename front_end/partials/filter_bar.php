<div id="filter_bar">

	<h2 class="product_results">Product Results <span class="count">(42)</span></h2>
				
	<h3>Narrow Your Search:</h3>

	<div class="toggle_box" id="browse_markets">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Browse by Market</a></h4>
		
		<ul class="toggle_target">
			<li class="mini_toggle" id="browse_foil_film">
				<a href="#" class="mini_control">Foil &amp; Film Tapes</a>
				<ul class="mini_target">
					<li><a href="#">Aluminum Foil Tapes</a></li>	
					<li><a href="#">Foil &amp; Paper Composite Tapes</a></li>	
					<li><a href="#">UL 181B-FX Film Tapes</a></li>	
					<li><a href="#">Polyethylene Film Tapes</a></li>	
					<li><a href="#">PVC Film Tapes</a></li>	
					<li><a href="#">Cold Temperature Foil</a></li>
				</ul>
			</li>
		</ul>
	</div>

	<div class="toggle_box open" id="browse_types">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Browse by Type</a></h4>
		
		<ul class="toggle_target">
			<li class="mini_toggle open" id="browse_foil_film">
				<a href="#" class="mini_control">Foil &amp; Film Tapes</a>
				<ul class="mini_target">
					<li><a href="#">Aluminum Foil Tapes</a></li>	
					<li><a href="#">Foil &amp; Paper Composite Tapes</a></li>	
					<li><a href="#">UL 181B-FX Film Tapes</a></li>	
					<li><a href="#">Polyethylene Film Tapes</a></li>	
					<li><a href="#">PVC Film Tapes</a></li>	
					<li><a href="#">Cold Temperature Foil</a></li>
				</ul>
			</li>
			<li class="mini_toggle" id="browse_more_types">
				<a href="#" class="mini_control">Browse More Types</a>
				<ul class="mini_target">
					<li><a href="#">Lorem Ipsum</a></li>
					<li><a href="#">Lorem Ipsum</a></li>
					<li><a href="#">Lorem Ipsum</a></li>
					<li><a href="#">Lorem Ipsum</a></li>
				</ul>
			</li>
		</ul>
	</div>

	<h3>Filter By Specifications:</h3>

	<div class="toggle_box" id="filter_adhesion_steel">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Adhesion to Steel</a></h4>
		
		<ul class="toggle_target filter_box">
			<li><a href="#" class="filter_control" data-value="red">Red</a></li>	
			<li><a href="#" class="filter_control" data-value="blue">Blue</a></li>	
			<li><a href="#" class="filter_control" data-value="green">Green</a></li>		
		</ul>
	</div>

	<div class="toggle_box" id="filter_tensile">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Tensile</a></h4>
		
		<ul class="toggle_target filter_box">
			<li><a href="#" class="filter_control" data-value="red">Red</a></li>	
			<li><a href="#" class="filter_control" data-value="blue">Blue</a></li>	
			<li><a href="#" class="filter_control" data-value="green">Green</a></li>		
		</ul>
		
	</div>

	<div class="toggle_box" id="filter_adhesive">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Adhesive</a></h4>
		
		<ul class="toggle_target filter_box">
			<li><a href="#" class="filter_control" data-value="red">Red</a></li>	
			<li><a href="#" class="filter_control" data-value="blue">Blue</a></li>	
			<li><a href="#" class="filter_control" data-value="green">Green</a></li>		
		</ul>
	</div>

	<div class="toggle_box" id="filter_backing">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Backing</a></h4>
		
		<ul class="toggle_target filter_box">
			<li><a href="#" class="filter_control" data-value="red">Red</a></li>	
			<li><a href="#" class="filter_control" data-value="blue">Blue</a></li>	
			<li><a href="#" class="filter_control" data-value="green">Green</a></li>		
		</ul>
		
	</div>

	<div class="toggle_box" id="filter_sizes">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Sizes</a></h4>
		
		<ul class="toggle_target filter_box">
			<li><a href="#" class="filter_control" data-value="red">Red</a></li>	
			<li><a href="#" class="filter_control" data-value="blue">Blue</a></li>	
			<li><a href="#" class="filter_control" data-value="green">Green</a></li>		
		</ul>

	</div>

	<div class="toggle_box" id="filter_physical">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Physical Properties</a></h4>
		
		<ul class="toggle_target filter_box">
			<li><a href="#" class="filter_control" data-value="red">Red</a></li>	
			<li><a href="#" class="filter_control" data-value="blue">Blue</a></li>	
			<li><a href="#" class="filter_control" data-value="green">Green</a></li>		
		</ul>

	</div>

	<div class="toggle_box open" id="filter_color">

		<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>Color</a></h4>
		
		<ul class="toggle_target filter_box">
			<li><a href="#" class="filter_control" data-value="red">Red</a></li>	
			<li><a href="#" class="filter_control active" data-value="blue">Blue</a></li>	
			<li><a href="#" class="filter_control" data-value="green">Green</a></li>		
		</ul>

	</div>

	<h2 class="product_results">Other Results <span class="count">(14)</span></h2>
	
</div>