<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>ShurTape - True to Your Work</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fonts.css">
	<link rel="stylesheet" href="css/slideshow.css"><!-- TODO - call this only on homepage -->
	<link rel="stylesheet" href="css/thumb_slider.css"><!-- TODO - call this only where needed (product detail, etc.) -->
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/home.css"><!-- TODO - call this only on homepage -->
	<link rel="stylesheet" href="css/product.css"><!-- TODO - call this only on product category and product detail -->
	<link rel="stylesheet" href="css/search.css"><!-- TODO - call this only on product search -->
	<link rel="stylesheet" href="css/filter_bar.css"><!-- TODO - call this only on pages that have the filter sidebar (i.e. - category and search) -->
	
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
	<![endif]-->
	
</head>