<?php 

/* set the page vars */
$body_class = 'industry_links';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' ); ?>
	
<div id="content">

	<div id="main" class="fixed_wrap white_box clearfix">
		<h1 class="headline">
			Industry Links
		</h1>
		
		<div class="blue_bar">STANDARDS &amp; REGULATIONS</div>

		<ul class="list-unstyled link_list">
			<li>American National Standards Institue - <a href="http://www.ansi.org">www.ansi.org</a></li>
			<li>American Society for Testing and Materials (ASTM) - <a href="http://www.astm.org">www.astm.org</a></li>
			<li>Environmental Protection Agency (EPA) - <a href="http://www.epa.gov">www.epa.gov</a></li> 
			<li>Underwriters Laboratories Inc. - <a href="http://www.ul.com">www.ul.com</a></li>
		</ul>
		
		<div class="blue_bar">TRADE SHOWS &amp; ASSOCIATIONS</div>

		<ul class="list-unstyled link_list">
			<li>Adhesive &amp; Sealants Industry (ASI) - <a href="http://www.adhesivesmag.com">www.adhesivesmag.com</a></li>
			<li>Packaging Machinery Manufacturers Institue - <a href="http://www.pmmi.org">www.pmmi.org</a></li>
			<li>PackExpo.com - <a href="http://www.packexpo.com">www.packexpo.com</a></li>
			<li>Pressure Sensitive Tape Council - <a href="http://www.pstc.org">www.pstc.org</a></li>
  
		</ul>
		
		<div class="blue_bar">FOR DUCT TAPE FANS</div>

		<ul class="list-unstyled link_list">
			<li><a href="http://duckbrand.com/duck-tape-club">The Duck Tape Club Website</a></li>
			<li><a href="http://www.ideafinder.com/history/inventions/ducttape.htm">A Brief History of Duct Tape</a></li>
			<li><a href="http://www.ducttapeguys.com/">Duck Tape Guys.com</a></li>
			<li><a href="http://www.diynetwork.com/diy/hi_duct_tape/">I Did it with Duct Tape</a></li>
			<li><a href="http://ducttapefestival.com/">Avon Duct Tape Festival</a></li>
			<li><a href="http://www.wikihow.com/Make-a-Duct-Tape-Wallet">How to Make a Duct Tape Wallet</a></li>

		</ul>

	</div>

</div>
	
<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
