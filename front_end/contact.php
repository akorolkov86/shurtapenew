<?php 

/* set the page vars */
$body_class = 'contact';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' ); ?>
	
<div id="content">

	<div id="main" class="fixed_wrap white_box clearfix">
	
		<h1 class="headline">
			Contact Us
		</h1>
		
		<div class="blue_bar"></div>
		
		<div class="clearfix">
		
			<div id="left" class="pull-left">
				<img src="images/photo_operator.png" alt="" />
			</div>
			<div id="right" class="pull-right">
			
				<p>
					At Shurtape, we're eager to answer any questions you have about our products or our company. 
					We also love to hear your comments about your Shurtape experience. All feedback is welcome 
					-- we look forward to hearing from you!
				</p>
				
				<form id="contact_form" method="post" action="#" role="form">
				
					<div id="form_box">
	
						<div class="all_errors_container"></div>
						
						<div class="form_wrapper">
							<h2>Send a Message <span class="required">All fields required</span></h2>
							<div class="form-group field_group">
								<label class="sr-only" for="contact_name">Name:</label>
								<input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Name:"
								error_required="NAME ERROR MESSAGE HERE">
							</div>
							<div class="form-group field_group">
								<label class="sr-only" for="contact_email">Email</label>
								<input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="Email:"
								error_required="EMAIL ERROR MESSAGE HERE"
								error_email="EMAIL NOT VALID">
							</div>
							<div class="form-group field_group">
								<label class="sr-only" for="contact_address">Address:</label>
								<input type="text" class="form-control" id="contact_address" name="contact_address" placeholder="Address:"
								error_required="ADDRESS ERROR MESSAGE HERE">
							</div>
							<div class="form-inline clearfix">
								<div class="form-group field_group first">
									<label class="sr-only" for="contact_city">City:</label>
									<input type="text" class="form-control" id="contact_city" name="contact_city" placeholder="City:"
									error_required="CITY ERROR MESSAGE HERE">
								</div>
								<div class="form-group field_group middle">
									<label class="sr-only" for="contact_state">State</label>
									<select class="form-control" id="contact_state" name="contact_state" tabindex="-1">
										<option value="">State</option>
										<option value="AL">AL</option>
										<option value="AK">AK</option>
										<option value="AZ">AZ</option>
										<option value="AR">AR</option>
										<option value="CA">CA</option>
										<option value="CO">CO</option>
										<option value="CT">CT</option>
										<option value="DE">DE</option>
										<option value="DC">DC</option>
										<option value="FL">FL</option>
										<option value="GA">GA</option>
										<option value="HI">HI</option>
										<option value="ID">ID</option>
										<option value="IL">IL</option>
										<option value="IN">IN</option>
										<option value="IA">IA</option>
										<option value="KS">KS</option>
										<option value="KY">KY</option>
										<option value="LA">LA</option>
										<option value="ME">ME</option>
										<option value="MD">MD</option>
										<option value="MA">MA</option>
										<option value="MI">MI</option>
										<option value="MN">MN</option>
										<option value="MS">MS</option>
										<option value="MO">MO</option>
										<option value="MT">MT</option>
										<option value="NE">NE</option>
										<option value="NV">NV</option>
										<option value="NH">NH</option>
										<option value="NJ">NJ</option>
										<option value="NM">NM</option>
										<option value="NY">NY</option>
										<option value="NC">NC</option>
										<option value="ND">ND</option>
										<option value="OH">OH</option>
										<option value="OK">OK</option>
										<option value="OR">OR</option>
										<option value="PA">PA</option>
										<option value="RI">RI</option>
										<option value="SC">SC</option>
										<option value="SD">SD</option>
										<option value="TN">TN</option>
										<option value="TX">TX</option>
										<option value="UT">UT</option>
										<option value="VT">VT</option>
										<option value="VA">VA</option>
										<option value="WA">WA</option>
										<option value="WV">WV</option>
										<option value="WI">WI</option>
										<option value="WY">WY</option>
									</select>
								</div>
								<div class="form-group field_group last">
									<label class="sr-only" for="contact_zip">Zip:</label>
									<input type="text" class="form-control" id="contact_zip" name="contact_zip" placeholder="Zip:"
									error_required="ZIP ERROR MESSAGE HERE">
								</div>
							</div>
							<div class="form-group field_group">
								<label class="sr-only" for="contact_phone">Phone:</label>
								<input type="text" class="form-control" id="contact_phone" name="contact_phone" placeholder="Phone Number:"
								error_required="PHONE ERROR MESSAGE HERE">
							</div>
							<div class="form-group field_group">
								<label class="sr-only" for="contact_sendto">Select Email Address to Contact:</label>
								<select class="form-control" id="contact_sendto" name="contact_sendto" tabindex="-1">
									<option value="">Select Email Address to Contact:</option>
									<option value="custservice@shurtape.com">custservice@shurtape.com</option>
									<option value="custservice@shurtape.com">custservice@shurtape.com</option>
								</select>
							</div>
							<div class="form-group field_group">
								<label class="sr-only" for="contact_query">Enter your query:</label>
								<textarea rows="4" class="form-control" id="contact_query" name="contact_query" placeholder="Enter your query:"
								error_required="QUERY ERROR MESSAGE HERE"></textarea>
							</div>
						</div>
						<div class="button_bar">
							<a href="#" class="btn btn-orange btn-lg btn-wide validate_this_form">Contact Me<span class="pointer"></span></a>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<table class="table">
			<thead>
				<tr>
					<th>Region</th>
					<th colspan="3">Contact</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="region">United States</td>
					<td>
						<b>Customer Service</b><br />
						1-888-442-8273 (TAPE)<br />
						custservice@shurtape.com<br />
						8am - 5pm ET M-F
					</td>
					<td>
						<b>Product Support</b><br />
						productsupport@shurtape.com
					</td>
					<td>
						<b>Media Support</b><br />
						mediasupport@shurtape.com
					</td>
				</tr>
				<tr>
					<td class="region">Canada</td>
					<td>
						<b>Customer Service</b><br />
						1-800-463-8273<br />
						custservice@shurtape.com<br />
						8am - 5pm ET M-F
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td class="region">International</td>
					<td>
						<b>Customer Service</b><br />
						exportcs@shurtape.com
					</td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		
	</div>
</div>
	
<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
