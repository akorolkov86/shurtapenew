<?php 
/**
 * product index
 */

/* set the page vars */
$body_class = 'product index';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' );?>
	
<div id="content">

	<div class="fixed_wrap white_box clearfix">
	
		<div id="sidebar" class="pull-left">
		
			<?php include_once( 'partials/filter_bar.php' ); ?>
			
		</div>
		<div id="main" class="pull-right">
			<h1 class="headline">
				Shurtape Products
			</h1>
			
			<h3 class="blue_bar">BROWSE BY MARKET</h3>
			
			<div class="product_grid">
				<ul>
					<li>
						<a href="#" class="item">
							<img src="images/tout_packing.jpg" alt="Packaging Solutions" />
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Packaging Solutions</b></span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<img src="images/tout_industrial.jpg" alt="Industrial Tapes" />
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Industrial Tapes</b></span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<img src="images/tout_hvac.jpg" alt="HVAC" />
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>HVAC</b></span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<img src="images/tout_arts.jpg" alt="Arts & Entertainment" />
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Arts &amp; Entertainment</b></span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<img src="images/tout_painting.jpg" alt="Professional Paint" />
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Professional Paint</b></span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<img src="images/tout_transportation.jpg" alt="Transportation" />
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Transportation</b></span>
							</span>
						</a>
					</li>
				</ul>
			</div>
			
			<h3 class="blue_bar">BROWSE BY TYPE</h3>
		
			<div class="product_grid">
				<ul>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Foil &amp; Paper Composite</b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
					<li>
						<a href="#" class="item">
							<span class="item_thumb">
								<img src="images/fpo_category_item.jpg" alt="" />
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b>Aluminum Foil </b> Tapes</span>
							</span>
						</a>
					</li>
				</ul>
			</div>	
			
		</div>
	</div>
	
</div>

<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
