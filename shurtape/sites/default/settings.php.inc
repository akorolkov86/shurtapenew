<?php

error_reporting(-1); // reports all errors
ini_set("display_errors", "1"); // shows all errors
ini_set("log_errors", 1);
ini_set("error_log", "/tmp/php-error.log");


$vendorDir = __DIR__ . '/../all/vendor';
require_once $vendorDir.'/autoload.php';

$includePaths = [
  $vendorDir . '/pear/net_url2',
  $vendorDir . '/pear/http_request2',
];
array_push($includePaths, get_include_path());
set_include_path(join(PATH_SEPARATOR, $includePaths));

$conf['file_private_path'] = NULL;