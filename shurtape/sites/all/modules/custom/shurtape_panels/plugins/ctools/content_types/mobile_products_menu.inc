<?php
$plugin = [
  'title' => 'mobile_products_menu',
  'single' => TRUE,
  'render callback' => 'shurtape_panels_mobile_products_menu_render',
  'description' => 'mobile_products_menu',
  'category' => t('Shurtape panes'),
];


function shurtape_panels_mobile_products_menu_render() {
  // @todo need refactoring
$code = <<<'CODE'
?>
  <div class="mobile_toggle_box open" id="mobile_products_menu">
    <h2><a href="#" class="mobile_toggle_control">Products</a></h2>

    <div class="mobile_toggle_target">
      <ul class="sub_menu">
        <div class="mobile_browse_box">
          <?php

          /* market or type page ? */
          $url = parse_url($_SERVER['REQUEST_URI']);
          $url_parts = explode('/', $url['path']);
          $count = count($url_parts);
          $page_slug = strtolower($url_parts[$count - 1]);
          ?>
          <h3 class="blue_bar">Narrow Your Search:</h3>
          <li>
            <span class="blue_corner"></span>
            <ul class="menu">
              <li class="first last expanded<?php print $page_slug == 'markets' ? ' open' : ''; ?>">
                <a href="#" title="Browse by Market">Browse by Market</a>
                <ul class="menu">
                  <?php
                  $get_market_tree = taxonomy_get_tree(7);
                  $browse_market_tree = taxonomy_nested_tree_render($get_market_tree, FALSE, FALSE, 'Markets');
                  print drupal_render($browse_market_tree);
                  ?>
                </ul>
              </li>
              <li class="first last expanded<?php print $page_slug == 'type' ? ' open' : ''; ?>">
                <a href="#" title="Browse by Type">Browse by Type</a>
                <ul class="menu">
                  <?php
                  $get_type_tree = taxonomy_get_tree(9);
                  $browse_type_tree = taxonomy_nested_tree_render($get_type_tree, FALSE, FALSE, 'Tape Type');
                  print drupal_render($browse_type_tree);
                  ?>
                </ul>
              </li>
            </ul>
          </li>
        </div>
        <div class="mobile_filter_box">
          <?php

          /* filter section of navigation - only available on search pages */
          $filters = shurtape_products_nav_generate_filters();
          if (
            !isset($_GET['search_api_views_fulltext']) &&
            arg(1) !== "markets" &&
            isset($filters)
          ): ?>
            <h3 class="blue_bar">Filter By Specification:</h3>
            <li>
              <span class="blue_corner"></span>
              <ul class="menu">
                <?php for ($i = 0; $i < count($filters); $i++):
                  $id = str_replace(" ", "_", strtolower($filters[$i]['content']['#title']));
                  $title = $filters[$i]['content']['#title']; ?>
                  <a href="#" title="<?php print $title; ?>"><?php print $title; ?></a>
                  <div class="toggle_target">
                    <?php print drupal_render($filters[$i]['content']); ?>
                  </div>
                <?php endfor; ?>
              </ul>
            </li>
          <?php endif; ?>
        </div>
      </ul>
    </div>
  </div>
CODE;
  $block = new stdClass();
  ob_start();
  eval($code);
  $block->content['#markup'] = ob_get_clean ();
  return $block;
}