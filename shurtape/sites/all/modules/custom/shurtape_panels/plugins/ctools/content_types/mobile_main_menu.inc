<?php
$plugin = [
  'title' => t('mobile_main_menu'),
  'single' => TRUE,
  'render callback' => 'shurtape_panels_mobile_main_menu_render',
  'description' => t('mobile_main_menu'),
  'category' => t('Shurtape panes'),
];

function shurtape_panels_mobile_main_menu_render() {
  $block = new stdClass();

  $products_menu = menu_tree_output(menu_tree_all_data('menu-products-menu'));
  $resources_menu = menu_tree_output(menu_tree_all_data('menu-resources-menu'));
  $about_menu = menu_tree_output(menu_tree_all_data('menu-about-menu'));
  $contact_menu = menu_tree_output(menu_tree_all_data('menu-contact-menu'));

  $products_menu = drupal_render($products_menu);
  $resources_menu = drupal_render($resources_menu);
  $about_menu = drupal_render($about_menu);
  $contact_menu = drupal_render($contact_menu);

  $base_path = base_path();
  $wtb = t('Where to Buy');
  $home = t('Home');
  $block->content['#markup'] = <<<HTML
<div class="mobile_toggle_box open" id="mobile_main_menu">
    <h2><a href="#" class="mobile_toggle_control">Main Menu</a></h2>
    <div class="mobile_toggle_target">
      <ul class="sub_menu">
        <li><a href="/" title="{$home}">{$home}</a></li>
        <li> {$products_menu}</li>
        <li> {$resources_menu}</li>
        <li> {$about_menu}</li>
        <li> {$contact_menu}</li>
        <li><a href="{$base_path}where-to-buy" title="{$wtb}">{$wtb}</a></li>
      </ul>
    </div>
  </div>
HTML;
  return $block;
}