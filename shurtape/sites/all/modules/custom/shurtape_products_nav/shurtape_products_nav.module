<?php

/**
 * @file
 * Shurtape products categories and subcategories navigation.
 */

define('SHURTAPE_NAV_PATH', drupal_get_path('module', 'shurtape_products_nav'));

// ------------------------------------------------------------ core hooks

/**
 * Implements hook_menu().
 */

function shurtape_products_nav_init() {
    //check if the compare product session needs to be cleared
   //only check in search pages and technical
    if ((count(arg()) == 2 && arg(1) != "shurtape_products_nav_compare_products" && arg(0) == "search" && arg(1) == "products")
         || (arg(0) == "search" && arg(1) == "products" && arg(2) == "technical")   
            ) {
        _shurtape_products_nav_check_product_session_compare_clear();    
    }
}

/**
 * Implements hook_menu().
 */
function shurtape_products_nav_menu(){
  $items = array();

  $items['ajax/shurtape_products_nav_compare_products/%'] = array(
    'title' => '',
    'access arguments' => array('access content'),
    'page callback' => '_shurtape_products_nav_compare_product_session_manager',
    'page arguments' => array(2,3),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function shurtape_products_nav_theme($existing, $type, $theme, $path) {

  return array(
    // Custom
    'products_filter_bar' => array(
      'variables' => array(),
      'template'  => 'products-filter-bar',
      'path'      => SHURTAPE_NAV_PATH . '/theme',
    ),
    'views_view_unformatted__product_count__by_term_id' => array(
      'variables' => array(),
      'template'  => 'views-view-unformatted--product-count--by-term-id',
      'base hook' => 'views_view_unformatted',
      'path'      => SHURTAPE_NAV_PATH . '/theme',
    ),
    'views_view_fields__product_count__by_term_id' => array(
      'variables' => array(),
      'template'  => 'views-view-fields--product-count--by-term-id',
      'base hook' => 'views_view_fields',
      'path'      => SHURTAPE_NAV_PATH . '/theme',
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function shurtape_products_nav_block_info() {

  $blocks['products_categories_nav'] = array(
    'info' => t('Products Categories Navigation'),
    'cache' => DRUPAL_CACHE_GLOBAL,
    'region' => 'sidebar_first',
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function shurtape_products_nav_block_view($delta = '') {

  // This example is adapted from node.module.
  $block = array();

  switch ($delta) {

    case 'products_categories_nav':

      $block['subject'] = '<none>';
      $block['content'] = _get_products_categories_navigation();
      break;
  }
  return $block;
}

/**
 * Implements hook_block_configure().
 */
function shurtape_products_nav_block_configure($delta = '') {
  // This example comes from node.module.
  $form = array();
  if ($delta == 'products_categories_nav') {

    $vocs = _get_vocabulary_by_vocabulary_name('Tape Type');

    foreach($vocs['all'] as $voc) {
      $options[$voc->vid] = $voc->name;
    }

    $form['nav_vid'] = array(
      '#type' => 'select',
      '#title' => t('Select which vocabulary you want to display'),
      '#default_value' => variable_get('nav_vid', $vocs['selected']->vid),
      '#options' => $options,
    );

    $form['nav_level'] = array(
      '#type' => 'select',
      '#title' => t('How many levels should be displayed'),
      '#default_value' => variable_get('nav_level', 1),
      '#options' => array(1 => 'level 1', 2 => 'level 2', 3 => 'level 3', 4 => 'level 4'),
    );

  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function shurtape_products_nav_block_save($delta = '', $edit = array()) {

  switch($delta) {

    case 'products_categories_nav' :

      variable_set('nav_vid', $edit['nav_vid']);
      variable_set('nav_level', $edit['nav_level']);

      break;
  }
}

// ------------------------------------------------------ helper functions

/**
 * Returns a vocabulary name given a vocabulary object.
 *
 * @param str $vname
 *   The real vocabulary name to match against.
 */
function _get_vocabulary_by_vocabulary_name($vname) {

  $vocabularies = array(
    'all' => taxonomy_get_vocabularies(),
    'selected' => NULL,
  );

  foreach($vocabularies['all'] as $voc) {
    if($voc->name == $vname) {
      $vocabularies['selected'] = $voc;
    }
  }

  return $vocabularies;
}

/**
 * Handles the 'products_categories_nav' block content.
 */
function _get_products_categories_navigation() {
  global $language;

  $lang = ($language->language != 'en') ? $language->language.'/' : "";
  $tid = 'all';
  $vid = NULL;

  if(isset($_GET['t']) && is_numeric($_GET['t'])) {
    $tid = $_GET['t'];
  } elseif(isset($_GET['field_product_type']) && is_numeric($_GET['field_product_type'])) {
    $tid = $_GET['field_product_type'];
  }

  if(arg(1)) {
    $vid = arg(1);
  }

  if(isset($_GET['f']) && is_array($_GET['f'])) {
    $getf = $_GET['f'];
    $getf = explode(':', $getf[0]);
    if($getf[0] == 'field_product_type') {
      $vid = 'type';
    } elseif($getf[0] == 'field_product_market_list' || $getf[0] == 'field_product_market_list%3Aparents_all') {
      $vid = 'markets';
    }
    if(isset($getf[1])) {
      $tid = $getf[1];
    }
  }

  $vars['search_page'] = FALSE;
  if(arg(0) == 'search') {
    $vars['search_page'] = TRUE;
  }

  // Set default "open" class.
  $vid_clean = strtolower(str_replace(' ', '_', $vid));

  $vars['browse_' . $vid_clean . '_css_class'] = 'open';

  $vars['total_products'] = strip_tags(views_embed_view('product_count', 'by_term_id', $tid));

  $vars['browse_markets'] = _get_browse_products_rendered('Markets', 5);

  $vars['browse_type'] = _get_browse_products_rendered('Tape Type', 5);

  $vars['filters'] = shurtape_products_nav_generate_filters();

//  $vars['product_assets'] = module_invoke('facetapi', 'block_view', 'F1p1jU7Y1BKlLyRFvGgiNd8D4E1kpGiw');


  $view = views_get_view('search');
  $view->set_display('other_results');
  $view->execute();

  $asset_view_count = $view->total_rows;
  
//  $product_asset_count  = count($vars['product_assets']['content']['field_product_assets']['#items']);
//  $vars['total_product_assets'] = $asset_view_count+$product_asset_count;
  $vars['total_product_assets'] = $asset_view_count;

  if(arg(0) == "search"){
  unset($view);
  $view = views_get_view('search');
  $view->set_display('page');
  $view->execute();
  $vars['total_products'] =  $view->total_rows;
  }

  $f_array = isset($_GET['f']) ? $_GET['f'] : NULL;

  $vars['link_results'] = "";
  $vars['link_other_results'] = "";

  if(arg(0) == "search" && arg(1) == "products"):

      if(!is_null($f_array)):
        for($i=0;$i<count($f_array);$i++):
          if(!isset($link['output'])){
            $link['output'] = sprintf("f[%s]=%s",$i,addslashes($f_array[$i]));
          }else{
            $link['output'] .= sprintf("&f[%s]=%s",$i,addslashes($f_array[$i]));
          }
        endfor;
      endif;

        if(isset($_GET['search_api_views_fulltext'])){
          if(!isset($link['output'])){
          $link['output'] = sprintf("search_api_views_fulltext=%s",addslashes($_GET['search_api_views_fulltext']));
          }else{
          $link['output'] .= sprintf("&search_api_views_fulltext=%s",addslashes($_GET['search_api_views_fulltext']));
          }
        }

        if(isset($_GET['items_per_page'])){
          if(!isset($link['output'])){
          $link['output'] = sprintf("items_per_page=%s",addslashes($_GET['items_per_page']));
          }else{
          $link['output'] .= sprintf("&items_per_page=%s",addslashes($_GET['items_per_page']));
          }
        }

        if(isset($_GET['page'])){
          if(!isset($link['output'])){
          $link['output'] = sprintf("page=%s",addslashes($_GET['page']));
          }else{
          $link['output'] .= sprintf("&page=%s",addslashes($_GET['page']));
          }
        }

      if(!isset($link['output'])){
          $link['output'] = "";
      }else{
          $link['output'] = "?".$link['output'];
      }

      global $language;
      
      $lang = ($language->language != 'en') ? $language->language.'/' : "";
      
      $link['output']= str_replace("field_product_market_list%3Aparents_all", "field_product_market_list%253Aparents_all", $link['output']);
      $vars['link_results'] = base_path() .$lang. "search/products".$link['output'];
      $vars['link_other_results'] = base_path() .$lang. "search/products/other".$link['output'];

  else:

    $link_results_append = '';
    if(is_numeric($tid)) {
            if (arg(0) == "products" && arg(1) == "markets") {
                $link_results_append = "?f[0]=field_product_market_list%253Aparents_all:$tid";                
            }

            if (arg(0) == "products" && arg(1) == "type") {
                $link_results_append = "?f[0]=field_product_type:$tid";                
            }                        
        }

    $vars['link_results'] = base_path() . "search/products{$link_results_append}";    
    
//if true means we are at landing of type or market, not a subcategory or seach
//because no query string present
    if(!isset($_GET['f'])){
        $vars['redirect_to_product_landing'] = true;
    }else{
        $vars['redirect_to_product_landing'] = false;
    }
  endif;

  return theme('products_filter_bar', $vars);

}

/**
 * Returns a fully rendered vocabulary filter menu.
 *
 * @param str $voc_name
 *   The vocabulary name to match against.
 * @param int $level
 *   The number of term levels (children) to return.
 */
function _get_browse_products_rendered($voc_name, $level = 1, $search_links = FALSE) {

  $vocs = _get_vocabulary_by_vocabulary_name($voc_name);

  $type_vid = variable_get('nav_vid', $vocs['selected']->vid);

  if(!$type_vid) {
    return t('A vocabulary for the "@voc_name" must be selected', array('@voc_name' => $voc_name));
  }

  $tree = taxonomy_get_nested_tree($type_vid, array('max_depth' => $level));

  return taxonomy_nested_tree_render($tree, FALSE, $search_links, $voc_name);
}

/**
 * Builds and returns a nested tree.
 *
 * @param array $opts
 *   An array of options.
 * @return array
 */
function taxonomy_get_nested_tree($vtid = array(), $opts = array()) {

  $return = array();

  // We only want to fetch from the non-recursive call.
  if(!is_array($vtid)) {
    $cache_key_append = '';
    if(isset($opts['full_entity'])) {
        $cache_key_append = '_full';
    }
    $cache_key = "shurtape_nav_tree_vid_{$vtid}{$cache_key_append}";
    if($return = cache_get($cache_key, 'cache')) {
      return $return->data;
    }
  }

  $max_depth = isset($opts['max_depth']) ? $opts['max_depth'] : NULL;
  $parent = isset($opts['parent']) ? $opts['parent'] : 0;
  $parents_index = isset($opts['parents_index']) ? $opts['parents_index'] : array();
  $depth = isset($opts['depth']) ? $opts['depth'] : 0;
  $full_entity = isset($opts['full_entity']) ? $opts['full_entity'] : FALSE;

  if (!is_array($vtid)) {
    $vtid = taxonomy_get_tree($vtid, 0, NULL, $full_entity);
  }

  foreach ($vtid as $term) {
    foreach ($term->parents as $term_parent) {
      if ($term_parent == $parent) {
        $return[$term->tid] = $term;
      }
      else {
        $parents_index[$term_parent][$term->tid] = $term;
      }
    }
  }

  if($return) {
    foreach ($return as &$term) {
      if (isset($parents_index[$term->tid]) && (is_null($max_depth) || $depth < $max_depth)) {

        $opts_rec = array('max_depth' => $max_depth, 'parent' => $term->tid, 'parents_index' => $parents_index, 'depth' => $depth + 1);

        $term->children = taxonomy_get_nested_tree($parents_index[$term->tid], $opts_rec);
      }
    }
  }

  // Make sure to only cached at the end of recursive calls.
  if(isset($cache_key) && $cache_key) {
    // Don't cache empty object.
    if($return) {
      cache_set($cache_key, $return, 'cache', CACHE_PERMANENT);
    }
  }

  return $return;
}

/**
 * Builds a renderable nested taxonomy list.
 *
 * @param array $tree
 * @param string $recurring
 * @return multitype
 *   A rendererable object.
 */
function taxonomy_nested_tree_render($tree, $recurring = FALSE, $search_links = FALSE, $voc_name = NULL) {
  global $language;
  $tree = localize_tree($tree);

  $items = array();
  $tid = NULL;

  if(isset($_GET['t']) && is_numeric($_GET['t'])) {
    $tid = $_GET['t'];
  } elseif(isset($_GET['f'])) {
    $fget = $_GET['f'];
    if($fget) {
      $fget = explode(':', $fget[0]);
      $tid = $fget[1];
    }
  }

  // We only want to fetch from the non-recursive call.
  if(!$recurring) {
    $cache_key_append = strtolower(str_replace(' ', '_', $voc_name));
    $cache_key = "shurtape_nav_tree_items_{$cache_key_append}_{$tid}";
    if($items = cache_get($cache_key, 'cache')) {
      return $items->data;
    }
  }

  $tid_parents = array();
  if($tid) {
    foreach(taxonomy_get_parents_all($tid) as $parent) {
      $tid_parents[$parent->tid] = $parent->tid;
    }
  }

  if (count($tree)) {
    foreach ($tree as $term) {
      $item = array('data' => l($term->name, "taxonomy/term/{$term->tid}"));

      if (isset($term->children) && ($tid == $term->tid || isset($tid_parents[$term->tid]))) {
        $item['class'] = array('class' => 'open');
        $item["children"] = taxonomy_nested_tree_render($term->children, TRUE);
      }

      $items[] = $item;
    }
  }

  if ($recurring) {
    return $items;
  }

  $return = array(
    '#theme' => 'item_list',
    '#items' => $items,
    '#attributes' => array('class' => 'toggle_target  '),
  );

  if($items) {
    cache_set($cache_key . '_' . $language->language, $return, 'cache', CACHE_PERMANENT);
  }

  return $return;
}



/*
* Helper Functions For Building Filters Out
*
*/

function shurtape_products_nav_generate_filters(){

  // Adhesive Facet xH1Twe80Ztkb3gBmLcYd9z4pV3tXKHYS
  // Backing Facet t0No50NMYFJ6A46rvE1GW0Eyq8xyA2D8
  // Liner Facet m9muCpNAwXi2JUhGfm32jUfFxdLu1BIZ
  // Box Weight Facet oYpm22zWhQ91I8A9UCiO1N900uDFRoIl
  // Handling Conditions Facet 5yYen93hSx8qnWEN4TGY10Q3BoFi3RpC
  // FDA Compliance Facet otbZMmxtBIvFD1wiOrk0YoPy4tSPvD1X
  // Holding Power Facet 4Fz3Dl6NCm1Q5JkfTklKfxcZU1ahKTg9
  // Cold Temp Facet 5N7eo9iRI0TlUjmYaEfHM7dDNKm3f0PM
  // UV Resistance Facet O6wEc4yQyvxsqzYX2gdD7g3B3Jda5G7S
  // Water Proof Facet Fndyn0XCzZ2fhKcwcdati8u0oU1q59sK
  // Green Point Facet DI0sG30v9RoGLcUpPAoyXZbiEIm3EKO3
  // UL723 Facet 08e0gIp8tw73PqzZJHJmcgtriIG7GckQ
  // UL Listed Facet 2ygQZHNQRdc8v7TLr116aAyQayftL1Ta
  // Clean Removal Facet gEPNXsibDMPdLHwrEAwPl0fduFyWEigO
  // Carpet Tape Facet SdLkCXiMRtYABNTzlcAmV1hTcmukUEWH

  // Packaging TID - 201
  // Cloth & Duct Tapes TID - 184
  // Double Coat TID - 234
  // Paper Tapes TID - 170
  // Foil TID - 224
  // Building and Construction Tapes TID - 246

  // instantiate facets
//  $getfacets = views_embed_view('search');
  $filters = [];
  // Set Facet Blocks Available
  $facets_array[201] = array("oYpm22zWhQ91I8A9UCiO1N900uDFRoIl","5yYen93hSx8qnWEN4TGY10Q3BoFi3RpC","5N7eo9iRI0TlUjmYaEfHM7dDNKm3f0PM","otbZMmxtBIvFD1wiOrk0YoPy4tSPvD1X","4Fz3Dl6NCm1Q5JkfTklKfxcZU1ahKTg9","O6wEc4yQyvxsqzYX2gdD7g3B3Jda5G7S");
  $facets_array[184] = array("Fndyn0XCzZ2fhKcwcdati8u0oU1q59sK","DI0sG30v9RoGLcUpPAoyXZbiEIm3EKO3","08e0gIp8tw73PqzZJHJmcgtriIG7GckQ","2ygQZHNQRdc8v7TLr116aAyQayftL1Ta","O6wEc4yQyvxsqzYX2gdD7g3B3Jda5G7S","m9muCpNAwXi2JUhGfm32jUfFxdLu1BIZ");
  $facets_array[234] = array("xH1Twe80Ztkb3gBmLcYd9z4pV3tXKHYS","SdLkCXiMRtYABNTzlcAmV1hTcmukUEWH","m9muCpNAwXi2JUhGfm32jUfFxdLu1BIZ");
  $facets_array[170] = array("gEPNXsibDMPdLHwrEAwPl0fduFyWEigO","O6wEc4yQyvxsqzYX2gdD7g3B3Jda5G7S","aaflergq0aJGFPDJPitTTy11PO4ae91i");
  $facets_array[224] = array("Fndyn0XCzZ2fhKcwcdati8u0oU1q59sK","DI0sG30v9RoGLcUpPAoyXZbiEIm3EKO3","2ygQZHNQRdc8v7TLr116aAyQayftL1Ta","m9muCpNAwXi2JUhGfm32jUfFxdLu1BIZ","xH1Twe80Ztkb3gBmLcYd9z4pV3tXKHYS");
  $facets_array[499] = array("O6wEc4yQyvxsqzYX2gdD7g3B3Jda5G7S","DI0sG30v9RoGLcUpPAoyXZbiEIm3EKO3","2ygQZHNQRdc8v7TLr116aAyQayftL1Ta");
  $facets_array[246] = array("O6wEc4yQyvxsqzYX2gdD7g3B3Jda5G7S","DI0sG30v9RoGLcUpPAoyXZbiEIm3EKO3","2ygQZHNQRdc8v7TLr116aAyQayftL1Ta");

  $f_array = isset($_GET['f']) ? $_GET['f'] : NULL;
  $t_array = isset($_GET['t']) ? $_GET['t'] : NULL;

  if($f_array != NULL) {
    $extracted_values = explode(":", $f_array[0]);
    if($extracted_values[0] == "field_product_type"){
      $sortTID = taxonomy_get_parents_all($extracted_values[1]);
    }
  } elseif($t_array != NULL) {
    $sortTID = taxonomy_get_parents_all($t_array);
  }

  if(isset($sortTID) && is_array($sortTID)){
    krsort ($sortTID);

    foreach($sortTID as $id => $value):
      $parentTID = $value->tid;
      break;
    endforeach;

    if(isset($facets_array[$parentTID])) {
      $currentFacet = $facets_array[$parentTID];

      for($i=0;$i<count($currentFacet);$i++):

        if(isset($currentFilter)){
          unset($currentFilter);
        }

        $currentFilter = module_invoke('facetapi', 'block_view', $currentFacet[$i]);
        if(isset($currentFilter['content']['#facet']['allowed operators']['and']) || isset($currentFilter['content']['#facet']['allowed operators']['or'])){

          $current_id  = $currentFilter['content']['#facet']['field'];
          $current_name = $currentFilter['content']['#facet']['label'];
          $current_name_not = str_replace("Is","Is Not",$currentFilter['content']['#facet']['label']);

          for($b=0;$b<count($currentFilter['content'][$current_id]['#items']);$b++) :
            $current_data = $currentFilter['content'][$current_id]['#items'][$b]['data'];
            if($b==1){
              $currentFilter['content'][$current_id]['#items'][$b]['data'] = str_replace("Yes",$current_name,$current_data);
            } else {
              $currentFilter['content'][$current_id]['#items'][$b]['data'] = str_replace("No",$current_name_not,$current_data);
            }

          endfor;

          $currentFilter['content']['#title'] = str_replace("Is ","",$current_name);

        }

        if(!is_null($currentFilter)){
          $filters[]= $currentFilter;
        }

      endfor;

      return $filters;

    }
  }
}

function _shurtape_products_nav_compare_product_session_manager($op, $product_id = null) {
    $resp = new stdClass();
    switch ($op) {
        case "clear":
            unset($_SESSION["shurtape"]["compare_products"]);
            $resp->status = "Compare session cleared";
            break;
        case "clear_init":
            unset($_SESSION["shurtape"]["compare_products"]);
            return;
            break;
        case "get":
            $resp->status = "Returning current array of products to compare";
            $resp->product_string = implode("+", $_SESSION["shurtape"]["compare_products"]["list"]);
            break;
        case "add":
            if (is_numeric($product_id) && !isset($_SESSION["shurtape"]["compare_products"]["list"][$product_id])) {
                $_SESSION["shurtape"]["compare_products"]["list"][$product_id] = $product_id;
                $resp->status = "Product added: $product_id";
            } else {
                $resp->status = "Product invalid or already in array: $product_id";
            }
            break;
        case "remove":
            if (is_numeric($product_id) && isset($_SESSION["shurtape"]["compare_products"]["list"][$product_id])) {
                unset($_SESSION["shurtape"]["compare_products"]["list"][$product_id]);
                $resp->status = "Product removed: $product_id";
            } else {

                $resp->status = "Product invalid or not in array: $product_id";
            }
            break;
        default:
            $resp->status = "No action performed";
    }

    $resp->product_count = count($_SESSION["shurtape"]["compare_products"]["list"]);
    if ($op != "clear" && $op != "clear_init" && $op != "get") {
        parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $queries);
        $_SESSION["shurtape"]["compare_products"]["query"] = $queries;
        $_SESSION["shurtape"]["compare_products"]["path"] = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
    }
    return drupal_json_output($resp);
}

function _shurtape_products_nav_check_product_session_compare_clear() {
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];

    $current_path = parse_url($pageURL, PHP_URL_PATH);
    parse_str(parse_url($pageURL, PHP_URL_QUERY), $current_query_string);

    //check if we have a session to begin with    
    if (isset($_SESSION["shurtape"]["compare_products"])) {
        //check if we are on the same page as before and only doing pagination, if not, reset the compare session        
        $do_clear = false;
        if ($_SESSION["shurtape"]["compare_products"]["path"] != $current_path) {
            $do_clear = true;
        }

        if (!$do_clear) {
            //check if query string before and now have the same size    
            $count_query_before = count($_SESSION["shurtape"]["compare_products"]["query"]);
            $count_query_now = count($current_query_string);

            //normalize size for items per page and page number parameters (not taking into account those for the comparison)
            if (isset($_SESSION["shurtape"]["compare_products"]["query"]["items_per_page"]))
                $count_query_before--;
            if (isset($_SESSION["shurtape"]["compare_products"]["query"]["page"]))
                $count_query_before--;
            if (isset($current_query_string["items_per_page"]))
                $count_query_now--;
            if (isset($current_query_string["page"]))
                $count_query_now--;
            //normalize for empty parameters
            foreach ($_SESSION["shurtape"]["compare_products"]["query"] as $session_parameter) {
                if (empty($session_parameter))
                    $count_query_before--;
            }
            foreach ($current_query_string as $current_parameter) {
                if (empty($current_parameter))
                    $count_query_now--;
            }
            //now check if they have the same amount of parameters to continue further
            if ($count_query_before != $count_query_now) {
                $do_clear = true;
            }
        }

        if (!$do_clear) {
            // check all the parameters in the current page existed before,
            // except page number and items per page
            foreach ($current_query_string as $key => $current_parameter) {
                //discard parameters like items per page and page number
                if ($key != "items_per_page" && $key != "page") {
                    //check for current parameter having value and existing in session 
                    if (!isset($_SESSION["shurtape"]["compare_products"]["query"][$key]) && $current_parameter != "") {
                        $do_clear = true;
                        //parameter has same name, check if current parameter value is the same as session, array version   
                    } elseif (isset($_SESSION["shurtape"]["compare_products"]["query"][$key]) && is_array($current_parameter)) {
                        foreach ($current_parameter as $key2 => $value) {
                            if ($value != $_SESSION["shurtape"]["compare_products"]["query"][$key][$key2]) {
                                $do_clear = true;
                            }
                        }
                        //parameter has same name, check if current parameter value is the same as session, simple value version   
                    } else {
                        if ($_SESSION["shurtape"]["compare_products"]["query"][$key] != $current_parameter) {
                            $do_clear = true;
                        }
                    }
                }
            }
        }

        if ($do_clear) {
            _shurtape_products_nav_compare_product_session_manager("clear_init");
        }
    }//if session exists for $_SESSION["shurtape"]["compare_products"]
}
