<?php
$webform_node = node_load(arg(1));
$display = array('label' => 'hidden');
$content_title = render(field_view_field('node', $webform_node, 'field_promo_content_title', $display));
$content_body = render(field_view_field('node', $webform_node, 'field_promotion_content_body', $display));
$logos = render(field_view_field('node', $webform_node, 'field_promotion_logos', $display))
?>

<div class="row blue_bar promotion_product_header padded_body">
	<?php print $content_title; ?>    
</div>

<div class="padded_body">
	<p class="promo_thank-you-interest">Thank you for your interest in Shurtape® products.</p>
	<?php print $content_body; ?>
	<?php print $logos; ?>
</div>