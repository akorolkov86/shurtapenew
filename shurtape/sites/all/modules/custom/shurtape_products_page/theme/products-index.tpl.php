<?php
$browse_markets     = isset($browse_markets) ? $browse_markets : null;
$markets           = isset($markets) ? $markets : null;
$browse_tape_type   = isset($browse_tape_type) ? $browse_tape_type : null;
$tape_type         = isset($tape_type) ? $tape_type : null;

if (arg(1) != '') {
    if (arg(1) == 'type') {
        $show_section = "type";
    } else {
        $show_section = "markets";
    }
} else {
    $show_section = "all";
}
?>

<?php if ( ! filter_input( INPUT_GET, 't', FILTER_SANITIZE_STRING ) ): ?>
	<h1 class="headline">
		<?php print t('Shurtape Products') ?>
	</h1>
<?php endif; ?>

<?php if($show_section == "markets" || $show_section == "all"):?>

	<h3 class="blue_bar"><?php print t('BROWSE BY MARKET') ?></h3>

	<div class="product_grid browse_market">
		<ul class="flush">
			<?php if($browse_markets !== null): ?>
				<?php foreach( $browse_markets as $market ): ?>
					<li>
<?php



?>
						<a href="<?php print url("taxonomy/term/{$market->tid}"); ?>" class="item">
							<span class="mobile_link_icon mobile">
								<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
							</span>
							<span class="item_thumb">
								<?php 
//if(isset($market->field_markets_term_image_index) && $market->field_markets_term_image_index):
$terms = taxonomy_term_load($market->tid);

if(isset($terms->field_markets_term_image_index['und'][0]['uri'])):?>
<img  src="<?php print file_create_url($terms->field_markets_term_image_index['und'][0]['uri']); ?>" />
				<?php //print render($market->field_markets_term_image_index); ?>
								<?php else : /* @todo this else needs to be removed after dev */ ?>
									<img src="<?php print $path; ?>/images/index_default_image.jpg" />
								<?php endif; ?>
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<span class="item_title"><b><?php print $market->name; ?></b></span>
							</span>
						</a>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>

<?php endif; ?>

<?php if($show_section == "type" || $show_section == "all"):?>

	<h3 class="blue_bar"><?php print t('BROWSE BY TYPE') ?></h3>
	<div class="product_grid browse_type">
		<ul class="flush">
			<?php if( $browse_tape_type !== null ): ?>
				<?php foreach($browse_tape_type as $tape_type) : ?>
					<li>
						
						<a href="<?php print url("taxonomy/term/{$tape_type->tid}"); ?>" class="item">
							<span class="mobile_link_icon mobile">
								<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
							</span>
							<span class="item_thumb">
								<?php 
//if(isset($market->field_markets_term_image_index) && $market->field_markets_term_image_index):
$terms_t = taxonomy_term_load($tape_type->tid);

if(isset($terms_t->field_type_term_image_index_page['und'][0]['uri'])):?>
				<img  src="<?php print file_create_url($terms_t->field_type_term_image_index_page['und'][0]['uri']); ?>" />

								<?php else : /* @todo this else needs to be removed after dev */ ?>
									<img src="<?php print $path; ?>/images/fpo_category_item.jpg" />
								<?php endif; ?>
							</span>
							<span class="item_content">
								<span class="cta_arrow"></span>
								<?php

								/* strip the "Tapes" from the item title - but only if it's the last word */
								$split   = explode( ' ', $tape_type->name );
								$last   = $split[ count( $split ) - 1 ];
								$item_title = ( strtolower( $last ) == 'tapes' ) ? '<b>' . str_replace( 'Tapes', '', $tape_type->name ) . '</b> Tapes' : '<b>' . $tape_type->name . '</b>';
								?>
								<span class="item_title"><?php echo $item_title; ?></span>
							</span>
						</a>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>

<?php endif; ?>
