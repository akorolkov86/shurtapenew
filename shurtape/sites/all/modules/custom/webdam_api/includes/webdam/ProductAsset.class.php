<?php
/**
 * @author marcolara
 *
 */
class ProductAsset {
	private $id;
	private $status;
	private $userid;
	private $folderid;
	private $filename;
	private $name;
	private $size;
	private $fileextension;
	private $width;
	private $height;
	private $embededlink;
	private $expirationdate;
	private $datecreated;
	private $description;

	/**
	 * Constructor
	 */
	public function __construct($id, $status, $userid, $folderid, $filename, $name, $size, $fileextension, $width, $height, $embededlink, $expirationdate, $datecreated, $description) {
		$this->setId($id);
		$this->setStatus($status);
		$this->setUserid($userid);
		$this->setFolderid($folderid);
		$this->setFilename($filename);
		$this->setName($name);
		$this->setSize($size);
		$this->setFileextension($fileextension);
		$this->setWidth($width);
		$this->setHeight($height);
		$this->setEmbededlink($embededlink);
		$this->setExpirationdate($expirationdate);
		$this->setDatecreated($datecreated);
		$this->setDescription($description);
	}
	
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = (string)$id;
	}

	public function getStatus() {
		return $this->status;
	}

	public function setStatus($status) {
		$this->status = (string)$status;
	}

	public function getUserid() {
		return $this->userid;
	}

	public function setUserid($userid) {
		$this->userid = (string)$userid;
	}

	public function getFolderid() {
		return $this->folderid;
	}

	public function setFolderid($folderid) {
		$this->folderid = (string)$folderid;
	}

	public function getFilename() {
		return $this->filename;
	}

	public function setFilename($filename) {
		$this->filename = (string)$filename;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = (string)$name;
	}

	public function getSize() {
		return $this->size;
	}

	public function setSize($size) {
		$this->size = (string)$size;
	}

	public function getFileextension() {
		return $this->fileextension;
	}

	public function setFileextension($fileextension) {
		$this->fileextension = (string)$fileextension;
	}

	public function getWidth() {
		return $this->width;
	}

	public function setWidth($width) {
		$this->width = (string)$width;
	}

	public function getHeight() {
		return $this->height;
	}

	public function setHeight($height) {
		$this->height = (string)$height;
	}

	public function getEmbededlink() {
		return $this->embededlink;
	}

	public function setEmbededlink($embededlink) {
		$this->embededlink = (string)$embededlink;
	}

	public function getExpirationdate() {
		return $this->expirationdate;
	}

	public function setExpirationdate($expirationdate) {
		$this->expirationdate = (string)$expirationdate;
	}

	public function getDatecreated() {
		return $this->datecreated;
	}

	public function setDatecreated($datecreated) {
		$this->datecreated = (string)$datecreated;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = (string)$description;
	}

}
