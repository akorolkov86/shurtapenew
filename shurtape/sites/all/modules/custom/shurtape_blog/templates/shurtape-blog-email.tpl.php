<?php
/**
 * @file
 * The default template file for shurtape blog e-mails.
 */
?>
<style type="text/css">
  p {
    font-family: Franklin Gothic, Arial, sans-serif;
  }
</style>
<table border="0" width="600" align="center" cellpadding="0" cellspacing="10" bgcolor="#ffffff">
  <tr>
    <td style="float: left; border: 0;">
      <a href="http://www.shurtape.com/?utm_campaign=Blog%20Newsletter&utm_medium=Email&utm_source=Email&utm_content=Logo" target="_blank">
        <img src="https://twshurtape.s3.amazonaws.com/content/images/shurtape-logo.png" border="0" alt="Shurtape"/>
      </a>
    </td>
    <td style="float: right; width: 160px; height: 26px; margin: 50px 0 0 0; border: 0;">
      <a href="https://twitter.com/ShurtapeTech" target="_blank">
        <img src="image:/sites/all/modules/custom/shurtape_blog/i/twitter.png" style="width: 32px; border: 0; padding: 0 0 0 0; float: right;" alt="Twitter"/>
      </a>
      <a href="http://www.linkedin.com/company/shurtape-technologies-llc" target="_blank">
        <img src="image:/sites/all/modules/custom/shurtape_blog/i/linkedin.png" style="width: 32px; border: 0; padding: 0 10px 0 0; float: right;" alt="LinkedIn"/>
      </a>
      <a href="http://www.youtube.com/user/Shurtapetech" target="_blank">
        <img src="image:/sites/all/modules/custom/shurtape_blog/i/youtube.png" style="width: 32px; border: 0; padding: 0 10px 0 0; float: right;" alt="YouTube" />
      </a>
      <a href="https://www.facebook.com/Shurtape" target="_blank">
        <img src="image:/sites/all/modules/custom/shurtape_blog/i/facebook.png" style="width: 32px; border: 0; padding: 0 10px 0 0; float: right;" alt="Facebook"/>
      </a>
    </td>
  </tr>
</table>
<table border="0" width="600" align="center" cellpadding="0" cellspacing="10" bgcolor="#ffffff">
  <tr>
    <td>
      <p>
        <font face="Franklin Gothic, Arial, sans-serif"><?php print $body;?></font>
      </p>
    </td>
  </tr>
</table>
<hr>
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center">
  <tr>
    <td>
      <table width="600" cellpadding="0" cellspacing="10" bgcolor="#ffffff" align="center">
        <tr style="width: 200px; height: 75px; margin: 60px 0 0 0;">
          <td valign="top">
            <p style="font-family:Franklin Gothic Book; color: #000000; font-size:11px; text-align: Left; margin: 0 10px 0 0;">
              <?php if (!empty($params['update_profile'])): ?>
                <a href="<?php print $params['update_profile']; ?>" alias="Update Profile">Update Profile</a>
              <?php endif; ?>
              <?php if (!empty($params['unsubscribe_url'])): ?>
                | <a href="<?php print $params['unsubscribe_url']; ?>" alias="Unsubscribe">Unsubscribe</a>
              <?php endif; ?>
            </p>
          </td>
          <td valign="top">
            <p style="font-family:Franklin Gothic Book; color: #000000; font-size:11px; text-align: right; margin: 0 0 0 10px;">&copy;2014 <b>Shurtape Technologies, LLC</b> All rights reserved. <br>1712 8<sup>th</sup> Street Drive SE, Hickory, NC 28602, USA<br>(888.442.TAPE)</p><br/>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
