<?php
/**
 * Created by PhpStorm.
 * User: punk_undead
 * Date: 25.07.15
 * Time: 2:04
 */

namespace Drupal\d_submodules;


trait SelfName {
  static function getSelfShortName($class = NULL) {
    if (NULL === $class) {
      $class = get_called_class();
    }
    return (new \ReflectionClass($class))->getShortName();
  }
}