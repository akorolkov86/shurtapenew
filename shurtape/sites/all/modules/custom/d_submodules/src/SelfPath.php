<?php

namespace Drupal\d_submodules;

trait SelfPath {
  static function getSelfPath($class = NULL) {
    if (NULL === $class) {
      $class = get_called_class();
    }
    return dirname(Submodules::getClassMap()[$class]);
  }
}