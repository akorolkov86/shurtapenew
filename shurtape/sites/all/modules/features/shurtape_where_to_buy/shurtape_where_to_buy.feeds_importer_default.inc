<?php
/**
 * @file
 * shurtape_where_to_buy.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function shurtape_where_to_buy_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'locations_impoter';
  $feeds_importer->config = array(
    'name' => 'Locations Impoter',
    'description' => 'Importer for distributors locations',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 0,
        'directory' => 's3://feeds',
        'allowed_schemes' => array(
          's3' => 's3',
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '6',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Name',
            'target' => 'field_distributor_name',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Address',
            'target' => 'field_distributor_address',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'City',
            'target' => 'field_city',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'State',
            'target' => 'field_state',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Zip',
            'target' => 'field_zip',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Country',
            'target' => 'field_country',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Phone',
            'target' => 'field_phone',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Website',
            'target' => 'field_website',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'Buy Online',
            'target' => 'field_buy_online',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Partner',
            'target' => 'field_partner',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Address',
            'target' => 'locations:street',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'City',
            'target' => 'locations:city',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'Country',
            'target' => 'locations:country',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'Zip',
            'target' => 'locations:postal_code',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'State',
            'target' => 'locations:province',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'Latitude',
            'target' => 'locations:locpick][user_latitude',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'Longitude',
            'target' => 'locations:locpick][user_longitude',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 1,
        'bundle' => 'location',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['locations_impoter'] = $feeds_importer;

  return $export;
}
