<?php
/**
 * @file
 * shurtape_site_layout.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function shurtape_site_layout_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'common_footer';
  $mini->category = 'Layout';
  $mini->admin_title = 'common footer';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'shurtape-layout__footer';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'footer_desktop__top' => NULL,
      'footer_desktop__bottom' => NULL,
      'footer_desktop__first' => NULL,
      'footer_desktop__second' => NULL,
      'footer_desktop__third' => NULL,
      'footer_desktop__fourth' => NULL,
      'footer_desktop__fifth' => NULL,
      'footer_mobile' => NULL,
    ),
  );
  $display->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'none',
    ),
  );
  $display->title = '';
  $display->uuid = '4361bf0d-e108-4255-9785-e6c76be3deb1';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-35c63cb3-08ca-4333-ac74-c6b7fbb9800b';
    $pane->panel = 'footer_desktop__bottom';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '<div id="copyline" class="fixed_wrap clearfix">© 2006-{{drupal_date(constant(\'REQUEST_TIME\'), \'custom\', \'Y\')}} Shurtape Technologies, LLC</div>',
      'admin_title' => 'copyline',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '35c63cb3-08ca-4333-ac74-c6b7fbb9800b';
    $display->content['new-35c63cb3-08ca-4333-ac74-c6b7fbb9800b'] = $pane;
    $display->panels['footer_desktop__bottom'][0] = 'new-35c63cb3-08ca-4333-ac74-c6b7fbb9800b';
    $pane = new stdClass();
    $pane->pid = 'new-23166336-56e0-4cc1-b0d8-789482b3b9fa';
    $pane->panel = 'footer_desktop__fifth';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '<a class="pull-right footer_logo" title="ShurTape - True to Your Work" href="/">ShurTape</a>
',
      'admin_title' => 'footer_logo',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '23166336-56e0-4cc1-b0d8-789482b3b9fa';
    $display->content['new-23166336-56e0-4cc1-b0d8-789482b3b9fa'] = $pane;
    $display->panels['footer_desktop__fifth'][0] = 'new-23166336-56e0-4cc1-b0d8-789482b3b9fa';
    $pane = new stdClass();
    $pane->pid = 'new-8323f7e7-c93a-4f28-bcd1-dbf843cb797c';
    $pane->panel = 'footer_desktop__fifth';
    $pane->type = 'views';
    $pane->subtype = 'social_icons_home';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => 'FOLLOW US',
      'override_title_heading' => 'h4',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '8323f7e7-c93a-4f28-bcd1-dbf843cb797c';
    $display->content['new-8323f7e7-c93a-4f28-bcd1-dbf843cb797c'] = $pane;
    $display->panels['footer_desktop__fifth'][1] = 'new-8323f7e7-c93a-4f28-bcd1-dbf843cb797c';
    $pane = new stdClass();
    $pane->pid = 'new-a04b9528-75c5-4ede-afe2-d480e15d4a34';
    $pane->panel = 'footer_desktop__fifth';
    $pane->type = 'views';
    $pane->subtype = 'social_icons_home';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'a04b9528-75c5-4ede-afe2-d480e15d4a34';
    $display->content['new-a04b9528-75c5-4ede-afe2-d480e15d4a34'] = $pane;
    $display->panels['footer_desktop__fifth'][2] = 'new-a04b9528-75c5-4ede-afe2-d480e15d4a34';
    $pane = new stdClass();
    $pane->pid = 'new-2e601c34-d6c1-4983-9e9f-d696ed735b48';
    $pane->panel = 'footer_desktop__first';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '<h4><a href="/" title="{{t(\'Home\')}}">{{t(\'Home\')}}</a></h4>',
      'admin_title' => '',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2e601c34-d6c1-4983-9e9f-d696ed735b48';
    $display->content['new-2e601c34-d6c1-4983-9e9f-d696ed735b48'] = $pane;
    $display->panels['footer_desktop__first'][0] = 'new-2e601c34-d6c1-4983-9e9f-d696ed735b48';
    $pane = new stdClass();
    $pane->pid = 'new-cf5014e4-14b2-40d5-846f-653d40b553f6';
    $pane->panel = 'footer_desktop__fourth';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-about-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cf5014e4-14b2-40d5-846f-653d40b553f6';
    $display->content['new-cf5014e4-14b2-40d5-846f-653d40b553f6'] = $pane;
    $display->panels['footer_desktop__fourth'][0] = 'new-cf5014e4-14b2-40d5-846f-653d40b553f6';
    $pane = new stdClass();
    $pane->pid = 'new-2ed99268-378b-43ee-b46b-343dc3a34eb9';
    $pane->panel = 'footer_desktop__second';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-products-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2ed99268-378b-43ee-b46b-343dc3a34eb9';
    $display->content['new-2ed99268-378b-43ee-b46b-343dc3a34eb9'] = $pane;
    $display->panels['footer_desktop__second'][0] = 'new-2ed99268-378b-43ee-b46b-343dc3a34eb9';
    $pane = new stdClass();
    $pane->pid = 'new-e38f73ab-5281-4393-b653-7a3a3ec3a37c';
    $pane->panel = 'footer_desktop__third';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-resources-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e38f73ab-5281-4393-b653-7a3a3ec3a37c';
    $display->content['new-e38f73ab-5281-4393-b653-7a3a3ec3a37c'] = $pane;
    $display->panels['footer_desktop__third'][0] = 'new-e38f73ab-5281-4393-b653-7a3a3ec3a37c';
    $pane = new stdClass();
    $pane->pid = 'new-ddae4d6a-6ca6-46dc-b278-a2ed0128bce6';
    $pane->panel = 'footer_desktop__top';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => ' <a href="#" class="scroll_top">Scroll Top</a>',
      'admin_title' => 'scroll_top',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ddae4d6a-6ca6-46dc-b278-a2ed0128bce6';
    $display->content['new-ddae4d6a-6ca6-46dc-b278-a2ed0128bce6'] = $pane;
    $display->panels['footer_desktop__top'][0] = 'new-ddae4d6a-6ca6-46dc-b278-a2ed0128bce6';
    $pane = new stdClass();
    $pane->pid = 'new-5ca4647c-c460-419e-8413-516811754c33';
    $pane->panel = 'footer_mobile';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '    <div class="logo_bar">
      <a title="Home" id="logo" href="/">
        <img alt="Home" src="/sites/all/themes/shurtape/images/logo_footer_mobile_left.png">
      </a>
      <a title="Questions/Orders? Call Us: 888-442-8273" id="logo_cta" href="tel:888-442-8273">
        <img alt="Questions/Orders? Call Us: 888-442-8273" src="/sites/all/themes/shurtape/images/logo_footer_mobile_right.png">
      </a>
    </div>
    <ul class="footer_links unstyled">
      <li>
        <a title="Products" class="btn" href="/products">Products</a>
      </li>
      <li>
        <a title="Resources" class="btn" href="/resources/media">Resources</a>
      </li>
      <li>
        <a title="About" class="btn" href="/about/profile">About</a>
      </li>
      <li>
        <a title="Contact" class="btn" href="/contact-us">Contact Us</a>
      </li>
      <li>
        <a title="Where to Buy" class="btn btn-orange" href="/where-to-buy">Where
          to Buy</a>
      </li>
    </ul>',
      'admin_title' => 'footer_mobile__menu',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5ca4647c-c460-419e-8413-516811754c33';
    $display->content['new-5ca4647c-c460-419e-8413-516811754c33'] = $pane;
    $display->panels['footer_mobile'][0] = 'new-5ca4647c-c460-419e-8413-516811754c33';
    $pane = new stdClass();
    $pane->pid = 'new-b6905317-8692-4578-a7e6-5cd0906ae9d8';
    $pane->panel = 'footer_mobile';
    $pane->type = 'views';
    $pane->subtype = 'social_icons_home';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 1,
      'override_title_text' => 'Follow Us',
      'override_title_heading' => 'h4',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'default',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'clearfix',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b6905317-8692-4578-a7e6-5cd0906ae9d8';
    $display->content['new-b6905317-8692-4578-a7e6-5cd0906ae9d8'] = $pane;
    $display->panels['footer_mobile'][1] = 'new-b6905317-8692-4578-a7e6-5cd0906ae9d8';
    $pane = new stdClass();
    $pane->pid = 'new-1c66a676-45af-44cc-ad50-47b01cff1f73';
    $pane->panel = 'footer_mobile';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '    <div class="back_to_top">
      <a href="#">
        <img alt="Back to Top" src="/sites/all/themes/shurtape/images/btn_mobile_scroll_top.png">
      </a>
    </div>',
      'admin_title' => 'footer_mobile__back_to_top',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'clearfix',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '1c66a676-45af-44cc-ad50-47b01cff1f73';
    $display->content['new-1c66a676-45af-44cc-ad50-47b01cff1f73'] = $pane;
    $display->panels['footer_mobile'][2] = 'new-1c66a676-45af-44cc-ad50-47b01cff1f73';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['common_footer'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'common_header';
  $mini->category = 'Layout';
  $mini->admin_title = 'common header';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'shurtape-layout--header';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'before_header' => NULL,
      'header' => NULL,
    ),
  );
  $display->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'none',
    ),
  );
  $display->title = '';
  $display->uuid = '0a405e49-7387-4c17-ac58-07b224adfbc1';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3dbda090-7d73-4de6-bb14-b5752bd6312a';
    $pane->panel = 'before_header';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '  <div class="logo_bar mobile">
    <a href="{{url(\'front\')}}" id="logo" title="{{ t(\'Home\') }}">
      <img src="{{ base_path() ~ drupal_get_path(\'theme\', \'shurtape\')}}/images/logo_mobile_left.png" alt="{{ t(\'Home\') }}"/>
    </a>
    <a href="tel:888-442-8273" id="logo_cta" title="{{ t(\'Questions/Orders? Call Us: 888-442-8273\')}}">
      <img src="{{base_path() ~ drupal_get_path(\'theme\', \'shurtape\') }}/images/logo_mobile_right.png" alt="{{ t(\'Questions/Orders? Call Us: 888-442-8273\')}}"/>
    </a>
  </div>',
      'admin_title' => 'logo_bar mobile',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3dbda090-7d73-4de6-bb14-b5752bd6312a';
    $display->content['new-3dbda090-7d73-4de6-bb14-b5752bd6312a'] = $pane;
    $display->panels['before_header'][0] = 'new-3dbda090-7d73-4de6-bb14-b5752bd6312a';
    $pane = new stdClass();
    $pane->pid = 'new-28fe3cb6-f9fb-4e3e-8de9-7ec13db7da3a';
    $pane->panel = 'header';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => ' <a href="{{url(\'<front>\')}}" id="logo" title="{{ t(\'Home\') }}">
      <img src="{{ base_path() ~ drupal_get_path(\'theme\', \'shurtape\') }}/images/logo_large.png" alt="{{ t(\'Home\') }}"/>
</a>',
      'admin_title' => 'Logo',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '28fe3cb6-f9fb-4e3e-8de9-7ec13db7da3a';
    $display->content['new-28fe3cb6-f9fb-4e3e-8de9-7ec13db7da3a'] = $pane;
    $display->panels['header'][0] = 'new-28fe3cb6-f9fb-4e3e-8de9-7ec13db7da3a';
    $pane = new stdClass();
    $pane->pid = 'new-9c744ff8-8630-4bad-a6e9-afb85666e58c';
    $pane->panel = 'header';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '      <div id="top_cta">
        Questions/Orders? Call Us: <b class="blue">888-442-8273</b>
      </div>',
      'admin_title' => 'top_cta',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9c744ff8-8630-4bad-a6e9-afb85666e58c';
    $display->content['new-9c744ff8-8630-4bad-a6e9-afb85666e58c'] = $pane;
    $display->panels['header'][1] = 'new-9c744ff8-8630-4bad-a6e9-afb85666e58c';
    $pane = new stdClass();
    $pane->pid = 'new-5d775d24-1f94-445c-b41f-211175100201';
    $pane->panel = 'header';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '<div id="search_box">
        <form action="{{ base_path()}}search/products" method="get">
          <div>
            <div class="container-inline">
              <h2 class="element-invisible">Search form</h2>

              <div class="form-item form-type-textfield form-item-search-block-form">
                <label class="element-invisible" for="edit-search-block-form--2">Search </label>
                <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_api_views_fulltext" value="" size="15" maxlength="128" class="form-text">
              </div>
              <div class="form-actions form-wrapper" id="edit-actions">
                <input type="submit" id="edit-submit" class="form-submit"/>
              </div>
            </div>
          </div>
        </form>
      </div>',
      'admin_title' => 'search_box',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '5d775d24-1f94-445c-b41f-211175100201';
    $display->content['new-5d775d24-1f94-445c-b41f-211175100201'] = $pane;
    $display->panels['header'][2] = 'new-5d775d24-1f94-445c-b41f-211175100201';
    $pane = new stdClass();
    $pane->pid = 'new-dd28408f-821e-4629-9230-4fce025b7bf3';
    $pane->panel = 'header';
    $pane->type = 'desktop_header_nav';
    $pane->subtype = 'desktop_header_nav';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '3600',
        'granularity' => 'none',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'dd28408f-821e-4629-9230-4fce025b7bf3';
    $display->content['new-dd28408f-821e-4629-9230-4fce025b7bf3'] = $pane;
    $display->panels['header'][3] = 'new-dd28408f-821e-4629-9230-4fce025b7bf3';
    $pane = new stdClass();
    $pane->pid = 'new-e5d6acb2-fd07-4c64-a95a-cadefe1bfd28';
    $pane->panel = 'header';
    $pane->type = 'block';
    $pane->subtype = 'locale-language_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'e5d6acb2-fd07-4c64-a95a-cadefe1bfd28';
    $display->content['new-e5d6acb2-fd07-4c64-a95a-cadefe1bfd28'] = $pane;
    $display->panels['header'][4] = 'new-e5d6acb2-fd07-4c64-a95a-cadefe1bfd28';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-3dbda090-7d73-4de6-bb14-b5752bd6312a';
  $mini->display = $display;
  $export['common_header'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'common_uber_header';
  $mini->category = 'Layout';
  $mini->admin_title = 'common uber header';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'shurtape-layout__uber-header';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'footer_desktop__top' => NULL,
      'footer_desktop__bottom' => NULL,
      'footer_desktop__first' => NULL,
      'footer_desktop__second' => NULL,
      'footer_desktop__third' => NULL,
      'footer_desktop__fourth' => NULL,
      'footer_desktop__fifth' => NULL,
      'footer_mobile' => NULL,
      'top' => NULL,
      'header_mobile' => NULL,
      'mobile_nav' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'b7e32e74-0a01-4654-893f-cf0ab1901fd4';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3b419b7a-f673-4c0e-9598-abc386fa7283';
    $pane->panel = 'header_mobile';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '<div class="control_bar clearfix">
    <a href="#" class="menu_control">
      <img src="/sites/all/themes/shurtape/images/control_mobile_menu.png" alt=""/>
    </a>
    <a href="/contact-us" title="Contact" class="tab_control">
      <img src="/sites/all/themes/shurtape/images/btn_mobile_tab_contact.png" alt=""/>
    </a>
  </div>',
      'admin_title' => 'control_bar',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3b419b7a-f673-4c0e-9598-abc386fa7283';
    $display->content['new-3b419b7a-f673-4c0e-9598-abc386fa7283'] = $pane;
    $display->panels['header_mobile'][0] = 'new-3b419b7a-f673-4c0e-9598-abc386fa7283';
    $pane = new stdClass();
    $pane->pid = 'new-7661940a-67a8-4d3e-9536-e6d9e4f6a57e';
    $pane->panel = 'mobile_nav';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '<div class="search_box">
    <form action="/search/products" method="get">
				<span class="input_wrap">
					<input title="Enter the terms you wish to search for." type="text" name="search_api_views_fulltext" value="" maxlength="128" class="form-text">
				</span>
      <a href="#" class="submit_this_form">

      </a>
    </form>
  </div>',
      'admin_title' => 'search_box',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7661940a-67a8-4d3e-9536-e6d9e4f6a57e';
    $display->content['new-7661940a-67a8-4d3e-9536-e6d9e4f6a57e'] = $pane;
    $display->panels['mobile_nav'][0] = 'new-7661940a-67a8-4d3e-9536-e6d9e4f6a57e';
    $pane = new stdClass();
    $pane->pid = 'new-1f59a542-98f5-4221-96b0-0f01fdcba526';
    $pane->panel = 'mobile_nav';
    $pane->type = 'mobile_main_menu';
    $pane->subtype = 'mobile_main_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '3600',
        'granularity' => 'none',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '1f59a542-98f5-4221-96b0-0f01fdcba526';
    $display->content['new-1f59a542-98f5-4221-96b0-0f01fdcba526'] = $pane;
    $display->panels['mobile_nav'][1] = 'new-1f59a542-98f5-4221-96b0-0f01fdcba526';
    $pane = new stdClass();
    $pane->pid = 'new-edb15352-7bdb-4b1a-860b-681a118f9443';
    $pane->panel = 'mobile_nav';
    $pane->type = 'mobile_products_menu';
    $pane->subtype = 'mobile_products_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'edb15352-7bdb-4b1a-860b-681a118f9443';
    $display->content['new-edb15352-7bdb-4b1a-860b-681a118f9443'] = $pane;
    $display->panels['mobile_nav'][2] = 'new-edb15352-7bdb-4b1a-860b-681a118f9443';
    $pane = new stdClass();
    $pane->pid = 'new-a4ce5eb7-2741-4465-a3c3-d3696acbf7af';
    $pane->panel = 'mobile_nav';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '<div id="mobile_nav_footer">
    <a href="/" id="logo" title="{{\'Home\'|t}}">
      <img src="{{ base_path() ~ drupal_get_path(\'theme\', \'shurtape\')}}/images/logo_footer_mobile_left.png" alt="{{\'Home\'|t}}"/>
    </a>
    <a href="tel:888-442-8273" id="cta_mobile_nav" title="{{\'Questions/Orders? Call Us: 888-442-8273\'|t}}">
      <img src="{{ base_path() ~ drupal_get_path(\'theme\', \'shurtape\')}}/images/cta_mobile_nav.png" alt="{{\'Questions/Orders? Call Us: 888-442-8273\'|t}}"/>
    </a>
  </div>',
      'admin_title' => 'mobile_nav_footer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'a4ce5eb7-2741-4465-a3c3-d3696acbf7af';
    $display->content['new-a4ce5eb7-2741-4465-a3c3-d3696acbf7af'] = $pane;
    $display->panels['mobile_nav'][3] = 'new-a4ce5eb7-2741-4465-a3c3-d3696acbf7af';
    $pane = new stdClass();
    $pane->pid = 'new-453ddfae-62b1-45b4-ad67-204d803f121d';
    $pane->panel = 'top';
    $pane->type = 'd\\Panels\\ContentTypes\\Twig';
    $pane->subtype = 'd\\Panels\\ContentTypes\\Twig';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'content' => '<div class="nav_overlay"></div>',
      'admin_title' => 'nav_overlay',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '453ddfae-62b1-45b4-ad67-204d803f121d';
    $display->content['new-453ddfae-62b1-45b4-ad67-204d803f121d'] = $pane;
    $display->panels['top'][0] = 'new-453ddfae-62b1-45b4-ad67-204d803f121d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['common_uber_header'] = $mini;

  return $export;
}
