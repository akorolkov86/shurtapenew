<?php
/**
 * @file
 * promotion_page.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function promotion_page_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'Promotion form template',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '7d0b7279-b3a3-49da-8ff9-b25811bc8e89',
  'type' => 'webform',
  'language' => 'und',
  'created' => 1448886336,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '4a220085-e863-4a90-b98c-7bdbd0c238ec',
  'revision_uid' => 1,
  'body' => array(),
  'title_field' => array(
    'und' => array(
      0 => array(
        'value' => 'Promotion form template',
        'format' => NULL,
        'safe_value' => 'Promotion form template',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'translations' => array(
    'original' => 'und',
    'data' => array(
      'und' => array(
        'entity_type' => 'node',
        'entity_id' => 13353,
        'revision_id' => 13358,
        'language' => 'und',
        'source' => '',
        'uid' => 0,
        'status' => 1,
        'translate' => 0,
        'created' => 1448879103,
        'changed' => 1448879103,
      ),
    ),
  ),
  'webform' => array(
    'nid' => 13353,
    'confirmation' => '',
    'confirmation_format' => 'full_html',
    'redirect_url' => 'promotion/[node:nid]/thank-you',
    'status' => 1,
    'block' => 0,
    'allow_draft' => 0,
    'auto_save' => 0,
    'submit_notice' => 0,
    'submit_text' => '',
    'submit_limit' => -1,
    'submit_interval' => -1,
    'total_submit_limit' => -1,
    'total_submit_interval' => -1,
    'progressbar_bar' => 0,
    'progressbar_page_number' => 0,
    'progressbar_percent' => 0,
    'progressbar_pagebreak_labels' => 0,
    'progressbar_include_confirmation' => 0,
    'progressbar_label_first' => '',
    'progressbar_label_confirmation' => '',
    'preview' => 0,
    'preview_next_button_label' => '',
    'preview_prev_button_label' => '',
    'preview_title' => '',
    'preview_message' => '',
    'preview_message_format' => 'full_html',
    'preview_excluded_components' => array(),
    'next_serial' => 1,
    'confidential' => 0,
    'machine_name' => 'promotion_form_template',
    'record_exists' => TRUE,
    'roles' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
    ),
    'emails' => array(),
    'components' => array(
      1 => array(
        'nid' => 13353,
        'cid' => 1,
        'pid' => 0,
        'form_key' => 'contact_name',
        'name' => 'Name',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 32,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'description_above' => FALSE,
          'placeholder' => '',
          'analysis' => FALSE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:1:#title',
          ),
        ),
        'required' => 1,
        'weight' => 19,
        'machine_name' => 'request_a_sample__name',
        'page_num' => 1,
      ),
      3 => array(
        'nid' => 13353,
        'cid' => 3,
        'pid' => 0,
        'form_key' => 'email',
        'name' => 'Email',
        'type' => 'email',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 31,
          'disabled' => 0,
          'unique' => 0,
          'description' => '',
          'attributes' => array(),
          'multiple' => 0,
          'format' => 'short',
          'description_above' => FALSE,
          'placeholder' => '',
          'analysis' => FALSE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:3:#title',
          ),
        ),
        'required' => 1,
        'weight' => 20,
        'machine_name' => 'request_a_sample__email',
        'page_num' => 1,
      ),
      21 => array(
        'nid' => 13353,
        'cid' => 21,
        'pid' => 0,
        'form_key' => 'name_error_msg',
        'name' => 'Name error msg',
        'type' => 'markup',
        'value' => '<p class="promotion_error_message"><span style="color: #ff0000;">Please enter your name.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
          'display_on' => 'form',
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:21:#markup',
          ),
        ),
        'required' => 0,
        'weight' => 21,
        'machine_name' => 'shurgrip_free_roll_offer__name_error_msg',
        'page_num' => 1,
      ),
      17 => array(
        'nid' => 13353,
        'cid' => 17,
        'pid' => 0,
        'form_key' => 'email_error_message',
        'name' => 'Email Error message',
        'type' => 'markup',
        'value' => '<p class="promotion_error_message"><span style="color: #ff0000;">Please enter a valid email address.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
          'display_on' => 'form',
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:17:#markup',
          ),
        ),
        'required' => 0,
        'weight' => 22,
        'machine_name' => 'shurgrip_free_roll_offer__email_error_message',
        'page_num' => 1,
      ),
      13 => array(
        'nid' => 13353,
        'cid' => 13,
        'pid' => 0,
        'form_key' => 'shipping_address',
        'name' => 'Shipping Address',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 32,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'description_above' => FALSE,
          'placeholder' => '',
          'analysis' => FALSE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:13:#title',
          ),
        ),
        'required' => 1,
        'weight' => 23,
        'machine_name' => 'shurgrip_free_roll_offer__shipping_address',
        'page_num' => 1,
      ),
      2 => array(
        'nid' => 13353,
        'cid' => 2,
        'pid' => 0,
        'form_key' => 'address2',
        'name' => 'Address 2',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 32,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'description_above' => FALSE,
          'placeholder' => '',
          'analysis' => FALSE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:2:#title',
          ),
        ),
        'required' => 0,
        'weight' => 24,
        'machine_name' => 'request_a_sample__mailing_address',
        'page_num' => 1,
      ),
      22 => array(
        'nid' => 13353,
        'cid' => 22,
        'pid' => 0,
        'form_key' => 'shipping_error_msg',
        'name' => 'Shipping error msg',
        'type' => 'markup',
        'value' => '<p><span style="color: #ff0000;">Please enter a shipping address.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
          'display_on' => 'form',
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:22:#markup',
          ),
        ),
        'required' => 0,
        'weight' => 25,
        'machine_name' => 'shurgrip_free_roll_offer__shipping_error_msg',
        'page_num' => 1,
      ),
      4 => array(
        'nid' => 13353,
        'cid' => 4,
        'pid' => 0,
        'form_key' => 'city',
        'name' => 'City',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 7,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'description_above' => FALSE,
          'placeholder' => '',
          'analysis' => FALSE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:4:#title',
          ),
        ),
        'required' => 1,
        'weight' => 26,
        'machine_name' => 'request_a_sample__phone',
        'page_num' => 1,
      ),
      11 => array(
        'nid' => 13353,
        'cid' => 11,
        'pid' => 0,
        'form_key' => 'states',
        'name' => 'State',
        'type' => 'select',
        'value' => 0,
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'aslist' => 1,
          'optrand' => 0,
          'items' => '0|State:
AL|Alabama
AK|Alaska
AS|American Samoa
AZ|Arizona
AR|Arkansas
CA|California
CO|Colorado
CT|Connecticut
DE|Delaware
DC|District of Columbia
FL|Florida
GA|Georgia
GU|Guam
HI|Hawaii
ID|Idaho
IL|Illinois
IN|Indiana
IA|Iowa
KS|Kansas
KY|Kentucky
LA|Louisiana
ME|Maine
MH|Marshall Islands
MD|Maryland
MA|Massachusetts
MI|Michigan
MN|Minnesota
MS|Mississippi
MO|Missouri
MT|Montana
NE|Nebraska
NV|Nevada
NH|New Hampshire
NJ|New Jersey
NM|New Mexico
NY|New York
NC|North Carolina
ND|North Dakota
MP|Northern Marianas Islands
OH|Ohio
OK|Oklahoma
OR|Oregon
PW|Palau
PA|Pennsylvania
PR|Puerto Rico
RI|Rhode Island
SC|South Carolina
SD|South Dakota
TN|Tennessee
TX|Texas
UT|Utah
VT|Vermont
VI|Virgin Islands
VA|Virginia
WA|Washington
WV|West Virginia
WI|Wisconsin
WY|Wyoming
',
          'multiple' => 0,
          'custom_keys' => 1,
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'options_source' => '',
          'empty_option' => '',
          'description_above' => FALSE,
          'analysis' => TRUE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#title',
            1 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-0',
            2 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-AL',
            3 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-AK',
            4 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-AS',
            5 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-AZ',
            6 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-AR',
            7 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-CA',
            8 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-CO',
            9 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-CT',
            10 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-DE',
            11 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-DC',
            12 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-FL',
            13 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-GA',
            14 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-GU',
            15 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-HI',
            16 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-ID',
            17 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-IL',
            18 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-IN',
            19 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-IA',
            20 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-KS',
            21 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-KY',
            22 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-LA',
            23 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-ME',
            24 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MH',
            25 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MD',
            26 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MA',
            27 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MI',
            28 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MN',
            29 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MS',
            30 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MO',
            31 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MT',
            32 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-NE',
            33 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-NV',
            34 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-NH',
            35 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-NJ',
            36 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-NM',
            37 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-NY',
            38 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-NC',
            39 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-ND',
            40 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-MP',
            41 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-OH',
            42 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-OK',
            43 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-OR',
            44 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-PW',
            45 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-PA',
            46 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-PR',
            47 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-RI',
            48 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-SC',
            49 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-SD',
            50 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-TN',
            51 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-TX',
            52 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-UT',
            53 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-VT',
            54 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-VI',
            55 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-VA',
            56 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-WA',
            57 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-WV',
            58 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-WI',
            59 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:11:#options-WY',
          ),
        ),
        'required' => 1,
        'weight' => 27,
        'machine_name' => 'shurgrip_free_roll_offer__states',
        'page_num' => 1,
      ),
      6 => array(
        'nid' => 13353,
        'cid' => 6,
        'pid' => 0,
        'form_key' => 'zip',
        'name' => 'Zip',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 6,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'description_above' => FALSE,
          'placeholder' => '',
          'analysis' => FALSE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:6:#title',
          ),
        ),
        'required' => 1,
        'weight' => 28,
        'machine_name' => 'shurgrip_free_roll_offer__zip',
        'page_num' => 1,
      ),
      23 => array(
        'nid' => 13353,
        'cid' => 23,
        'pid' => 0,
        'form_key' => 'city_error_msg',
        'name' => 'City error msg',
        'type' => 'markup',
        'value' => '<p><span style="color: #ff0000;">Please enter your city and select a state.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
          'display_on' => 'form',
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:23:#markup',
          ),
        ),
        'required' => 0,
        'weight' => 29,
        'machine_name' => 'shurgrip_free_roll_offer__city_error_msg',
        'page_num' => 1,
      ),
      20 => array(
        'nid' => 13353,
        'cid' => 20,
        'pid' => 0,
        'form_key' => 'zip_error_message',
        'name' => 'ZIP Error message',
        'type' => 'markup',
        'value' => '<p class="promotion_error_message"><span style="color: #ff0000;">Please enter a ZIP code.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
          'display_on' => 'form',
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:20:#markup',
          ),
        ),
        'required' => 0,
        'weight' => 30,
        'machine_name' => 'shurgrip_free_roll_offer__zip_error_message',
        'page_num' => 1,
      ),
      14 => array(
        'nid' => 13353,
        'cid' => 14,
        'pid' => 0,
        'form_key' => 'custom_options_1',
        'name' => 'Custom Options 1',
        'type' => 'select',
        'value' => 1,
        'extra' => array(
          'title_display' => 'none',
          'private' => 0,
          'aslist' => 1,
          'optrand' => 0,
          'items' => '1|Select a Tape Sample
',
          'multiple' => 0,
          'custom_keys' => 0,
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'options_source' => '',
          'empty_option' => '',
          'description_above' => FALSE,
          'analysis' => TRUE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:14:#title',
            1 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:14:#options-1',
          ),
        ),
        'required' => 1,
        'weight' => 31,
        'machine_name' => 'promotion_form_template__custom_options_1',
        'page_num' => 1,
      ),
      15 => array(
        'nid' => 13353,
        'cid' => 15,
        'pid' => 0,
        'form_key' => 'custom_options_2',
        'name' => 'Custom Options 2',
        'type' => 'select',
        'value' => 1,
        'extra' => array(
          'title_display' => 'none',
          'private' => 0,
          'aslist' => 1,
          'optrand' => 0,
          'items' => '1|Select a Job Function
',
          'multiple' => 0,
          'custom_keys' => 0,
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'options_source' => '',
          'empty_option' => '',
          'description_above' => FALSE,
          'analysis' => TRUE,
          'translated_strings' => array(
            0 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:15:#title',
            1 => 'webform:4a220085-e863-4a90-b98c-7bdbd0c238ec:15:#options-1',
          ),
        ),
        'required' => 1,
        'weight' => 32,
        'machine_name' => 'promotion_form_template__custom_options_2',
        'page_num' => 1,
      ),
    ),
    'conditionals' => array(),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'locations' => array(),
  'location' => array(),
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2015-11-30 07:25:36 -0500',
);
  return $nodes;
}
