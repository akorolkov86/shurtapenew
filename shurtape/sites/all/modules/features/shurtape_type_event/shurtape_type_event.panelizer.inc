<?php
/**
 * @file
 * shurtape_type_event.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_event_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:event:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'event';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'content' => NULL,
      'title' => NULL,
      'default' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'breadcrumb_section' => NULL,
      'bottom' => NULL,
    ),
    'content' => array(
      'style' => 'naked',
    ),
    'title' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'b7891fcb-fdcf-45ef-bf77-d252f6addce1';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a6aef57b-14ca-497a-86bc-94e6216676b6';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a6aef57b-14ca-497a-86bc-94e6216676b6';
    $display->content['new-a6aef57b-14ca-497a-86bc-94e6216676b6'] = $pane;
    $display->panels['content'][0] = 'new-a6aef57b-14ca-497a-86bc-94e6216676b6';
    $pane = new stdClass();
    $pane->pid = 'new-bb570d1a-88ed-4954-9f90-d265e47d471d';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_booth';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'bb570d1a-88ed-4954-9f90-d265e47d471d';
    $display->content['new-bb570d1a-88ed-4954-9f90-d265e47d471d'] = $pane;
    $display->panels['content'][1] = 'new-bb570d1a-88ed-4954-9f90-d265e47d471d';
    $pane = new stdClass();
    $pane->pid = 'new-f0c44429-04bc-4d8e-80f1-ac730b746bf8';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_time_date';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'f0c44429-04bc-4d8e-80f1-ac730b746bf8';
    $display->content['new-f0c44429-04bc-4d8e-80f1-ac730b746bf8'] = $pane;
    $display->panels['content'][2] = 'new-f0c44429-04bc-4d8e-80f1-ac730b746bf8';
    $pane = new stdClass();
    $pane->pid = 'new-cd524ee9-8ca8-4c3a-8846-428534bb1fc1';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_location';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'cd524ee9-8ca8-4c3a-8846-428534bb1fc1';
    $display->content['new-cd524ee9-8ca8-4c3a-8846-428534bb1fc1'] = $pane;
    $display->panels['content'][3] = 'new-cd524ee9-8ca8-4c3a-8846-428534bb1fc1';
    $pane = new stdClass();
    $pane->pid = 'new-8c1dbd5a-3e02-4c50-ac99-169b2117a337';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_event_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_link' => '',
        'image_style' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '8c1dbd5a-3e02-4c50-ac99-169b2117a337';
    $display->content['new-8c1dbd5a-3e02-4c50-ac99-169b2117a337'] = $pane;
    $display->panels['content'][4] = 'new-8c1dbd5a-3e02-4c50-ac99-169b2117a337';
    $pane = new stdClass();
    $pane->pid = 'new-833ff052-6391-4638-81ab-d2dbf9ff5f6f';
    $pane->panel = 'title';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'h1',
      'id' => '',
      'class' => 'headline',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '833ff052-6391-4638-81ab-d2dbf9ff5f6f';
    $display->content['new-833ff052-6391-4638-81ab-d2dbf9ff5f6f'] = $pane;
    $display->panels['title'][0] = 'new-833ff052-6391-4638-81ab-d2dbf9ff5f6f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-a6aef57b-14ca-497a-86bc-94e6216676b6';
  $panelizer->display = $display;
  $export['node:event:default'] = $panelizer;

  return $export;
}
