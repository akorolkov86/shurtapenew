<?php
/**
 * @file
 * blog_phase_1.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function blog_phase_1_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-resources-menu_blog:blog
  $menu_links['menu-resources-menu_blog:blog'] = array(
    'menu_name' => 'menu-resources-menu',
    'link_path' => 'blog',
    'router_path' => 'blog',
    'link_title' => 'Blog',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-resources-menu_blog:blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'menu-resources-menu_resources:node/855',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Blog');


  return $menu_links;
}
