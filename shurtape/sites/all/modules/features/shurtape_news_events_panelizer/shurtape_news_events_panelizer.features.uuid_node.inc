<?php
/**
 * @file
 * shurtape_news_events_panelizer.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function shurtape_news_events_panelizer_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => 'News & Events',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'page',
  'language' => 'und',
  'created' => 1382051678,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '3d3d3534-8737-4492-be31-e90a61e4c947',
  'body' => array(),
  'title_field' => array(
    'und' => array(
      0 => array(
        'value' => 'News & Events',
        'format' => NULL,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'translations' => array(
    'original' => 'und',
    'data' => array(
      'und' => array(
        'entity_type' => 'node',
        'language' => 'und',
        'source' => '',
        'status' => 1,
        'translate' => 0,
        'created' => 1448968908,
        'entity_uuid' => '3d3d3534-8737-4492-be31-e90a61e4c947',
        'user_uuid' => 'a6b0fabd-8bd4-4019-b895-b6408db74827',
      ),
    ),
  ),
  'panelizer' => array(
    'page_manager' => array(
      'entity_type' => 'node',
      'name' => NULL,
      'no_blocks' => 0,
      'css_id' => '',
      'css' => '',
      'pipeline' => 'standard',
      'contexts' => array(),
      'relationships' => array(),
      'view_mode' => 'page_manager',
      'css_class' => '',
      'title_element' => 'H2',
      'link_to_entity' => 1,
      'extra' => array(),
      'did_uuid' => '3d9c89ad-c514-444a-8c0d-c65d5f0d0c7e',
    ),
  ),
  'comment_count' => 0,
  'locations' => array(),
  'location' => array(),
  'date' => '2013-10-17 19:14:38 -0400',
  'user_uuid' => 'f75e5bba-00c7-4141-b3b5-66cb6631d6ab',
);
  $nodes[] = array(
  'title' => 'Locations',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'page',
  'language' => 'und',
  'created' => 1382649660,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'aa0a08e9-335c-46c4-880b-53919fe74ba2',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Our products are available in virtually every corner of the world.</p><div class="map text-center"><img src="/sites/default/files/content/images/globalmap_orange_Catalog.jpg" alt="Shurtape office locations" width="695" height="364" /></div>',
        'summary' => '',
        'format' => 'full_html',
        'safe_summary' => '',
      ),
    ),
  ),
  'title_field' => array(
    'und' => array(
      0 => array(
        'value' => 'Locations',
        'format' => NULL,
      ),
    ),
  ),
  'metatags' => array(
    'und' => array(
      'robots' => array(
        'value' => array(
          'index' => 0,
          'follow' => 0,
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'translations' => array(
    'original' => 'und',
    'data' => array(
      'und' => array(
        'entity_type' => 'node',
        'language' => 'und',
        'source' => '',
        'status' => 1,
        'translate' => 0,
        'created' => 1382649660,
        'entity_uuid' => 'aa0a08e9-335c-46c4-880b-53919fe74ba2',
        'user_uuid' => 'f75e5bba-00c7-4141-b3b5-66cb6631d6ab',
      ),
    ),
  ),
  'panelizer' => array(
    'page_manager' => array(
      'entity_type' => 'node',
      'name' => NULL,
      'no_blocks' => 0,
      'css_id' => '',
      'css' => '',
      'pipeline' => 'standard',
      'contexts' => array(),
      'relationships' => array(),
      'view_mode' => 'page_manager',
      'css_class' => '',
      'title_element' => 'H2',
      'link_to_entity' => 1,
      'extra' => array(),
      'did_uuid' => '3d9c89ad-c514-444a-8c0d-c65d5f0d0c7e',
    ),
  ),
  'comment_count' => 0,
  'locations' => array(),
  'location' => array(),
  'pathauto_perform_alias' => FALSE,
  'date' => '2013-10-24 17:21:00 -0400',
  'user_uuid' => 'f75e5bba-00c7-4141-b3b5-66cb6631d6ab',
);
  $nodes[] = array(
  'title' => 'Careers',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'page',
  'language' => 'und',
  'created' => 1382679981,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'ba63592d-c79c-4195-85a9-32b22f6b6661',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<iframe id="careers-iframe" style="margin-right: auto; margin-left: auto; display: block;" title="CAREER SEARCH" name="CAREER SEARCH" src="http://www.shurtapetechnologies.appone.com" frameborder="no" scrolling="no"></iframe>',
        'summary' => '',
        'format' => 'php_code',
      ),
    ),
  ),
  'title_field' => array(
    'und' => array(
      0 => array(
        'value' => 'Careers',
        'format' => NULL,
      ),
    ),
  ),
  'metatags' => array(
    'und' => array(
      'robots' => array(
        'value' => array(
          'index' => 0,
          'follow' => 0,
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 0,
  ),
  'translations' => array(
    'original' => 'und',
    'data' => array(
      'und' => array(
        'entity_type' => 'node',
        'language' => 'und',
        'source' => '',
        'status' => 1,
        'translate' => 0,
        'created' => 1382679981,
        'entity_uuid' => 'ba63592d-c79c-4195-85a9-32b22f6b6661',
        'user_uuid' => 'a6b0fabd-8bd4-4019-b895-b6408db74827',
      ),
    ),
  ),
  'comment_count' => 0,
  'locations' => array(),
  'location' => array(),
  'pathauto_perform_alias' => FALSE,
  'date' => '2013-10-25 01:46:21 -0400',
  'user_uuid' => 'a6b0fabd-8bd4-4019-b895-b6408db74827',
);
  return $nodes;
}
