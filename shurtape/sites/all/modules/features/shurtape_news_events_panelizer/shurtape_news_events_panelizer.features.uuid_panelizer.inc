<?php
/**
 * @file
 * shurtape_news_events_panelizer.features.uuid_panelizer.inc
 */

/**
 * Implements hook_uuid_features_default_panelizer().
 */
function shurtape_news_events_panelizer_uuid_features_default_panelizer() {
  $uuid_panelizer = array();

  // Modified panelizer display for node 3d3d3534-8737-4492-be31-e90a61e4c947.
  $display = new panels_display();
  $display->layout = 'page_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'title' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'action_links' => NULL,
      'breadcrumb_section' => NULL,
      'bottom' => NULL,
    ),
    'content' => array(
      'style' => 'naked',
    ),
    'title' => array(
      'style' => 'naked',
    ),
    'breadcrumb_section' => array(
      'style' => 'naked',
    ),
    'content_top' => array(
      'style' => 'naked',
    ),
    'help' => array(
      'style' => 'naked',
    ),
    'bottom' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '3d9c89ad-c514-444a-8c0d-c65d5f0d0c7e';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $uuid_panelizer[] = $display;
  // Modified panelizer display for node aa0a08e9-335c-46c4-880b-53919fe74ba2.
  $display = new panels_display();
  $display->layout = 'page_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'title' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'action_links' => NULL,
      'breadcrumb_section' => NULL,
      'bottom' => NULL,
    ),
    'content' => array(
      'style' => 'naked',
    ),
    'title' => array(
      'style' => 'naked',
    ),
    'breadcrumb_section' => array(
      'style' => 'naked',
    ),
    'content_top' => array(
      'style' => 'naked',
    ),
    'help' => array(
      'style' => 'naked',
    ),
    'bottom' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '3d9c89ad-c514-444a-8c0d-c65d5f0d0c7e';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0be46ff1-27e9-4ba6-a5c5-acb59c519311';
    $pane->panel = 'bottom';
    $pane->type = 'block';
    $pane->subtype = 'avis_all-locations';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0be46ff1-27e9-4ba6-a5c5-acb59c519311';
    $display->content['new-0be46ff1-27e9-4ba6-a5c5-acb59c519311'] = $pane;
    $display->panels['bottom'][0] = 'new-0be46ff1-27e9-4ba6-a5c5-acb59c519311';
    $pane = new stdClass();
    $pane->pid = 'new-2cad7f53-992c-495f-95b5-86bd2a13d645';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2cad7f53-992c-495f-95b5-86bd2a13d645';
    $display->content['new-2cad7f53-992c-495f-95b5-86bd2a13d645'] = $pane;
    $display->panels['content'][0] = 'new-2cad7f53-992c-495f-95b5-86bd2a13d645';
    $pane = new stdClass();
    $pane->pid = 'new-a05bf0dc-139c-4725-99cb-d9e9b1ccfa49';
    $pane->panel = 'title';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'h1',
      'id' => '',
      'class' => 'headline',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a05bf0dc-139c-4725-99cb-d9e9b1ccfa49';
    $display->content['new-a05bf0dc-139c-4725-99cb-d9e9b1ccfa49'] = $pane;
    $display->panels['title'][0] = 'new-a05bf0dc-139c-4725-99cb-d9e9b1ccfa49';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-2cad7f53-992c-495f-95b5-86bd2a13d645';
  $uuid_panelizer[] = $display;

  return $uuid_panelizer;
}
