<?php
/**
 * @file
 * translation_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function translation_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: devel.
  $menus['devel'] = array(
    'menu_name' => 'devel',
    'title' => 'Development',
    'description' => 'Development link',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: features.
  $menus['features'] = array(
    'menu_name' => 'features',
    'title' => 'Features',
    'description' => 'Menu items for any enabled features.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: management.
  $menus['management'] = array(
    'menu_name' => 'management',
    'title' => 'Management',
    'description' => 'The <em>Management</em> menu contains links for administrative tasks.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-about-footer-menu.
  $menus['menu-about-footer-menu'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'title' => 'About footer menu',
    'description' => 'About menu for the footer navigation',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-about-menu.
  $menus['menu-about-menu'] = array(
    'menu_name' => 'menu-about-menu',
    'title' => 'About header menu',
    'description' => 'About menu for the header navigation',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-contact-menu.
  $menus['menu-contact-menu'] = array(
    'menu_name' => 'menu-contact-menu',
    'title' => 'Contact menu',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-metadata.
  $menus['menu-metadata'] = array(
    'menu_name' => 'menu-metadata',
    'title' => 'Metadata',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-product-detail-menu.
  $menus['menu-product-detail-menu'] = array(
    'menu_name' => 'menu-product-detail-menu',
    'title' => 'Product Detail Menu',
    'description' => ' ',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-products-menu.
  $menus['menu-products-menu'] = array(
    'menu_name' => 'menu-products-menu',
    'title' => 'Products menu',
    'description' => 'Products menu for the header and footer navigation',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: navigation.
  $menus['navigation'] = array(
    'menu_name' => 'navigation',
    'title' => 'Navigation',
    'description' => 'The <em>Navigation</em> menu contains links intended for site visitors. Links are added to the <em>Navigation</em> menu automatically by some modules.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t(' ');
  t('About footer menu');
  t('About header menu');
  t('About menu for the footer navigation');
  t('About menu for the header navigation');
  t('Contact menu');
  t('Development');
  t('Development link');
  t('Features');
  t('Main menu');
  t('Management');
  t('Menu items for any enabled features.');
  t('Metadata');
  t('Navigation');
  t('Product Detail Menu');
  t('Products menu');
  t('Products menu for the header and footer navigation');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('The <em>Management</em> menu contains links for administrative tasks.');
  t('The <em>Navigation</em> menu contains links intended for site visitors. Links are added to the <em>Navigation</em> menu automatically by some modules.');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('User menu');

  return $menus;
}
