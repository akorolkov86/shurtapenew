<?php
/**
 * @file
 * translation_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function translation_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-about-footer-menu_about:node/1.
  $menu_links['menu-about-footer-menu_about:node/1'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'About',
    'options' => array(
      'identifier' => 'menu-about-footer-menu_about:node/1',
      'attributes' => array(
        'title' => 'About',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-about-footer-menu_careers:node/428.
  $menu_links['menu-about-footer-menu_careers:node/428'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'node/428',
    'router_path' => 'node/%',
    'link_title' => 'Careers',
    'options' => array(
      'identifier' => 'menu-about-footer-menu_careers:node/428',
      'attributes' => array(
        'title' => 'Careers',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-footer-menu_about:node/1',
  );
  // Exported menu link: menu-about-footer-menu_company-history:node/467.
  $menu_links['menu-about-footer-menu_company-history:node/467'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'node/467',
    'router_path' => 'node/%',
    'link_title' => 'Company History',
    'options' => array(
      'identifier' => 'menu-about-footer-menu_company-history:node/467',
      'attributes' => array(
        'title' => 'Company History',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-footer-menu_about:node/1',
  );
  // Exported menu link: menu-about-footer-menu_human-resources:node/427.
  $menu_links['menu-about-footer-menu_human-resources:node/427'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'node/427',
    'router_path' => 'node/%',
    'link_title' => 'Human Resources',
    'options' => array(
      'identifier' => 'menu-about-footer-menu_human-resources:node/427',
      'attributes' => array(
        'title' => 'Human Resources',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-footer-menu_about:node/1',
  );
  // Exported menu link: menu-about-footer-menu_locations:node/401.
  $menu_links['menu-about-footer-menu_locations:node/401'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'node/401',
    'router_path' => 'node/%',
    'link_title' => 'Locations',
    'options' => array(
      'identifier' => 'menu-about-footer-menu_locations:node/401',
      'attributes' => array(
        'title' => 'Locations',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-footer-menu_about:node/1',
  );
  // Exported menu link: menu-about-footer-menu_merchandise-return-policy:node/6867.
  $menu_links['menu-about-footer-menu_merchandise-return-policy:node/6867'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'node/6867',
    'router_path' => 'node/%',
    'link_title' => 'Merchandise Return Policy',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-about-footer-menu_merchandise-return-policy:node/6867',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -42,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-footer-menu_about:node/1',
  );
  // Exported menu link: menu-about-footer-menu_privacy-policy:node/465.
  $menu_links['menu-about-footer-menu_privacy-policy:node/465'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'node/465',
    'router_path' => 'node/%',
    'link_title' => 'Privacy Policy',
    'options' => array(
      'identifier' => 'menu-about-footer-menu_privacy-policy:node/465',
      'attributes' => array(
        'title' => 'Privacy Policy',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-footer-menu_about:node/1',
  );
  // Exported menu link: menu-about-footer-menu_terms-and-conditions:node/466.
  $menu_links['menu-about-footer-menu_terms-and-conditions:node/466'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'node/466',
    'router_path' => 'node/%',
    'link_title' => 'Terms and Conditions',
    'options' => array(
      'identifier' => 'menu-about-footer-menu_terms-and-conditions:node/466',
      'attributes' => array(
        'title' => 'Terms & Conditions',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-footer-menu_about:node/1',
  );
  // Exported menu link: menu-about-footer-menu_where-to-buy:store-locator.
  $menu_links['menu-about-footer-menu_where-to-buy:store-locator'] = array(
    'menu_name' => 'menu-about-footer-menu',
    'link_path' => 'store-locator',
    'router_path' => 'store-locator',
    'link_title' => 'Where to Buy',
    'options' => array(
      'identifier' => 'menu-about-footer-menu_where-to-buy:store-locator',
      'attributes' => array(
        'title' => 'Where to Buy',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-footer-menu_about:node/1',
  );
  // Exported menu link: menu-about-menu_about:node/1.
  $menu_links['menu-about-menu_about:node/1'] = array(
    'menu_name' => 'menu-about-menu',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'About',
    'options' => array(
      'identifier' => 'menu-about-menu_about:node/1',
      'alter' => TRUE,
      'attributes' => array(
        'title' => 'About',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-about-menu_careers:node/428.
  $menu_links['menu-about-menu_careers:node/428'] = array(
    'menu_name' => 'menu-about-menu',
    'link_path' => 'node/428',
    'router_path' => 'node/%',
    'link_title' => 'Careers',
    'options' => array(
      'identifier' => 'menu-about-menu_careers:node/428',
      'attributes' => array(
        'title' => 'Careers',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-menu_about:node/1',
  );
  // Exported menu link: menu-about-menu_company-history:node/467.
  $menu_links['menu-about-menu_company-history:node/467'] = array(
    'menu_name' => 'menu-about-menu',
    'link_path' => 'node/467',
    'router_path' => 'node/%',
    'link_title' => 'Company History',
    'options' => array(
      'identifier' => 'menu-about-menu_company-history:node/467',
      'attributes' => array(
        'title' => 'Company History',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-menu_about:node/1',
  );
  // Exported menu link: menu-about-menu_company-profile:node/1.
  $menu_links['menu-about-menu_company-profile:node/1'] = array(
    'menu_name' => 'menu-about-menu',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'Company Profile',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-about-menu_company-profile:node/1',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-menu_about:node/1',
  );
  // Exported menu link: menu-about-menu_human-resources:node/427.
  $menu_links['menu-about-menu_human-resources:node/427'] = array(
    'menu_name' => 'menu-about-menu',
    'link_path' => 'node/427',
    'router_path' => 'node/%',
    'link_title' => 'Human Resources',
    'options' => array(
      'identifier' => 'menu-about-menu_human-resources:node/427',
      'attributes' => array(
        'title' => 'Human Resources',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-menu_about:node/1',
  );
  // Exported menu link: menu-about-menu_locations:node/401.
  $menu_links['menu-about-menu_locations:node/401'] = array(
    'menu_name' => 'menu-about-menu',
    'link_path' => 'node/401',
    'router_path' => 'node/%',
    'link_title' => 'Locations',
    'options' => array(
      'identifier' => 'menu-about-menu_locations:node/401',
      'attributes' => array(
        'title' => 'Locations',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-menu_about:node/1',
  );
  // Exported menu link: menu-about-menu_where-to-buy:store-locator.
  $menu_links['menu-about-menu_where-to-buy:store-locator'] = array(
    'menu_name' => 'menu-about-menu',
    'link_path' => 'store-locator',
    'router_path' => 'store-locator',
    'link_title' => 'Where to Buy',
    'options' => array(
      'identifier' => 'menu-about-menu_where-to-buy:store-locator',
      'attributes' => array(
        'title' => 'Where to Buy',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-about-menu_about:node/1',
  );
  // Exported menu link: menu-product-detail-menu_data-sheets:<front>.
  $menu_links['menu-product-detail-menu_data-sheets:<front>'] = array(
    'menu_name' => 'menu-product-detail-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Data Sheets',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'class' => array(
          0 => 'btn',
          1 => 'btn-orange',
          2 => 'find_data_sheet',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-product-detail-menu_data-sheets:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-product-detail-menu_find-a-distributor:store-locator.
  $menu_links['menu-product-detail-menu_find-a-distributor:store-locator'] = array(
    'menu_name' => 'menu-product-detail-menu',
    'link_path' => 'store-locator',
    'router_path' => 'store-locator',
    'link_title' => 'Find A Distributor',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'btn',
          1 => 'btn-orange',
        ),
      ),
      'identifier' => 'menu-product-detail-menu_find-a-distributor:store-locator',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-product-detail-menu_question:<front>.
  $menu_links['menu-product-detail-menu_question:<front>'] = array(
    'menu_name' => 'menu-product-detail-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Question',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-product-detail-menu_question:<front>',
      'attributes' => array(
        'name' => 'question_popout',
        'class' => array(
          0 => 'btn',
          1 => 'open_request_popout',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-product-detail-menu_request-a-sample:<front>.
  $menu_links['menu-product-detail-menu_request-a-sample:<front>'] = array(
    'menu_name' => 'menu-product-detail-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Request A Sample',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'btn',
          1 => 'open_request_popout',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-product-detail-menu_request-a-sample:<front>',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-product-detail-menu_request-sample:<front>.
  $menu_links['menu-product-detail-menu_request-sample:<front>'] = array(
    'menu_name' => 'menu-product-detail-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Request Sample',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-product-detail-menu_request-sample:<front>',
      'attributes' => array(
        'name' => 'request_sample_popout',
        'class' => array(
          0 => 'btn',
          1 => 'open_request_popout',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-product-detail-menu_use-the-product-locator:node/429.
  $menu_links['menu-product-detail-menu_use-the-product-locator:node/429'] = array(
    'menu_name' => 'menu-product-detail-menu',
    'link_path' => 'node/429',
    'router_path' => 'node/%',
    'link_title' => 'Use The Product Locator',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'btn',
        ),
      ),
      'identifier' => 'menu-product-detail-menu_use-the-product-locator:node/429',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-products-menu_green-point-products:node/861.
  $menu_links['menu-products-menu_green-point-products:node/861'] = array(
    'menu_name' => 'menu-products-menu',
    'link_path' => 'node/861',
    'router_path' => 'node/%',
    'link_title' => 'Green Point Products',
    'options' => array(
      'identifier' => 'menu-products-menu_green-point-products:node/861',
      'attributes' => array(
        'title' => 'Green Point Products',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-products-menu_products:products',
  );
  // Exported menu link: menu-products-menu_market:products/markets.
  $menu_links['menu-products-menu_market:products/markets'] = array(
    'menu_name' => 'menu-products-menu',
    'link_path' => 'products/markets',
    'router_path' => 'products/%',
    'link_title' => 'Market',
    'options' => array(
      'identifier' => 'menu-products-menu_market:products/markets',
      'attributes' => array(
        'title' => 'Market',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-products-menu_products:products',
  );
  // Exported menu link: menu-products-menu_product-index:node/429.
  $menu_links['menu-products-menu_product-index:node/429'] = array(
    'menu_name' => 'menu-products-menu',
    'link_path' => 'node/429',
    'router_path' => 'node/%',
    'link_title' => 'Product Index',
    'options' => array(
      'identifier' => 'menu-products-menu_product-index:node/429',
      'attributes' => array(
        'title' => 'Product Index',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-products-menu_products:products',
  );
  // Exported menu link: menu-products-menu_products:products.
  $menu_links['menu-products-menu_products:products'] = array(
    'menu_name' => 'menu-products-menu',
    'link_path' => 'products',
    'router_path' => 'products',
    'link_title' => 'Products',
    'options' => array(
      'identifier' => 'menu-products-menu_products:products',
      'attributes' => array(
        'title' => 'Products',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-products-menu_type:products/type.
  $menu_links['menu-products-menu_type:products/type'] = array(
    'menu_name' => 'menu-products-menu',
    'link_path' => 'products/type',
    'router_path' => 'products/%',
    'link_title' => 'Type',
    'options' => array(
      'identifier' => 'menu-products-menu_type:products/type',
      'attributes' => array(
        'title' => 'Type',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-products-menu_products:products',
  );
  // Exported menu link: menu-resources-menu_blog:blog.
  $menu_links['menu-resources-menu_blog:blog'] = array(
    'menu_name' => 'menu-resources-menu',
    'link_path' => 'blog',
    'router_path' => 'blog',
    'link_title' => 'Blog',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-resources-menu_blog:blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-resources-menu_resources:node/855',
  );
  // Exported menu link: menu-resources-menu_industry-links:resources/industry-links.
  $menu_links['menu-resources-menu_industry-links:resources/industry-links'] = array(
    'menu_name' => 'menu-resources-menu',
    'link_path' => 'resources/industry-links',
    'router_path' => 'resources/industry-links',
    'link_title' => 'Industry Links',
    'options' => array(
      'identifier' => 'menu-resources-menu_industry-links:resources/industry-links',
      'attributes' => array(
        'title' => 'Industry Links',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-resources-menu_resources:node/855',
  );
  // Exported menu link: menu-resources-menu_media-library:node/855.
  $menu_links['menu-resources-menu_media-library:node/855'] = array(
    'menu_name' => 'menu-resources-menu',
    'link_path' => 'node/855',
    'router_path' => 'node/%',
    'link_title' => 'Media Library',
    'options' => array(
      'identifier' => 'menu-resources-menu_media-library:node/855',
      'attributes' => array(
        'title' => 'Media Library',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-resources-menu_resources:node/855',
  );
  // Exported menu link: menu-resources-menu_news--events:node/29.
  $menu_links['menu-resources-menu_news--events:node/29'] = array(
    'menu_name' => 'menu-resources-menu',
    'link_path' => 'node/29',
    'router_path' => 'node/%',
    'link_title' => 'News & Events',
    'options' => array(
      'identifier' => 'menu-resources-menu_news--events:node/29',
      'attributes' => array(
        'title' => 'News & Events',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-resources-menu_resources:node/855',
  );
  // Exported menu link: menu-resources-menu_product-stewardship:node/425.
  $menu_links['menu-resources-menu_product-stewardship:node/425'] = array(
    'menu_name' => 'menu-resources-menu',
    'link_path' => 'node/425',
    'router_path' => 'node/%',
    'link_title' => 'Product Stewardship',
    'options' => array(
      'identifier' => 'menu-resources-menu_product-stewardship:node/425',
      'attributes' => array(
        'title' => 'Product Stewardship',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'menu-resources-menu_resources:node/855',
  );
  // Exported menu link: menu-resources-menu_resources:node/855.
  $menu_links['menu-resources-menu_resources:node/855'] = array(
    'menu_name' => 'menu-resources-menu',
    'link_path' => 'node/855',
    'router_path' => 'node/%',
    'link_title' => 'Resources',
    'options' => array(
      'identifier' => 'menu-resources-menu_resources:node/855',
      'attributes' => array(
        'title' => 'Resources',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About');
  t('Blog');
  t('Careers');
  t('Company History');
  t('Company Profile');
  t('Data Sheets');
  t('Find A Distributor');
  t('Green Point Products');
  t('Human Resources');
  t('Industry Links');
  t('Locations');
  t('Market');
  t('Media Library');
  t('Merchandise Return Policy');
  t('News & Events');
  t('Privacy Policy');
  t('Product Index');
  t('Product Stewardship');
  t('Products');
  t('Question');
  t('Request A Sample');
  t('Request Sample');
  t('Resources');
  t('Terms and Conditions');
  t('Type');
  t('Use The Product Locator');
  t('Where to Buy');

  return $menu_links;
}
