<?php
/**
 * @file
 * shurtape_type_media_assets_info.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_media_assets_info_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:media_asset_infographic:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'media_asset_infographic';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_media_asset_infographic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'title' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'action_links' => NULL,
      'breadcrumb_section' => NULL,
      'content' => NULL,
      'bottom' => NULL,
      'img_with_link' => NULL,
    ),
    'img_with_link' => array(
      'style' => 'naked',
    ),
    'content' => array(
      'style' => 'naked',
    ),
    'title' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '0c5985f9-e234-4846-9e7c-4b829a9a0926';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b63c96db-2a40-4960-a60f-084b6a96889a';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_media_type';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'taxonomy_term_reference_link',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b63c96db-2a40-4960-a60f-084b6a96889a';
    $display->content['new-b63c96db-2a40-4960-a60f-084b6a96889a'] = $pane;
    $display->panels['content'][0] = 'new-b63c96db-2a40-4960-a60f-084b6a96889a';
    $pane = new stdClass();
    $pane->pid = 'new-9f3ab6bb-5c9f-48c7-a0df-a2ad8d9b95c5';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_social_share_description';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9f3ab6bb-5c9f-48c7-a0df-a2ad8d9b95c5';
    $display->content['new-9f3ab6bb-5c9f-48c7-a0df-a2ad8d9b95c5'] = $pane;
    $display->panels['content'][1] = 'new-9f3ab6bb-5c9f-48c7-a0df-a2ad8d9b95c5';
    $pane = new stdClass();
    $pane->pid = 'new-9e0413a3-f871-4acc-a7f2-0b756abc2b60';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_share_by_mail_subject';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '9e0413a3-f871-4acc-a7f2-0b756abc2b60';
    $display->content['new-9e0413a3-f871-4acc-a7f2-0b756abc2b60'] = $pane;
    $display->panels['content'][2] = 'new-9e0413a3-f871-4acc-a7f2-0b756abc2b60';
    $pane = new stdClass();
    $pane->pid = 'new-bdcaf23f-05da-4cf4-b321-466c62c2dfc0';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_share_by_mail_body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'bdcaf23f-05da-4cf4-b321-466c62c2dfc0';
    $display->content['new-bdcaf23f-05da-4cf4-b321-466c62c2dfc0'] = $pane;
    $display->panels['content'][3] = 'new-bdcaf23f-05da-4cf4-b321-466c62c2dfc0';
    $pane = new stdClass();
    $pane->pid = 'new-75e8956a-3c3c-41c5-8698-581edaa368c8';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_asset_weight';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '75e8956a-3c3c-41c5-8698-581edaa368c8';
    $display->content['new-75e8956a-3c3c-41c5-8698-581edaa368c8'] = $pane;
    $display->panels['content'][4] = 'new-75e8956a-3c3c-41c5-8698-581edaa368c8';
    $pane = new stdClass();
    $pane->pid = 'new-eb4b36cb-dfea-4915-9a33-a98aa3879d4a';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_thumbnail_brand_asset';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'eb4b36cb-dfea-4915-9a33-a98aa3879d4a';
    $display->content['new-eb4b36cb-dfea-4915-9a33-a98aa3879d4a'] = $pane;
    $display->panels['content'][5] = 'new-eb4b36cb-dfea-4915-9a33-a98aa3879d4a';
    $pane = new stdClass();
    $pane->pid = 'new-7fb23ce8-da13-4f3e-9311-7a0fbcbde291';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_brand_asset_weight';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '7fb23ce8-da13-4f3e-9311-7a0fbcbde291';
    $display->content['new-7fb23ce8-da13-4f3e-9311-7a0fbcbde291'] = $pane;
    $display->panels['content'][6] = 'new-7fb23ce8-da13-4f3e-9311-7a0fbcbde291';
    $pane = new stdClass();
    $pane->pid = 'new-5efe81d3-d154-42c0-bc82-6e9906335c90';
    $pane->panel = 'img_with_link';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_infographic_full_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_link' => 'file',
        'image_style' => '',
      ),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5efe81d3-d154-42c0-bc82-6e9906335c90';
    $display->content['new-5efe81d3-d154-42c0-bc82-6e9906335c90'] = $pane;
    $display->panels['img_with_link'][0] = 'new-5efe81d3-d154-42c0-bc82-6e9906335c90';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:media_asset_infographic:default'] = $panelizer;

  return $export;
}
