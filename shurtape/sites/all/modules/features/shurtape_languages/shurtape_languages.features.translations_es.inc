<?php
/**
 * @file
 * shurtape_languages.features.translations_es.inc
 */

/**
 * Implements hook_translations_es_defaults().
 */
function shurtape_languages_translations_es_defaults() {
  $translations = array();
  $translatables = array();
  $translations['es:default']['d2c3276528824e1a368567685cf61f62'] = array(
    'source' => 'No matter what line of work you\'re in, Shurtape gets the job done.',
    'context' => '',
    'location' => '/es',
    'translation' => 'No matter what line of work you\'re in, Shurtape gets the job done.s',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('No matter what line of work you\'re in, Shurtape gets the job done.', array(), array('context' => ''));
  return $translations;
}
