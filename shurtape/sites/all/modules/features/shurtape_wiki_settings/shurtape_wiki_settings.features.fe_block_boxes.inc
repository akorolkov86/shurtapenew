<?php
/**
 * @file
 * shurtape_wiki_settings.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function shurtape_wiki_settings_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Book\'s';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'books_block';
  $fe_block_boxes->body = '<p><a href="/dashboard/books">Book\'s page</a></p>';

  $export['books_block'] = $fe_block_boxes;

  return $export;
}
