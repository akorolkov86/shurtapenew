<?php
/**
 * @file
 * shurtape_wiki_settings.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function shurtape_wiki_settings_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view any about_us content'.
  $permissions['view any about_us content'] = array(
    'name' => 'view any about_us content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any article content'.
  $permissions['view any article content'] = array(
    'name' => 'view any article content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any blog content'.
  $permissions['view any blog content'] = array(
    'name' => 'view any blog content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any book content'.
  $permissions['view any book content'] = array(
    'name' => 'view any book content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any contact_us content'.
  $permissions['view any contact_us content'] = array(
    'name' => 'view any contact_us content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any distributor content'.
  $permissions['view any distributor content'] = array(
    'name' => 'view any distributor content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any event content'.
  $permissions['view any event content'] = array(
    'name' => 'view any event content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any footer_logos content'.
  $permissions['view any footer_logos content'] = array(
    'name' => 'view any footer_logos content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any home_social_icons content'.
  $permissions['view any home_social_icons content'] = array(
    'name' => 'view any home_social_icons content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any industry_link content'.
  $permissions['view any industry_link content'] = array(
    'name' => 'view any industry_link content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any location content'.
  $permissions['view any location content'] = array(
    'name' => 'view any location content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any location_item content'.
  $permissions['view any location_item content'] = array(
    'name' => 'view any location_item content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any media_asset_infographic content'.
  $permissions['view any media_asset_infographic content'] = array(
    'name' => 'view any media_asset_infographic content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any media_library_asset content'.
  $permissions['view any media_library_asset content'] = array(
    'name' => 'view any media_library_asset content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any media_library_logos content'.
  $permissions['view any media_library_logos content'] = array(
    'name' => 'view any media_library_logos content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any media_library_video content'.
  $permissions['view any media_library_video content'] = array(
    'name' => 'view any media_library_video content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any page content'.
  $permissions['view any page content'] = array(
    'name' => 'view any page content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any panel content'.
  $permissions['view any panel content'] = array(
    'name' => 'view any panel content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any product content'.
  $permissions['view any product content'] = array(
    'name' => 'view any product content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any product_asset content'.
  $permissions['view any product_asset content'] = array(
    'name' => 'view any product_asset content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any product_image content'.
  $permissions['view any product_image content'] = array(
    'name' => 'view any product_image content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any promotion_page content'.
  $permissions['view any promotion_page content'] = array(
    'name' => 'view any promotion_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any region_contact content'.
  $permissions['view any region_contact content'] = array(
    'name' => 'view any region_contact content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any simplenews content'.
  $permissions['view any simplenews content'] = array(
    'name' => 'view any simplenews content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any vignette_slider content'.
  $permissions['view any vignette_slider content'] = array(
    'name' => 'view any vignette_slider content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  // Exported permission: 'view any webform content'.
  $permissions['view any webform content'] = array(
    'name' => 'view any webform content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'nodetype_access',
  );

  return $permissions;
}
