<?php
/**
 * @file
 * shurtape_type_blog.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_blog_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:blog:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'blog';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'title' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'breadcrumb_section' => NULL,
      'content' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '769f1528-3ccc-415a-8749-4d28c7adc9e0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-03337800-c47e-4f03-b177-6b32e5def147';
    $pane->panel = 'bottom';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Shurtape Blog title',
      'title' => '',
      'body' => '<h2>Shurtape Blog</h2>',
      'format' => 'php_code',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '03337800-c47e-4f03-b177-6b32e5def147';
    $display->content['new-03337800-c47e-4f03-b177-6b32e5def147'] = $pane;
    $display->panels['bottom'][0] = 'new-03337800-c47e-4f03-b177-6b32e5def147';
    $pane = new stdClass();
    $pane->pid = 'new-63fb3c69-9293-4ad6-b525-3d09784efaf7';
    $pane->panel = 'bottom';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_category';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '63fb3c69-9293-4ad6-b525-3d09784efaf7';
    $display->content['new-63fb3c69-9293-4ad6-b525-3d09784efaf7'] = $pane;
    $display->panels['bottom'][1] = 'new-63fb3c69-9293-4ad6-b525-3d09784efaf7';
    $pane = new stdClass();
    $pane->pid = 'new-0d051302-0df8-4f79-8a52-f873b982d957';
    $pane->panel = 'bottom';
    $pane->type = 'node_created';
    $pane->subtype = 'node_created';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'format' => 'simple_date_with_no_time',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '0d051302-0df8-4f79-8a52-f873b982d957';
    $display->content['new-0d051302-0df8-4f79-8a52-f873b982d957'] = $pane;
    $display->panels['bottom'][2] = 'new-0d051302-0df8-4f79-8a52-f873b982d957';
    $pane = new stdClass();
    $pane->pid = 'new-816abcd1-9be6-4bfa-829a-24344d67e1f7';
    $pane->panel = 'bottom';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_main_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '',
        'image_link' => '',
      ),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '816abcd1-9be6-4bfa-829a-24344d67e1f7';
    $display->content['new-816abcd1-9be6-4bfa-829a-24344d67e1f7'] = $pane;
    $display->panels['bottom'][3] = 'new-816abcd1-9be6-4bfa-829a-24344d67e1f7';
    $pane = new stdClass();
    $pane->pid = 'new-b1d50083-1da2-42fe-9e4d-4710b4fd147c';
    $pane->panel = 'bottom';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'trim_length' => 400,
      ),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'b1d50083-1da2-42fe-9e4d-4710b4fd147c';
    $display->content['new-b1d50083-1da2-42fe-9e4d-4710b4fd147c'] = $pane;
    $display->panels['bottom'][4] = 'new-b1d50083-1da2-42fe-9e4d-4710b4fd147c';
    $pane = new stdClass();
    $pane->pid = 'new-c972a562-fab2-4d3a-b576-940d801029b7';
    $pane->panel = 'bottom';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_additional_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '',
        'image_link' => '',
      ),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'c972a562-fab2-4d3a-b576-940d801029b7';
    $display->content['new-c972a562-fab2-4d3a-b576-940d801029b7'] = $pane;
    $display->panels['bottom'][5] = 'new-c972a562-fab2-4d3a-b576-940d801029b7';
    $pane = new stdClass();
    $pane->pid = 'new-3e12b3b8-36d8-4527-83ea-98ed0dae46a2';
    $pane->panel = 'bottom';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_youtube_video_id';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '3e12b3b8-36d8-4527-83ea-98ed0dae46a2';
    $display->content['new-3e12b3b8-36d8-4527-83ea-98ed0dae46a2'] = $pane;
    $display->panels['bottom'][6] = 'new-3e12b3b8-36d8-4527-83ea-98ed0dae46a2';
    $pane = new stdClass();
    $pane->pid = 'new-ea674ed0-deb8-4693-8079-90e084d92c3d';
    $pane->panel = 'bottom';
    $pane->type = 'node_comment_form';
    $pane->subtype = 'node_comment_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'anon_links' => 0,
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = 'ea674ed0-deb8-4693-8079-90e084d92c3d';
    $display->content['new-ea674ed0-deb8-4693-8079-90e084d92c3d'] = $pane;
    $display->panels['bottom'][7] = 'new-ea674ed0-deb8-4693-8079-90e084d92c3d';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-63fb3c69-9293-4ad6-b525-3d09784efaf7';
  $panelizer->display = $display;
  $export['node:blog:default'] = $panelizer;

  return $export;
}
