<?php
/**
 * @file
 * shurtape_simplenews_newsletter_vignette_slider.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function shurtape_simplenews_newsletter_vignette_slider_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_industry_link_group'.
  $field_bases['field_industry_link_group'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_industry_link_group',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'industry_links',
          'parent' => 0,
        ),
      ),
      'entity_translation_sync' => FALSE,
      'options_list_callback' => 'i18n_taxonomy_allowed_values',
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_link_markup'.
  $field_bases['field_link_markup'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_link_markup',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 1,
    'type' => 'text_long',
    'views_natural_sort_enable_sort' => 0,
  );

  return $field_bases;
}
