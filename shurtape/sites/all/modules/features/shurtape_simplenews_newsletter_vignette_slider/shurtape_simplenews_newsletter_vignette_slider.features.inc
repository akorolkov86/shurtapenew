<?php
/**
 * @file
 * shurtape_simplenews_newsletter_vignette_slider.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function shurtape_simplenews_newsletter_vignette_slider_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function shurtape_simplenews_newsletter_vignette_slider_node_info() {
  $items = array(
    'industry_link' => array(
      'name' => t('Industry Link'),
      'base' => 'node_content',
      'description' => t('Use this to add links to the Industry Links page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'simplenews' => array(
      'name' => t('Simplenews newsletter'),
      'base' => 'node_content',
      'description' => t('A newsletter issue to be sent to subscribed email addresses.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'vignette_slider' => array(
      'name' => t('Vignette Slider'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Vignette Slider'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
