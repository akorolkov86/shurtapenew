<?php
/**
 * @file
 * shurtape_type_product.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function shurtape_type_product_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'physical_properties';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Physical Properties';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Physical Properties';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Field collection item: Entity with the Physical Properties (field_physical_properties) */
  $handler->display->display_options['relationships']['field_physical_properties_node']['id'] = 'field_physical_properties_node';
  $handler->display->display_options['relationships']['field_physical_properties_node']['table'] = 'field_collection_item';
  $handler->display->display_options['relationships']['field_physical_properties_node']['field'] = 'field_physical_properties_node';
  $handler->display->display_options['relationships']['field_physical_properties_node']['required'] = TRUE;
  /* Field: Field collection item: Property Type */
  $handler->display->display_options['fields']['field_property_type']['id'] = 'field_property_type';
  $handler->display->display_options['fields']['field_property_type']['table'] = 'field_data_field_property_type';
  $handler->display->display_options['fields']['field_property_type']['field'] = 'field_property_type';
  $handler->display->display_options['fields']['field_property_type']['label'] = '';
  $handler->display->display_options['fields']['field_property_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_property_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_property_type']['type'] = 'i18n_taxonomy_term_reference_plain';
  /* Field: Field collection item: Property Standard */
  $handler->display->display_options['fields']['field_property_standard']['id'] = 'field_property_standard';
  $handler->display->display_options['fields']['field_property_standard']['table'] = 'field_data_field_property_standard';
  $handler->display->display_options['fields']['field_property_standard']['field'] = 'field_property_standard';
  $handler->display->display_options['fields']['field_property_standard']['label'] = 'Standard';
  /* Field: Field collection item: Property Metric */
  $handler->display->display_options['fields']['field_property_metric']['id'] = 'field_property_metric';
  $handler->display->display_options['fields']['field_property_metric']['table'] = 'field_data_field_property_metric';
  $handler->display->display_options['fields']['field_property_metric']['field'] = 'field_property_metric';
  $handler->display->display_options['fields']['field_property_metric']['label'] = 'Metric';
  /* Field: Field collection item: ASTM Test Method */
  $handler->display->display_options['fields']['field_astm_test_method_1']['id'] = 'field_astm_test_method_1';
  $handler->display->display_options['fields']['field_astm_test_method_1']['table'] = 'field_data_field_astm_test_method';
  $handler->display->display_options['fields']['field_astm_test_method_1']['field'] = 'field_astm_test_method';
  $handler->display->display_options['fields']['field_astm_test_method_1']['empty'] = 'N/A';
  $handler->display->display_options['fields']['field_astm_test_method_1']['type'] = 'i18n_taxonomy_term_reference_plain';
  /* Sort criterion: Field collection item: Property Type (field_property_type) */
  $handler->display->display_options['sorts']['field_property_type_tid']['id'] = 'field_property_type_tid';
  $handler->display->display_options['sorts']['field_property_type_tid']['table'] = 'field_data_field_property_type';
  $handler->display->display_options['sorts']['field_property_type_tid']['field'] = 'field_property_type_tid';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'field_physical_properties_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['relationship'] = 'field_physical_properties_node';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    'und' => 'und',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['physical_properties'] = $view;

  $view = new view();
  $view->name = 'product_colors';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Product Colors';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Field collection item: Entity with the Sizes and Colors (field_sizes_colors) */
  $handler->display->display_options['relationships']['field_sizes_colors_node']['id'] = 'field_sizes_colors_node';
  $handler->display->display_options['relationships']['field_sizes_colors_node']['table'] = 'field_collection_item';
  $handler->display->display_options['relationships']['field_sizes_colors_node']['field'] = 'field_sizes_colors_node';
  $handler->display->display_options['relationships']['field_sizes_colors_node']['required'] = TRUE;
  /* Field: Field collection item: Dimensions */
  $handler->display->display_options['fields']['field_product_dimensions']['id'] = 'field_product_dimensions';
  $handler->display->display_options['fields']['field_product_dimensions']['table'] = 'field_data_field_product_dimensions';
  $handler->display->display_options['fields']['field_product_dimensions']['field'] = 'field_product_dimensions';
  /* Field: Field collection item: Rolls Per Case */
  $handler->display->display_options['fields']['field_product_rolls_per_case']['id'] = 'field_product_rolls_per_case';
  $handler->display->display_options['fields']['field_product_rolls_per_case']['table'] = 'field_data_field_product_rolls_per_case';
  $handler->display->display_options['fields']['field_product_rolls_per_case']['field'] = 'field_product_rolls_per_case';
  $handler->display->display_options['fields']['field_product_rolls_per_case']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_product_rolls_per_case']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );
  /* Field: Field collection item: Weight lbs/case */
  $handler->display->display_options['fields']['field_product_weight_lbs_case']['id'] = 'field_product_weight_lbs_case';
  $handler->display->display_options['fields']['field_product_weight_lbs_case']['table'] = 'field_data_field_product_weight_lbs_case';
  $handler->display->display_options['fields']['field_product_weight_lbs_case']['field'] = 'field_product_weight_lbs_case';
  $handler->display->display_options['fields']['field_product_weight_lbs_case']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_product_weight_lbs_case']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '1',
    'prefix_suffix' => 0,
  );
  /* Field: Field collection item: Cases Per Pallet */
  $handler->display->display_options['fields']['field_product_cases_per_pallet']['id'] = 'field_product_cases_per_pallet';
  $handler->display->display_options['fields']['field_product_cases_per_pallet']['table'] = 'field_data_field_product_cases_per_pallet';
  $handler->display->display_options['fields']['field_product_cases_per_pallet']['field'] = 'field_product_cases_per_pallet';
  $handler->display->display_options['fields']['field_product_cases_per_pallet']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_product_cases_per_pallet']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );
  /* Field: Field collection item: Available Colors */
  $handler->display->display_options['fields']['field_product_available_colors']['id'] = 'field_product_available_colors';
  $handler->display->display_options['fields']['field_product_available_colors']['table'] = 'field_data_field_product_available_colors';
  $handler->display->display_options['fields']['field_product_available_colors']['field'] = 'field_product_available_colors';
  $handler->display->display_options['fields']['field_product_available_colors']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_product_available_colors']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_product_available_colors']['multi_type'] = 'ul';
  /* Sort criterion: Field collection item: Dimensions (field_product_dimensions) */
  $handler->display->display_options['sorts']['field_product_dimensions_value']['id'] = 'field_product_dimensions_value';
  $handler->display->display_options['sorts']['field_product_dimensions_value']['table'] = 'field_data_field_product_dimensions';
  $handler->display->display_options['sorts']['field_product_dimensions_value']['field'] = 'field_product_dimensions_value';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'field_sizes_colors_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';

  /* Display: Product Sizes and Colors */
  $handler = $view->new_display('block', 'Product Sizes and Colors', 'block_1');
  $export['product_colors'] = $view;

  $view = new view();
  $view->name = 'product_images';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Product Images';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Product Images';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'slide';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = 'item-list slides';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content: Product Images (field_product_images) */
  $handler->display->display_options['relationships']['field_product_images_nid']['id'] = 'field_product_images_nid';
  $handler->display->display_options['relationships']['field_product_images_nid']['table'] = 'field_data_field_product_images';
  $handler->display->display_options['relationships']['field_product_images_nid']['field'] = 'field_product_images_nid';
  $handler->display->display_options['relationships']['field_product_images_nid']['required'] = TRUE;
  $handler->display->display_options['relationships']['field_product_images_nid']['delta'] = '-1';
  /* Field: Content: Product Image */
  $handler->display->display_options['fields']['field_product_image']['id'] = 'field_product_image';
  $handler->display->display_options['fields']['field_product_image']['table'] = 'field_data_field_product_image';
  $handler->display->display_options['fields']['field_product_image']['field'] = 'field_product_image';
  $handler->display->display_options['fields']['field_product_image']['relationship'] = 'field_product_images_nid';
  $handler->display->display_options['fields']['field_product_image']['label'] = '';
  $handler->display->display_options['fields']['field_product_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_product_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_product_image']['settings'] = array(
    'image_style' => 'product-detail-main-image',
    'image_link' => '',
  );
  /* Sort criterion: Content: Product Images (field_product_images:delta) */
  $handler->display->display_options['sorts']['delta']['id'] = 'delta';
  $handler->display->display_options['sorts']['delta']['table'] = 'field_data_field_product_images';
  $handler->display->display_options['sorts']['delta']['field'] = 'delta';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['default_argument_skip_url'] = TRUE;
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product' => 'product',
  );
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    'und' => 'und',
  );

  /* Display: Product Image (Large) Block */
  $handler = $view->new_display('block', 'Product Image (Large) Block', 'block_1');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  $handler->display->display_options['block_caching'] = '-1';

  /* Display: Product Image (Thumbnail) Block */
  $handler = $view->new_display('block', 'Product Image (Thumbnail) Block', 'block_2');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'control';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = 'item-list controls';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Product Image */
  $handler->display->display_options['fields']['field_product_image']['id'] = 'field_product_image';
  $handler->display->display_options['fields']['field_product_image']['table'] = 'field_data_field_product_image';
  $handler->display->display_options['fields']['field_product_image']['field'] = 'field_product_image';
  $handler->display->display_options['fields']['field_product_image']['relationship'] = 'field_product_images_nid';
  $handler->display->display_options['fields']['field_product_image']['label'] = '';
  $handler->display->display_options['fields']['field_product_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_product_image']['alter']['text'] = '<a href="#">[field_product_image]</a>';
  $handler->display->display_options['fields']['field_product_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_product_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_product_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  $export['product_images'] = $view;

  return $export;
}
