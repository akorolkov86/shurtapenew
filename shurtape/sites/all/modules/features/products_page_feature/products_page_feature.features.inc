<?php
/**
 * @file
 * products_page_feature.features.inc
 */

/**
 * Implements hook_views_api().
 */
function products_page_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
