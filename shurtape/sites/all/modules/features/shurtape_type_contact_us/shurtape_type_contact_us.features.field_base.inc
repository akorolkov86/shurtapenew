<?php
/**
 * @file
 * shurtape_type_contact_us.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function shurtape_type_contact_us_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_contact_us_'.
  $field_bases['field_contact_us_'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_contact_us_',
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'entity_translation_sync' => array(
        0 => 'fid',
      ),
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
    'views_natural_sort_enable_sort' => 0,
  );

  return $field_bases;
}
