<?php
/**
 * @file
 * shurtape_type_contact_us.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_contact_us_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:contact_us:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'contact_us';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'title' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'breadcrumb_section' => NULL,
      'content' => NULL,
      'bottom' => NULL,
    ),
    'title' => array(
      'style' => 'naked',
    ),
    'content' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'efa2f95f-5b22-4296-8f44-7d9d1f7705f1';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2b050714-3492-4f00-833b-d40ee921f544';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_contact_us_';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_link' => '',
        'image_style' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2b050714-3492-4f00-833b-d40ee921f544';
    $display->content['new-2b050714-3492-4f00-833b-d40ee921f544'] = $pane;
    $display->panels['content'][0] = 'new-2b050714-3492-4f00-833b-d40ee921f544';
    $pane = new stdClass();
    $pane->pid = 'new-ba487bc7-3ab2-4058-964a-8f70014e4b27';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ba487bc7-3ab2-4058-964a-8f70014e4b27';
    $display->content['new-ba487bc7-3ab2-4058-964a-8f70014e4b27'] = $pane;
    $display->panels['content'][1] = 'new-ba487bc7-3ab2-4058-964a-8f70014e4b27';
    $pane = new stdClass();
    $pane->pid = 'new-f33df390-9579-449c-b21c-443fad2ad7fc';
    $pane->panel = 'title';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'h1',
      'id' => '',
      'class' => 'headline',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f33df390-9579-449c-b21c-443fad2ad7fc';
    $display->content['new-f33df390-9579-449c-b21c-443fad2ad7fc'] = $pane;
    $display->panels['title'][0] = 'new-f33df390-9579-449c-b21c-443fad2ad7fc';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-2b050714-3492-4f00-833b-d40ee921f544';
  $panelizer->display = $display;
  $export['node:contact_us:default'] = $panelizer;

  return $export;
}
