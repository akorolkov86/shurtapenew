<?php
/**
 * @file
 * where_to_buy_phase_2.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function where_to_buy_phase_2_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'vid\' => \'40002\',
      \'uid\' => \'1\',
      \'title\' => \'Grainger\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'b5dfc6f3-15a5-4286-bb27-e1bff4c2fb53\',
      \'nid\' => \'39997\',
      \'type\' => \'distributor\',
      \'language\' => \'und\',
      \'created\' => \'1438853154\',
      \'changed\' => \'1438853154\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'94c49f71-cd73-485a-9696-26ad81282026\',
      \'revision_timestamp\' => \'1438853154\',
      \'revision_uid\' => \'1\',
      \'field_address\' => array(),
      \'field_distributor_type\' => array(
        \'und\' => array(
          array(
            \'value\' => \'industrial\',
          ),
        ),
      ),
      \'field_formatted_address\' => array(),
      \'field_industrial_partner\' => array(
        \'und\' => array(
          array(
            \'value\' => \'0\',
          ),
        ),
      ),
      \'field_latlng\' => array(),
      \'field_phone_number\' => array(
        \'und\' => array(
          array(
            \'value\' => \'(503) 256-3663\',
            \'format\' => NULL,
            \'safe_value\' => \'(503) 256-3663\',
          ),
        ),
      ),
      \'field_website_url\' => array(
        \'und\' => array(
          array(
            \'url\' => \'http://www.grainger.com\',
            \'title\' => NULL,
            \'attributes\' => array(),
          ),
        ),
      ),
      \'metatags\' => array(
        \'und\' => array(
          \'robots\' => array(
            \'value\' => array(
              \'index\' => 0,
              \'follow\' => 0,
              \'noindex\' => 0,
              \'nofollow\' => 0,
              \'noarchive\' => 0,
              \'nosnippet\' => 0,
              \'noodp\' => 0,
              \'noydir\' => 0,
              \'noimageindex\' => 0,
              \'notranslate\' => 0,
            ),
          ),
        ),
      ),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1438853154\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'locations\' => array(),
      \'location\' => array(),
      \'name\' => \'admin\',
      \'picture\' => \'0\',
      \'data\' => \'b:0;\',
      \'path\' => array(
        \'pid\' => \'65873\',
        \'source\' => \'node/39997\',
        \'alias\' => \'grainger-0\',
        \'language\' => \'und\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
  (object) array(
      \'vid\' => \'40003\',
      \'uid\' => \'1\',
      \'title\' => \'Johnstone Supply\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'cc576b9b-daf5-403e-b5e5-2c29ca250a3d\',
      \'nid\' => \'39998\',
      \'type\' => \'distributor\',
      \'language\' => \'und\',
      \'created\' => \'1438853154\',
      \'changed\' => \'1438853154\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'c2855dca-0a98-44fb-b593-58e9db02b6e0\',
      \'revision_timestamp\' => \'1438853154\',
      \'revision_uid\' => \'1\',
      \'field_address\' => array(),
      \'field_distributor_type\' => array(
        \'und\' => array(
          array(
            \'value\' => \'hvac\',
          ),
        ),
      ),
      \'field_formatted_address\' => array(),
      \'field_industrial_partner\' => array(
        \'und\' => array(
          array(
            \'value\' => \'0\',
          ),
        ),
      ),
      \'field_latlng\' => array(),
      \'field_phone_number\' => array(
        \'und\' => array(
          array(
            \'value\' => \'800-462-5812\',
            \'format\' => NULL,
            \'safe_value\' => \'800-462-5812\',
          ),
        ),
      ),
      \'field_website_url\' => array(
        \'und\' => array(
          array(
            \'url\' => \'http://www.johnstonesupply.com\',
            \'title\' => NULL,
            \'attributes\' => array(),
          ),
        ),
      ),
      \'metatags\' => array(
        \'und\' => array(
          \'robots\' => array(
            \'value\' => array(
              \'index\' => 0,
              \'follow\' => 0,
              \'noindex\' => 0,
              \'nofollow\' => 0,
              \'noarchive\' => 0,
              \'nosnippet\' => 0,
              \'noodp\' => 0,
              \'noydir\' => 0,
              \'noimageindex\' => 0,
              \'notranslate\' => 0,
            ),
          ),
        ),
      ),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1438853154\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'locations\' => array(),
      \'location\' => array(),
      \'name\' => \'admin\',
      \'picture\' => \'0\',
      \'data\' => \'b:0;\',
      \'path\' => array(
        \'pid\' => \'65874\',
        \'source\' => \'node/39998\',
        \'alias\' => \'johnstone-supply\',
        \'language\' => \'und\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
  (object) array(
      \'vid\' => \'40004\',
      \'uid\' => \'1\',
      \'title\' => \'Lennox\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'aa89a5d5-f411-4aac-b9c9-803f2a2d4044\',
      \'nid\' => \'39999\',
      \'type\' => \'distributor\',
      \'language\' => \'und\',
      \'created\' => \'1438853154\',
      \'changed\' => \'1438853154\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'d5b06bdb-8eb3-4458-8f53-19db849257d8\',
      \'revision_timestamp\' => \'1438853154\',
      \'revision_uid\' => \'1\',
      \'field_address\' => array(),
      \'field_distributor_type\' => array(
        \'und\' => array(
          array(
            \'value\' => \'hvac\',
          ),
        ),
      ),
      \'field_formatted_address\' => array(),
      \'field_industrial_partner\' => array(
        \'und\' => array(
          array(
            \'value\' => \'0\',
          ),
        ),
      ),
      \'field_latlng\' => array(),
      \'field_phone_number\' => array(
        \'und\' => array(
          array(
            \'value\' => \'800-462-5812\',
            \'format\' => NULL,
            \'safe_value\' => \'800-462-5812\',
          ),
        ),
      ),
      \'field_website_url\' => array(
        \'und\' => array(
          array(
            \'url\' => \'http://www.lennox.com\',
            \'title\' => NULL,
            \'attributes\' => array(),
          ),
        ),
      ),
      \'metatags\' => array(
        \'und\' => array(
          \'robots\' => array(
            \'value\' => array(
              \'index\' => 0,
              \'follow\' => 0,
              \'noindex\' => 0,
              \'nofollow\' => 0,
              \'noarchive\' => 0,
              \'nosnippet\' => 0,
              \'noodp\' => 0,
              \'noydir\' => 0,
              \'noimageindex\' => 0,
              \'notranslate\' => 0,
            ),
          ),
        ),
      ),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1438853154\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'locations\' => array(),
      \'location\' => array(),
      \'name\' => \'admin\',
      \'picture\' => \'0\',
      \'data\' => \'b:0;\',
      \'path\' => array(
        \'pid\' => \'65875\',
        \'source\' => \'node/39999\',
        \'alias\' => \'lennox\',
        \'language\' => \'und\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
  (object) array(
      \'vid\' => \'40005\',
      \'uid\' => \'1\',
      \'title\' => \'Home Depot Supply\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'d11fb715-0b5f-4347-a2fa-22236f8dfe84\',
      \'nid\' => \'40000\',
      \'type\' => \'distributor\',
      \'language\' => \'und\',
      \'created\' => \'1438853154\',
      \'changed\' => \'1438853154\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'e7d4880d-17fb-446f-944b-d389634a85a5\',
      \'revision_timestamp\' => \'1438853154\',
      \'revision_uid\' => \'1\',
      \'field_address\' => array(),
      \'field_distributor_type\' => array(
        \'und\' => array(
          array(
            \'value\' => \'industrial\',
          ),
        ),
      ),
      \'field_formatted_address\' => array(),
      \'field_industrial_partner\' => array(
        \'und\' => array(
          array(
            \'value\' => \'0\',
          ),
        ),
      ),
      \'field_latlng\' => array(),
      \'field_phone_number\' => array(
        \'und\' => array(
          array(
            \'value\' => \'800-462-5812\',
            \'format\' => NULL,
            \'safe_value\' => \'800-462-5812\',
          ),
        ),
      ),
      \'field_website_url\' => array(
        \'und\' => array(
          array(
            \'url\' => \'https://hdsupplysolutions.com\',
            \'title\' => NULL,
            \'attributes\' => array(),
          ),
        ),
      ),
      \'metatags\' => array(
        \'und\' => array(
          \'robots\' => array(
            \'value\' => array(
              \'index\' => 0,
              \'follow\' => 0,
              \'noindex\' => 0,
              \'nofollow\' => 0,
              \'noarchive\' => 0,
              \'nosnippet\' => 0,
              \'noodp\' => 0,
              \'noydir\' => 0,
              \'noimageindex\' => 0,
              \'notranslate\' => 0,
            ),
          ),
        ),
      ),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1438853154\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'locations\' => array(),
      \'location\' => array(),
      \'name\' => \'admin\',
      \'picture\' => \'0\',
      \'data\' => \'b:0;\',
      \'path\' => array(
        \'pid\' => \'65876\',
        \'source\' => \'node/40000\',
        \'alias\' => \'home-depot-supply\',
        \'language\' => \'und\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
)',
);
  return $node_export;
}
