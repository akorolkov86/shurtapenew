<?php
/**
 * @file
 * where_to_buy_phase_2.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function where_to_buy_phase_2_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create distributor content'.
  $permissions['create distributor content'] = array(
    'name' => 'create distributor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any distributor content'.
  $permissions['delete any distributor content'] = array(
    'name' => 'delete any distributor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own distributor content'.
  $permissions['delete own distributor content'] = array(
    'name' => 'delete own distributor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any distributor content'.
  $permissions['edit any distributor content'] = array(
    'name' => 'edit any distributor content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own distributor content'.
  $permissions['edit own distributor content'] = array(
    'name' => 'edit own distributor content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
