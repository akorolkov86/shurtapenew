<?php
/**
 * @file
 * where_to_buy_phase_2.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function where_to_buy_phase_2_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Phone number';
  $rule->name = 'phone_number';
  $rule->field_name = 'field_phone_number';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'distributor';
  $rule->validator = 'field_validation_phone_validator';
  $rule->settings = array(
    'country' => 'ca',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      4 => 0,
    ),
    'errors' => 0,
  );
  $rule->error_message = 'Invalid phone number.';
  $export['phone_number'] = $rule;

  return $export;
}
