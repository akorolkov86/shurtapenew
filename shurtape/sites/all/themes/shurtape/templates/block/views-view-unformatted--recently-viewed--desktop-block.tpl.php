<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>

<?php if ( !empty( $rows ) ): 
	$i=0; 
	$row_count = count( $rows ); ?>
	<?php if ( $row_count > 4 ): ?>
		<a href="#" class="control prev"></a>
	<?php endif; ?>
	<div class="slide_window clearfix">
		<?php foreach ($rows as $id => $row): ?>
			<?php echo ( $i==0 || $i % 4 == 0 ) ? '<div class="slide">' : ''; ?>
				<div class="item">
					<?php echo $row; ?>
				</div>
			<?php echo ( ($i + 1) % 4 == 0 || $i == ( $row_count - 1 ) ) ? '</div>' : ''; ?>
			<?php $i++; ?>
		<?php endforeach; ?>
	</div>
	<?php if ( $row_count > 4 ): ?>
		<a href="#" class="control next"></a>
	<?php endif; ?>
<?php endif; ?>