<h1 class="headline">
  <?php if ($title): ?>
    <?php echo $title; ?>
  <?php endif; ?>
</h1>

<?php if ($page['content_top']): ?>
  <div id="content_top">
    <?php print render($page['content_top']); ?>
  </div>
<?php endif; ?>

<?php if (isset($page['help'])): ?>
  <?php print render($page['help']); ?>
<?php endif; ?>

<?php if (isset($action_links)): ?>
  <ul class="action-links">
    <?php print render($action_links); ?>
  </ul>
<?php endif; ?>

<div class="blue_bar desktop"></div>

<div class="clearfix">

  <div id="left" class="pull-left desktop">
    <img class="desktop" src="<?php print $GLOBALS['base_url']  . '/' . drupal_get_path('theme', 'shurtape') . '/images/contact-us3.png';?>" alt="Contact Us"/>
  </div>
  <div id="right" class="pull-right">
    <?php if($_GET['q'] == 'node/46078'): ?>
      <p>
        Need help finding a distributor location that carries Shurtape products? Please complete the form below to contact
        our International Customer Service Team.
      </p>
    <?php else: ?>
    <p>
      At Shurtape, we're eager to answer any questions you have about our products or our company.
      We also love to hear your comments about your Shurtape experience.
      All feedback is welcome — we look forward to hearing from you!
      If you would like to submit an idea or invention, please <?php print l(t('click here'), 'welcome-inventors-idea-people'); ?>.
    </p>
    <? endif; ?>

    <form id="contact_form" method="post" action="#" role="form">
      <div id="form_box">
        <div class="all_errors_container"></div>
        <div class="form_wrapper">
          <?php print render($content); ?>
        </div>
      </div>
    </form>
  </div>
</div>
