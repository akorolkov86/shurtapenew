<!--
<div class="mobile_breadcrumbs mobile">
	<span class="you_are_here">You Are Here:</span>
	<span class="breadcrumbs">
		<a href="<?php print base_path(); ?>">Home</a> / 
		<a href="<?php print base_path(); ?>resources/media">Resources</a> / 
		<a href="<?php print base_path(); ?>resources/news_events">News &amp; Events</a> /
		<?php echo $title; ?>
	</span>
</div>
-->

<h1 class="headline"><?php print t('News') ?></h1>

<?php if ($page['content_top']): ?>
	<div id="content_top">
		<?php print render($page['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($page['help'])): ?>
	<?php print render($page['help']); ?>
<?php endif; ?>

<?php if (isset($action_links)): ?>
	<ul class="action-links">
		<?php print render($action_links); ?>
	</ul>
<?php endif; ?>

<div class="blue_bar desktop">&nbsp;</div>

<div class="news_detail_wrap">

	<?php if ($title): ?>
		<h2 class="news_detail_title">
			<?php echo $title; ?>
		</h2>
	<?php endif; ?>

	<?php if (!empty($content['field_sub_heading'])): ?>
		<?php $content['field_sub_heading']['#label_display'] = 'hidden'; ?>
		<h3 class="news_detail_subtitle"><?php print render($content['field_sub_heading']); ?></h3>
	<?php endif; ?>

	<div class="news_detail_content clearfix">
		<?php if (isset($content['field_image'])): ?>
		<div class="news_detail_photo pull-left">
			<?php $content['field_image']['#label_display'] = 'hidden'; ?>
			<?php print render($content['field_image']); ?>
		</div>
		<?php endif; ?>
		<div <?php if (isset($content['field_image'])): ?>class="news_detail_body pull-right"<?php endif; ?>>
		<?php $content['body']['#label_display'] = 'hidden'; ?>
		<?php print render($content['body']); ?>
	    </div>
	</div>
</div>