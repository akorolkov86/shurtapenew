<!--
<div class="mobile_breadcrumbs mobile">
	<span class="you_are_here">You Are Here:</span>
	<span class="breadcrumbs">
		<a href="<?php print base_path(); ?>">Home</a> / 
		<a href="<?php print base_path(); ?>about/profile">About</a> / 
		<?php echo $title; ?>
	</span>
</div>
-->
<?php 
exit;
?>
<h1 class="headline">
	<?php if ($title): ?>
		<?php echo t($title); ?>
	<?php endif; ?>
</h1>

<?php if ($page['content_top']): ?>
	<div id="content_top">
		<?php print render($page['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($page['help'])): ?>
	<?php print render($page['help']); ?>
<?php endif; ?>

<?php if (isset($action_links)): ?>
	<ul class="action-links">
		<?php print render($action_links); ?>
	</ul>
<?php endif; ?>

<div class="blue_bar desktop"></div>

<?php if ( strtolower( $title ) == 'company history' ): ?>
<!--	<div class="marquee_image text-center">
		<div class="desktop">
			<img src="<?php //print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/photo_historic.png" alt="Shurtape historic building" />
		</div>
		<div class="mobile">
			<img src="<?php // print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/photo_historic_mobile.jpg" alt="Shurtape historic building" />
		</div>
	</div> -->
<?php endif; ?>
	
<div class="padded_body">
	<?php print render( $content ); ?>
</div>
