<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="product_index_heading">
	<div class="desktop">
		<div class="col product_name">
			<?php print t('Product Name'); ?>
		</div>
		<div class="col product_description">
			<?php print t('Description'); ?>
		</div>
		<div class="col tech_sheet">
			<?php print t('Technical Data Sheet'); ?>
		</div>
	</div>
	<div class="mobile">
		<div class="blue_bar">
			 <?php print t('Product Name'); ?> / <?php print t('Description'); ?> / <?php print t('Technical Data Sheet'); ?> 
		</div>
	</div>
</div>
<?php foreach ($rows as $id => $row): ?>
	<div<?php if ($classes_array[$id]): print ' class="' . $classes_array[$id] .'"'; endif; ?>>
		<?php print $row; ?>
	</div>
<?php endforeach; ?>
<script type="text/javascript">
/* define $ as jQuery just in case */
(function($)
{
	/* doc ready */
	$(function()
	{
		/**	
		 * remove blank buttons without a data sheet link - product index page
		 */
		function remove_blank_buttons()
		{
			var items = $('.view-product-index').find('.views-row');
			if (items.length > 0)
			{
				$.each(items, function(i) 
				{
					if (!$(items[i]).find('.grid_item').attr('href'))
					{
						$(items[i]).find('.download_link').hide();
					}
				});	
			}
		}
	
		/* fire remove blanj=k buttons function on page load */
		$(window).load(function()
		{
			remove_blank_buttons();
		});
	});
})(jQuery);
</script>
