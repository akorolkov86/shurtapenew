<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
<?php if (!empty($title)) : ?>
	<h3><?php print $title; ?></h3>
<?php endif; ?>
<?php print $list_type_prefix; ?>
<?php foreach ($rows as $id => $row): ?>
	<li class="<?php print $classes_array[$id]; ?>">
		<span class="mobile_link_icon mobile">
			<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
		</span>
		<?php print $row; ?>
	</li>
<?php endforeach; ?>
<?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>
<script type="text/javascript">
/* define $ as jQuery just in case */
( function( $ )
{
	/* doc ready */
	$( function( )
	{
		/* click events on li since the markup is all wrong for block anchors */
		/* using hammer.js now */
		$('.view-greenpoint-products ul li').hammer({ }).on( 'tap doubletap hold', function( e ) 
		{ 
			var target_link = $( this ).find( '.views-field-title a' ).attr( 'href' );
			window.location = target_link;
		});
	});
})( jQuery );
</script>