<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php if ($output != "") {?>
	<?php 
	$vid 	= $output;
	$url 	= 'http://gdata.youtube.com/feeds/api/videos/' . $vid;
	$doc 	= new DOMDocument;
	$doc->load( $url );
	$vtitle = $doc->getElementsByTagName( 'title' )->item( 0 )->nodeValue; 
	
	/* get the node alias path */
	$alias = "/".drupal_get_path_alias("node/".$row->nid);
	
	?>
	<a target="_blank" href="<?php echo $alias;?>" class="video_thumb " rel="<?php echo $vid; ?>">
		<span class="play_overlay"></span>
		<img src="//img.youtube.com/vi/<?php echo $vid; ?>/0.jpg" alt="" width="160" height="120" />
		<span class="vid_title"><?php echo $vtitle; ?></span>
	</a>
<?php } ?>