<?php

/**
 * @file
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<div class="location_results">
	<div class="clearfix">
		<?php if ( $rows && ! empty( $rows ) ): ?>
			<?php if( $title==1 ) :?>
			
			<?php else: ?>
				<span class="spacer">&nbsp;</span>
			<?php endif ?>
			<div class="results">
				<?php foreach ($rows as $id => $row): ?>
					<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
						<?php print $row; ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
	(function ($) 
	{
		Drupal.behaviors.gmap = {
			attach: function (context, settings) 
			{
				$(document).ready(function()
				{
					$('.location_results .results .views-row').each(function(i)
					{
						$(this).bind('click', function()
						{
							google.maps.event.trigger(
								Drupal.settings.gmap.auto1map.markers[i].marker, 'click'
							);
						});
					});
				});
			}
		};
	})(jQuery);
</script>
