<?php

/**
 * @file
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<div class="padded_body">
	<div class="results_section clearfix">
		<?php if ( $rows && ! empty( $rows ) ): ?>
			<?php $title = $title == 1 ? 'Partner Results' : 'Results'; ?>
			<h2><?php print $title; ?></h2>
			<div class="results_grid row">
				<?php foreach ($rows as $row_number => $columns): ?>
					<?php $i=0; foreach ($columns as $column_number => $item): $i++; ?>
						<div class="item col-md-4<?php echo ($i%3 == 0) ? ' last' : ''; ?>">
							<?php print $item; ?>
						</div>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>