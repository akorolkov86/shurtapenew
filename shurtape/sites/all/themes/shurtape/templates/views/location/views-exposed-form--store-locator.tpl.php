<?php

/**
 * @file
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $sort_by: The select box to sort the view using an exposed form.
 * - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
 * - $items_per_page: The select box with the available items per page. May be optional.
 * - $offset: A textfield to define the offset of the view. May be optional.
 * - $reset_button: A button to reset the exposed filter applied. May be optional.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
?>
<?php 

/* This ensures that, if clean URLs are off, the 'q' is added first so that it shows up first in the URL. */
if (!empty($q)):
	print $q;
endif; 
?>

<div class="location_filters">
	<div class="form_wrap clearfix">
		<h2>Find a U.S. location</h2>
		<div class="views-exposed-form">
			<div class="views-exposed-widgets clearfix">
				<?php foreach ($widgets as $id => $widget): ?>
					<div id="<?php print $widget->id; ?>-wrapper" class="views-exposed-widget views-widget-<?php print $id; ?>">
						<?php if (!empty($widget->operator)): ?>
							<div class="views-operator">
								<?php print $widget->operator; ?>
							</div>
						<?php endif; ?>
						<div class="views-widget">
							<?php print $widget->widget; ?>
						</div>
						<?php if (!empty($widget->description)): ?>
							<div class="description">
								<?php print $widget->description; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
				<?php if (!empty($sort_by)): ?>
					<div class="views-exposed-widget views-widget-sort-by">
						<?php print $sort_by; ?>
					</div>
					<div class="views-exposed-widget views-widget-sort-order">
						<?php print $sort_order; ?>
					</div>
				<?php endif; ?>
				<?php if (!empty($items_per_page)): ?>
					<div class="views-exposed-widget views-widget-per-page">
						<?php print $items_per_page; ?>
					</div>
				<?php endif; ?>
				<?php if (!empty($offset)): ?>
					<div class="views-exposed-widget views-widget-offset">
						<?php print $offset; ?>
					</div>
				<?php endif; ?>
				<div class="views-exposed-widget views-submit-button">
					<?php print $button; ?>
				</div>
				<?php if (!empty($reset_button)): ?>
					<div class="views-exposed-widget views-reset-button">
						<?php print $reset_button; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function getJsonFromUrl() 
	{
		var query = location.search.substr(1);
		var data = query.split("&");
		var result = {};
		for(var i=0; i<data.length; i++) {
			var item = data[i].split("=");
			result[item[0]] = item[1];
		}
		return result;
	}
	
	jQuery( function() 
	{
		var getVars = getJsonFromUrl();
		var miles = [5,10,25,50,100,200];
		var distance = jQuery('<select />').attr({
			id: 'edit-distance-search-distance',
			name: 'distance[search_distance]'
		}).addClass('form-select');
		
		jQuery.each( miles, function( i, mile ) 
		{
			var o = jQuery('<option />').attr('value', mile).html( 'Search within ' + mile + ' miles' ).appendTo( distance );
			if( parseInt(getVars['distance%5Bsearch_distance%5D']) === mile ) 
			{
				o.attr('selected', 'selected');
			}
		});
		
		jQuery('input#edit-distance-search-distance').replaceWith( distance );
		jQuery('.form-item-distance-search-units').hide();
		
		var zip = jQuery('#edit-distance-postal-code').attr('placeholder', 'Enter your Zip Code');
		var city = jQuery('#edit-field-city-value').attr('placeholder', 'Enter your City');
		var state = jQuery('#edit-field-state-value');
		state.find('option').first().html('Select State');
		
		jQuery('input#edit-submit-store-locator').insertAfter('#edit-field-state-value').clone().insertAfter('.form-item-distance-search-units');
		
		zip.on('focus', function() 
		{
			city.val('');
			state.prop('selectedIndex', 0);
		});
		city.on('focus', function() 
		{
			zip.val('');
		});
		state.on('focus', function() 
		{
			zip.val('');
		});
		
		if( jQuery('.location-map').children().length == 0 ) 
		{
			jQuery('<img />').attr('src', '/sites/all/themes/shurtape/images/map_default.png').appendTo( jQuery('.location-map') );
		}
	});
</script>
