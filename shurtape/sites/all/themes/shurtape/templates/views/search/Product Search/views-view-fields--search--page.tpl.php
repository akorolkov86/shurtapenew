<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
global $language;

if(isset($fields['field_product_type']->content))
{
    $fields['field_product_type']->content = sprintf("<span class='item_cat'>%s</span>",$fields['field_product_type']->content);
}

unset($fields['field_product_images']);

if (isset($_SESSION["shurtape"]["compare_products"]["list"][$fields['nid']->content])) {
    $compare_class = "current";
} else {
    $compare_class = "";
}

$lang = ($language->language != "en") ? $language->language.'/' : '';
?>

<?php if($language->language != "es"): ?>
<a href="#" class="control_compare <?php print $compare_class;?>" data-value="<?php print $fields['nid']->content;?>">Compare</a>
<?php endif; ?>

<span class="item_thumb">
        <?php print $fields['field_images']->content; ?>
   <?php unset($fields['field_images']->content); ?>
</span>
<span class="item_wrap">
	<?php

    if(isset($fields['nid'])):
        unset($fields['nid']);
    endif;
    foreach ($fields as $id => $field):
        if (!empty($field->separator)):
            print $field->separator;
        endif;
        print $field->wrapper_prefix;
        print $field->label_html;
        print $field->content;
        print $field->wrapper_suffix;
    endforeach;
    ?>
</span>
