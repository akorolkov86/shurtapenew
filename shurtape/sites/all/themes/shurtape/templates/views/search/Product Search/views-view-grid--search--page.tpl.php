<?php

/**
 * @file
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php foreach ($rows as $row_number => $columns): ?>

	<div class="row <?php if($row_number == 0) { echo "first"; } ?>">
		<span class="border_mask one"></span>
		<span class="border_mask two"></span>

		<?php foreach ($columns as $column_number => $item): ?>

			<div href="#" class="item col-md-4 <?php if($column_number %3 == 2) { echo "last"; } ?>">
				<span class="mobile_link_icon mobile">
					<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
				</span>
				<?php print $item; ?>
			</div>

		<?php endforeach; ?>

	</div>

<?php endforeach; ?>



