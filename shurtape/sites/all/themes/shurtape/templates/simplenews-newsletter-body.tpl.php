<?php

/**
 * @file
 * Default theme implementation to format the simplenews newsletter body.
 *
 * Copy this file in your theme directory to create a custom themed body.
 * Rename it to override it. Available templates:
 *   simplenews-newsletter-body--[tid].tpl.php
 *   simplenews-newsletter-body--[view mode].tpl.php
 *   simplenews-newsletter-body--[tid]--[view mode].tpl.php
 * See README.txt for more details.
 *
 * Available variables:
 * - $build: Array as expected by render()
 * - $build['#node']: The $node object
 * - $title: Node title
 * - $language: Language code
 * - $view_mode: Active view mode
 * - $simplenews_theme: Contains the path to the configured mail theme.
 * - $simplenews_subscriber: The subscriber for which the newsletter is built.
 *   Note that depending on the used caching strategy, the generated body might
 *   be used for multiple subscribers. If you created personalized newsletters
 *   and can't use tokens for that, make sure to disable caching or write a
 *   custom caching strategy implemention.
 *
 * @see template_preprocess_simplenews_newsletter_body()
 */
?>
<table border="0" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
  <tr><td><p style="text-align: center;"><a href="<?php print $node_link; ?>" style="font-size: 20px;font-family: Franklin Gothic, Arial, sans-serif;color: #003f69;font-weight: bold;text-decoration: none;"><?php print $title; ?></a></p></td></tr>
  <tr><td><p><img src="image:<?php print $blog_image; ?>" align="right" style="margin: 0 0 10px 10px; width: 212px;height: 100%;" /><font face="Franklin Gothic,arial,sans-serif"><?php print $blog_body; ?></font></p></td></tr>
  <tr><td><p><a href="<?php print $node_link; ?>"><img src="http://image.s6.exacttarget.com/lib/fe8712717c6c0d7d72/m/1/Read+more.jpeg" align="right"/></a></p></td></tr>
  <tr><td><p style="font-family: Franklin Gothic, Arial, sans-serif;color: #003f69;font-size: 16px;text-align: center;"><strong>Visit <a href="http://www.shurtape.com/?utm_campaign=Blog%20Newsletter&utm_medium=Email&utm_source=Email&utm_content=Text%20Link">shurtape.com</a> to view our full line of tapes.</strong></p></td></tr>
</table>
