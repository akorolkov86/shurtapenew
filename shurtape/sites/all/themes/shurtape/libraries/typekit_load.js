(function ($) {
  $.getScript('//use.typekit.net/jny7oeb.js', function () {
    try {
      $('body').addClass('typekit');
      Typekit.load();
    } catch (e) {
      $('body').removeClass('typekit');
    }
  })
})(jQuery)