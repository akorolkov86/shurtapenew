/**
 * main JS (use this for scripts common to all - or most - pages)
 */
/* define $ as jQuery just in case */
( function( $ ){

	/* doc ready */
	$( function( ){
		/* hide accordian box in default*/
		jQuery('.group .box').hide();

		var $this = $('.view-distributors:not(.views-distributors-process)');
		if ($this.length) {
			$this.addClass("views-distributors-process");
			$this.find('table').addClass("table");
			$this.find('.form-submit').addClass("btn btn-orange");
		}

		/* close the dropdown / flyout if user clicks outside the submenu */
		$( 'body' ).click( function( e )
		{
			var main_menu 	= $( 'ul.primary_menu' );
			var submenus	= main_menu.find( 'li > ul.menu > li > ul.menu' );
			var top_menus	= main_menu.find( 'li > ul.menu' );
			var top_links	= top_menus.find( 'li > a' );
			if (
				! submenus.is( e.target ) &&
				submenus.has( e.target ).length === 0 &&
				top_links.has( e.target ).length === 0
			) {
				hide_flyouts( );
			}
		});

		/* flyout timer - clear on mouse enter / start on mouse leave */
		var flyout_timer;
		$( '#nav' ).on( 'mouseenter', 'ul.primary_menu > li', function( )
		{
			/* clear the timer */
			clearTimeout( flyout_timer );

			/* set the vars */
			var trigger_el	= $( this );
			var parent_menu	= $( trigger_el ).parents( 'ul.primary_menu' );
			var top_menus	= parent_menu.find( 'li > ul.menu' );
			var submenus	= parent_menu.find( 'li > ul.menu > li > ul.menu' );
			var target_el	= trigger_el.find( 'ul.menu' );

			/* if not already open, animate the submenu open */
			if ( ! target_el.hasClass( 'open' ) )
			{
				/* add the active class to the parent anchor */
				parent_menu.find( '> li > ul.menu > li > a' ).removeClass( 'current' );
				if ( target_el.find( 'ul.menu').length > 0 )
				{
					trigger_el.find( '> ul.menu > li > a' ).addClass( 'current' );
				}

				/* close the other submenus and open the target submenu */
				top_menus.removeClass( 'open' );
				submenus.hide( );
				target_el.slideDown( 200 ).addClass( 'open' );
			}
		});

		$('div.mobile_toggle_target ul.menu li a, div.mobile_toggle_target ul.sub_menu li a').click(function(){
			$('.orange-selected').removeClass('orange-selected');
			if($(this).next('ul').length == 0 || $(this).attr('title') == "Contact"){
				$(this).parent().addClass('orange-selected');
			}


		});

		/* hide the flyouts on the main (top) menu - with a half-second delay */
		$( '#nav' ).on( 'mouseleave', 'ul.primary_menu > li', function( e )
		{
			flyout_timer = setTimeout( function( )
			{
				hide_flyouts( );
			}, 500 );
		});

		/* hide the flyouts on the main (top) menu - with a 3 second delay */
		function hide_flyouts( )
		{
			var main_menu 	= $( 'ul.primary_menu' );
			var submenus	= main_menu.find( 'li > ul.menu > li > ul.menu' );
			var top_menus	= main_menu.find( 'li > ul.menu' );
			var top_links	= top_menus.find( 'li > a' );
			submenus.hide( );
			top_links.removeClass( 'current' );
			top_menus.removeClass( 'open' );
		}

		/* init the placeholder plugin */
		$( 'input, textarea' ).placeholder( );

		/* submit the form via jquery (without validation) */
		$( '.submit_this_form' ).click( function( e ) {
			var form = $( this ).parents( 'form' );
			form.submit( );
			e.preventDefault( );
		});

		/**
		 * validate form elements with jQuery by class and custom attributes - my custom plugin
		 *
		 * TODO - make it more robust and turn into a plugin
		 * ---
		 * usage:
		 * 1. add "validate_this_form" class to the button, anchor, or other element that submits the form
		 * 2. add the following custom attributes to the input / textarea elements:
		 *    - "error_required" - error message thrown if the field is required
		 *    - "error_match" - error message thrown if a field doesn't match it's counterpart field
		 *    - "data_match" - name attribute of the counterpart field to match
		 *    - "error_email" - error message thrown if field must be a valid email
		 *    - "error_alphanumeric" - error message thrown if field must be alpha-numeric
		 *    - "error_min" - error message thrown if field has a minimum number of characters
		 *    - "data_min" - the minimum number of chars if using "error_min"
		 *    - "error_max" - error message thrown if field has a maximum number of characters
		 *    - "data_max" - the maximum number of chars if using "error_max"
		 *    - "error_counterpart" - error thrown to a counterpart field (e.g. confirm password)
		 *    - "data_counterpart" - name attribute of the counterpart field to throw the error (e.g. password)
		 * 3. error messages are thrown to "error_container" element within the respective "field_group" element and "all_errors_container"
		 */
		$( '.validate_this_form' ).click( function( e ) {

			var form = $( this ).parents( 'form' );

			/* clear the errors */
			form.find( '.all_errors_container' ).html( '' );
			form.find( '.error_container' ).html( '' );
			form.find( '.error_field' ).removeClass( 'error_field' );
			var errors = { };

			/**
			 * check standard validations
			 * go in reverse order of priority
			 * (e.g.) required should be the first thing checked, but it goes on the bottom
			 * so that it takes over the value of the errors[field.name] and the
			 * lower priority errors do not get displayed at the same time
			 */
			$.each( form.serializeArray( ), function( i, field )
			{
				var form_input = form.find( '*[name=' + field.name + ' ]' );
				if ( form_input.attr( 'error_match' ) !== undefined && form_input.attr( 'data_match' ) !== undefined )
				{
					var match_field = form_input.attr( 'data_match' );

					/* if the match field exists */
					if ( form.find( '*[name=' + match_field + ' ]' ).length !== 0 )
					{
						if ( field.value !== $( '*[name=' + match_field + ' ]' ).val( ) )
						{
							errors[ field.name ] = form_input.attr( 'error_match' );
						}
					}
				}
				if ( form_input.attr( 'error_email' ) !== undefined )
				{
					if ( ! is_valid_email( field.value ) )
					{
						errors[ field.name ] = form_input.attr( 'error_email' );
					}
				}
				if ( form_input.attr( 'error_alphanumeric' ) !== undefined )
				{
					if ( ! is_alphanumeric( field.value ) )
					{
						errors[ field.name ] = form_input.attr( 'error_alphanumeric' );
					}
				}
				if ( form_input.attr( 'error_min' ) !== undefined && form_input.attr( 'data_min' ) !== undefined )
				{
					if ( field.value.length < form_input.attr( 'data_min' ) )
					{
						errors[ field.name ] = form_input.attr( 'error_min' );
					}
				}
				if ( form_input.attr( 'error_max' ) !== undefined && form_input.attr( 'data_max' ) !== undefined )
				{
					if ( field.value.length > form_input.attr( 'data_max' ) )
					{
						errors[ field.name ] = form_input.attr( 'error_max' );
					}
				}
				if ( form_input.attr( 'error_required' ) !== undefined )
				{
					if ( field.value.length === 0 )
					{
						errors[ field.name ] = form_input.attr( 'error_required' );
					}
				}

			});

			/**
			* if the errors aren't empty
			*/
			if ( ! $.isEmptyObject( errors ) )
			{
				/* set the counterpart errors */
				$.each( errors, function( key, value )
				{
					if ( form.find( '*[name=' + key + ']' ).attr( 'data_counterpart' ) !== undefined )
					{
						var cp_message = form.find( '*[name=' + key + ' ]' ).attr( 'error_counterpart' ) !== undefined ? form.find( '*[name=' + key + ' ]' ).attr( 'error_counterpart' ) : '&nbsp;';
						var counterpart = form.find( '*[name=' + key + ' ]' ).attr( 'data_counterpart' );
						errors[ counterpart ] = ! errors[ counterpart ] ? cp_message : errors[ counterpart ];
					}
				});

				/* display the errors to error elements + set error classes */
				$.each( errors, function( key, value )
				{
					if ( form.find( '*[name=' + key + ']' ).length !== 0 )
					{
						var field_group = form.find( '*[name=' + key + ']').parents( '.field_group' );
						field_group.find( '.error_container' ).html( value );
						field_group.addClass( 'error_field' ); /* add error class to the whole field group */
						/* displays all errors into the all_errors_container - if desired */
						form.find( '.all_errors_container' ).append( value + '<br />' ).css({ 'display':'block' }).show( );
					}
				});

				/* scroll to the top of the form */
				$( 'html, body' ).animate({
					scrollTop: $( form ).offset( ).top
				}, 500 );

				/* clear the password fields */
				$.each( form.serializeArray( ), function( i, field )
				{
					var form_input = form.find( '*[name=' + field.name + ' ]' );
					if ( form_input.attr( 'type' ) === 'password' )
					{
						form_input.val( '' );
					}
				});
			}

			/**
			* otherwise submit the form
			*/
			if ( $.isEmptyObject( errors ) )
			{
				form.submit( );
			}
			e.preventDefault( );
		});

		/* check for alpha-numeric */
		function is_alphanumeric( string )
		{
			var regex = /^[A-Za-z0-9_.-]+$/;
			if ( regex.test( string ) === true )
			{
				return true;
			}
			return false;
		}

		/* check for valid email address */
		function is_valid_email( email )
		{
			var email_regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			if ( email_regex.test( email ) === true )
			{
				return true;
			}
			return false;
		}

		/* back to top link (global) */
		$( 'body' ).on( 'click', '.scroll_top', function( e )
		{
			$( 'html, body' ).animate({
				scrollTop:0
			}, 500 );
			e.preventDefault( );
		});

		/* back to top link (global) */
		$( '.back_to_top ' ).on( 'click', 'a', function( e )
		{
			$( 'html, body' ).animate({
				scrollTop:0
			}, 500 );
			e.preventDefault( );
		});

		/* show the back to top link only when the user is further down on the page (global) */
		$( 'a.scroll_top' ).hide( );
		$( window ).scroll( function( )
		{
			var top = $( window ).scrollTop( );
			if ( top > 500 )
			{
				$( 'a.scroll_top' ).fadeIn( );
			}
			else
			{
				$( 'a.scroll_top' ).fadeOut( );
			}
		});

		/* generic scroll-to effect - set the "rel" attribute to the ID of the element where you scroll to */
		$('body').on('click', 'scroll_to', function(e)
		{
			var target_id = $(this).attr( 'rel' );
			$('html, body').animate({
				scrollTop:$("#" + target_id).offset().top
			}, 600);
			e.preventDefault();
		});

	});

	/* tabs - my custom plugin */
	$.fn.tabs = function( )
	{        
		/* set vars */
		var tab_group 		= this;
		var tab_controls 	= tab_group.find( '.tab_control' );
		var active_tabs 	= tab_group.find( '.tab_control.current' );
		var tab_targets 	= tab_group.find( '.tab_target' );
		var active_tab 		= active_tabs.first( ).length > 0 ? active_tabs.first( ) : tab_group.find( '.tab_control:eq(0)' );
		var target_id		= active_tab.attr( 'rel' );
		var target_el		= tab_group.find( '.tab_target#' + target_id );

		/* set the css */
		tab_group.hide( );
		$( window ).load( function( ) {

			/* grab the active tab and hide all other tabs */
			tab_controls.removeClass( 'current' );
			active_tab.addClass( 'current' );
			tab_targets.hide( );
			target_el.show( );

			/* show the tabs when everything is loaded */
			tab_group.show( );

		});

		/* tab click */
		tab_group.on( 'click', '.tab_control', function( e ) {

			/* set active class on the clicked tab */
			tab_controls.removeClass( 'current' );
			$( this ).addClass( 'current' );

			/* grab the target tab and show it */
			var target_id 	= $( this ).attr( 'rel' );
			var target_el	= tab_group.find( '.tab_target#' + target_id );
			tab_targets.hide( );
			target_el.show( );
			e.preventDefault( );
		});
	}

	/* thumb slideshow - my custom plugin */
	$.fn.thumb_slider = function( )
	{
		/* set vars */
		var thumb_slider	= this;
		var slides 			= thumb_slider.find( '.slide' );
		var controls 		= thumb_slider.find( '.controls .control' );
		var active_controls = controls.find( '.current' );
		var active_control 	= active_controls.first( ).length > 0 ? active_controls.first( ) : thumb_slider.find( '.control:eq(0)' );
		var target_index	= active_control.index( );
		var target_el		= thumb_slider.find( '.slide:eq(' + target_index + ')' );

		/* set the css */
		thumb_slider.hide( );
		$( window ).load( function( )
		{
			/* grab the active slide and hide all other slides */
			controls.removeClass( 'current' ).css({ 'opacity':0.5 });
			active_control.addClass( 'current' ).css({ 'opacity':1 });;
			slides.hide( );
			target_el.show( );

			/* show the slideshow when everything is loaded */
			thumb_slider.show( );

		});

		/* click the thumb */
		thumb_slider.on( 'click', '.control a', function( e )
		{
			/* set active class on the clicked thumb */
			controls.removeClass( 'current' ).css({ 'opacity':0.5 });
			parent_control = $( this ).parents( '.control' );
			parent_control.addClass( 'current' ).css({ 'opacity':1 });

			/* grab the target thumb and show it */
			var target_index 	= $( this ).parents( '.control' ).index( );
			var target_el		= thumb_slider.find( '.slide:eq(' + target_index + ')' );
			slides.hide( );
			target_el.show( );
			e.preventDefault( );
		});

	}

	/* init the mobile nav function */
	mobile_nav();

	/* mobile nav sidebar events */
	function mobile_nav()
	{
		/* sidebar toggles */
		$( '#mobile_header' ).on(
			'click',
			'.menu_control',
			function(e)
			{
				var mobile_nav 	= $( '#mobile_nav' );

				/* open it if it wasn't already open */
				if ( ! mobile_nav.hasClass('open'))
				{
					open_mobile_nav( );
				}
				else
				{
					close_mobile_nav( );
				}
				e.preventDefault();
			}
		);

		/* top level toggles */
		$( '#mobile_nav' ).on(
			'click',
			'.mobile_toggle_control',
			function(e)
			{
				var parent_el = $( this ).parents( '.mobile_toggle_box' );
				var target = parent_el.find( '.mobile_toggle_target' );

				/* open it if it wasn't already open */
				if ( ! parent_el.hasClass('open'))
				{
					parent_el.addClass('open');
					target.show();
					console.log( 'open' );
				}
				else
				{
					parent_el.removeClass('open');
					target.hide();
				}
				e.preventDefault();
			}
		);

		/* toggle control click action */
		$('#mobile_nav ul.sub_menu').on(
			'click',
			'li > ul.menu > li.expanded > a',
			function(e)
			{
				var parent_el 	= $( this ).parents( 'li.expanded' );
				var target 		= parent_el.find( 'ul.menu' );

				/* open it if it wasn't already open */
				if ( ! parent_el.hasClass('open'))
				{
					$( '.mobile_browse_box li.expanded' ).removeClass('open');
					parent_el.addClass('open');
					target.show();
				}
				else
				{
					parent_el.removeClass('open');
					target.hide();
				}
				e.preventDefault();
			}
		);

		/**
		 * mobile nav - filter box toggles
		 * - in reverse because Drupal sucks and insists on controlling markup instead of separating business and view logic (i.e. the right way)
		 */
		$('#mobile_nav').on(
			'click',
			'.mobile_filter_box li > ul.menu > a',
			function(e)
			{
				var target 	= $( this ).next( '.toggle_target' );

				/* open it if it wasn't already open */
				if ( ! target.hasClass('closed'))
				{
					$( this ).addClass('closed');
					target.addClass('closed');
					target.hide();
				}
				else
				{
					$( this ).removeClass('closed');
					target.removeClass('closed');
					target.show();
				}
				e.preventDefault();
			}
		);

		/**
		 * reset mobile nav on click outside of nav
		 */
		$('.mobile_nav_open').on(
			'click',
			'.shift_box',
			function(e)
			{
				close_mobile_nav( );
				e.preventDefault();
			}
		);
	}

	/* open mobile nav */
	function open_mobile_nav( )
	{
		var mobile_nav 	= $( '#mobile_nav' );

		/* open it if it wasn't already open */
		if ( ! mobile_nav.hasClass('open'))
		{
			mobile_nav.addClass('open');
			$( 'body' ).addClass( 'mobile_nav_open' );
		}
		set_mobile_nav_top( );
	}


	/* function to set the mobile nav top values */
	function set_mobile_nav_top( )
	{
		var mobile_nav 	= $( '#mobile_nav' );
		var shift_box	= $( '.shift_box' );
		var control_bar = $( '#mobile_header .control_bar' );
		var cb_height	= control_bar.height( ) + 'px';

		/* set top values */
		mobile_nav.css({
			'top':cb_height
		});
		shift_box.css({
			'top':cb_height
		});
	}

	/* function to close the mobile nav */
	function close_mobile_nav( )
	{
		/* set vars */
		var mobile_nav 	= $( '#mobile_nav' );
		var shift_box	= $( '.shift_box' );

		mobile_nav.removeClass('open').css({
			'top':0
		});
		shift_box.css({
			'top':0
		});
		$( 'body' ).removeClass( 'mobile_nav_open' );

	}

	/* fire mobile nav reset on window resize */
	$( window ).resize(function() {
		set_mobile_nav_top( );
	});

	/* toggle / accordian events - my custom plugin */
	$.fn.accordian = function( speed )
	{

		/* set vars */
		var accordian 		= this;
		var groups 			= accordian.find( '.group' );
		var boxes 			= accordian.find( '.box' );
		var controls 		= accordian.find( '.control' );
		var active_controls = controls.find( '.current' );
		var active_control 	= active_controls.first( ).length > 0 ? active_controls.first( ) : accordian.find( '.control:eq(0)' );
		var speed = ( ! speed ) ? 300 : speed;

		/**
		 * slide up all of the toggle targets and open the ones with the parent toggle box set to open - very fast so as to not be visible
		 * - we do this to eliminate the hitch in the animation by animating up first instead of using "display:none;"
		 * - before sliding up, grab the collapsed accordian height plus one expanded box and set as min-height to get rid of Chrome animation choppiness
		 */
		var exp_height = accordian.height( );
		accordian.hide( );
		boxes.slideUp( 0 );

		/* on page load - show the accordian */
		$( window ).load( function( ) {

			/* show the accordian when everything is loaded */
			accordian.show( )

		});

		/* accordian control click action */
		accordian.on( 'click', '.control', function( e ) {
			var parent_el 	= $( this ).closest( '.group' );
			var target 		= parent_el.find( '.box' );
			if ( parent_el.hasClass( 'open' ) )
			{
				parent_el.removeClass( 'open' );
				target.slideUp( speed );
			}
			else
			{
				groups.removeClass( 'open' );
				boxes.slideUp( speed );
				parent_el.addClass( 'open' );
				target.slideDown( speed );
			}
			e.preventDefault( );
		});
	}

	/**
	 * mobile toggles
	 */
	$( '#mobile_toggle .mobile_toggle_control' ).click( function( e )
	{
		/* set vars */
		var parent_el 				= $( this ).parents( '.mobile_toggle_box' );
		var mobile_toggle_target 	= parent_el.find( '.mobile_toggle_target' );

		/* open it if it wasn't already open */
		if ( ! parent_el.hasClass( 'open' ) )
		{
			/* show toggle target */
			parent_el.addClass( 'open' );
			mobile_toggle_target.show( );

			/* switch the toggle_image */
			var toggle_icon = $( this ).find( '.icon img' ).attr( 'src' );
			var new_icon	= toggle_icon.replace( 'btn_mobile_toggle.png', 'btn_mobile_toggle_open.png' );
			$( this ).find( '.icon img' ).attr( 'src', new_icon );
		}
		else
		{
			/* hide toggle target */
			parent_el.removeClass( 'open' );
			mobile_toggle_target.hide( );

			/* switch the toggle_image */
			var toggle_icon = $( this ).find( '.icon img' ).attr( 'src' );
			var new_icon	= toggle_icon.replace( 'btn_mobile_toggle_open.png', 'btn_mobile_toggle.png' );
			$( this ).find( '.icon img' ).attr( 'src', new_icon );
		}
		e.preventDefault( );
	});

	/**
	 * mobile video slider - custom plugin (used on product detail and category pages
	 */
	$.fn.video_slider = function( )
	{
		/* set vars */
		var video_slider 	= this;
		var vids			= video_slider.find( 'ul li.vid' );
		var vids_count		= vids.length;
		if ( vids_count < 2 )
		{
			video_slider.find( '.arrow_control' ).hide( );
		}

		/* mobile click events */
		video_slider.on( 'click', '.arrow_control', function( e )
		{
			var curr_index		= video_slider.find( 'ul li.vid.active' ).index( );
			if ( $( this ).hasClass( 'prev' ) )
			{
				var target_index	= curr_index == 0 ? parseInt( vids_count - 1 ) : parseInt( curr_index - 1 );
			}
			else
			{
				var target_index	= curr_index == parseInt( vids_count - 1 ) ? 0 : parseInt( curr_index + 1 );
			}
			var target_el		= video_slider.find( '.vid:eq(' + target_index + ')' );
			vids.hide( ).removeClass( 'active' );
			target_el.show( ).addClass( 'active' );
			e.preventDefault( );
		});
	}

	/* swipe scroller for the thumb/video slider */
	$('.swipe_scroll').hammer({ }).on( 'swipeleft dragleft', function( e )
	{
		var parent_el = $( this );
		var trigger_link = parent_el.find( '.arrow_control.next' );
		trigger_link.trigger( 'click' );
		e.stopPropagation( );
	});

	/* swipe scroller for the thumb/video slider */
	$('.swipe_scroll').hammer({ }).on( 'swiperight dragright', function( e )
	{
		var parent_el = $( this );
		var trigger_link = parent_el.find( '.arrow_control.prev' );
		trigger_link.trigger( 'click' );
		e.stopPropagation( );
	});

	/* swipe scroller for the recently viewed slider */
	$('.swipe_scroll_home').hammer({ }).on( 'swipeleft dragleft', function( e )
	{
		var parent_el = $( this );
		var trigger_link = parent_el.find( '.control.next' );
		trigger_link.trigger( 'click' );
		e.stopPropagation( );
	});

	/* swipe scroller for the recently viewed slider */
	$('.swipe_scroll_home').hammer({ }).on( 'swiperight dragright', function( e )
	{
		var parent_el = $( this );
		var trigger_link = parent_el.find( '.control.prev' );
		trigger_link.trigger( 'click' );
		e.stopPropagation( );
	});


	/* swipe scroller for the recently viewed slider */
	$('.nav_overlay, #mobile_nav').hammer({ }).on( 'swipeleft dragleft', function( e )
	{
		close_mobile_nav( );
		e.stopPropagation( );
	});

	/* prevent default on ipod and ipad for the tel: link */
	$( 'a[href*="tel:"]' ).click( function( e )
	{
		var user_agent = navigator.userAgent.toLowerCase( );
		if ( (/ipod|ipad/).test( user_agent ) )
		{
			e.preventDefault( );
		}
	});

})( jQuery );

(function ($) {
	$(function () {
		$('#video_modal .modal-body').fitVids();
		$('#media_box').fitVids();
	});
})(jQuery);
