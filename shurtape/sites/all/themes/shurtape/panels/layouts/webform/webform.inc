<?php

/**
 * @file
 * Defines a naked avis layout.
 */

$plugin = array(
  'title' => 'Webform Layout',
  'category' => 'Avis',
  'icon' => 'webform.png',
  'theme' => 'webform',
  'regions' => array(
	  'title' => 'Title',
	  'content' => 'Content',
	  'content_top' => 'Content top',
	  'help' => 'Help',
	  'action_links' => 'Action Links',
      'breadcrumb_section' => 'Breadcrumb Section',
      'image' => 'Image',
      'annotation' => 'Annotation',
      'bottom' => 'Bottom',
  ),
);