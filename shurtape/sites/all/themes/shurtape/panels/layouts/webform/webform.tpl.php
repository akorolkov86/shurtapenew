<h1 class="headline">
	<?php if ($content['title']): ?>
		<?php print t($content['title']); ?>
	<?php endif; ?>
</h1>

<?php if ($content['content_top']): ?>
	<div id="content_top">
		<?php print render($content['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($content['help'])): ?>
	<?php print render($content['help']); ?>
<?php endif; ?>

<?php if (isset($content['action_links'])): ?>
	<ul class="action-links">
		<?php print render($content['action_links']); ?>
	</ul>
<?php endif; ?>

<div class="blue_bar desktop">
	<?php if (isset($content['breadcrumb_section'])): ?>
		<?php print render($content['breadcrumb_section']); ?>
	<?php endif; ?>
</div>

<div class="clearfix">
	<div id="left" class="pull-left desktop">
		<?php print $content['image'] ; ?>
	</div>

	<div id="right" class="pull-right">

		<?php print $content['annotation'] ; ?>

		<form id="contact_form" method="post" action="#" role="form">

			<div id="form_box">
				<div class="all_errors_container"></div>
				<div class="form_wrapper">
					<?php print $content['content'] ; ?>
				</div>
			</div>
		</form>
	</div>
</div>

<?php print $content['bottom'] ; ?>
