<?php

/**
 * @file
 * Defines a naked avis layout.
 */

$plugin = array(
  'title' => 'Page News Layout',
  'category' => 'Avis',
  'css' => 'page_news.css',
  'icon' => 'page_news.png',
  'theme' => 'page_news',
  'regions' => array(
	  'title' => 'Title',
	  'content_top' => 'Content top',
	  'help' => 'Help',
	  'action_links' => 'Action Links',
      'breadcrumb_section' => 'Breadcrumb Section',
      'field_sub_heading' => 'Content Sub heading',
      'field_image' => 'Image field',
      'content' => 'Content',
  ),
);