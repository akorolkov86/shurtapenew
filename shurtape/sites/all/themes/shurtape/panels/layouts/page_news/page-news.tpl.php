<h1 class="headline"><?php print t('News') ?></h1>

<?php if ($content['content_top']): ?>
	<div id="content_top">
		<?php print render($content['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($content['help'])): ?>
	<?php print render($content['help']); ?>
<?php endif; ?>

<?php if (isset($content['action_links'])): ?>
	<ul class="action-links"><?php print render($content['action_links']); ?></ul>
<?php endif; ?>

<div class="blue_bar desktop">
	<?php if (isset($content['breadcrumb_section'])): ?>
		<?php print render($content['breadcrumb_section']); ?>
	<?php endif; ?>
</div>

<div class="news_detail_wrap">
	<?php if ($content['title']): ?>
		<h2 class="news_detail_title">
			<?php print $content['title']; ?>
		</h2>
	<?php endif; ?>

	<?php if (!empty($content['field_sub_heading'])): ?>
		<h3 class="news_detail_subtitle"><?php print $content['field_sub_heading']; ?></h3>
	<?php endif; ?>

	<div class="news_detail_content clearfix">
		<?php if (!empty($content['field_image'])): ?>
			<div class="news_detail_photo pull-left">
				<?php print $content['field_image']; ?>
			</div>
		<?php endif; ?>
		<div <?php if (!empty($content['field_image'])): ?>class="news_detail_body pull-right"<?php endif; ?>>
			<?php print $content['content']; ?>
		</div>
	</div>
</div>