<?php if ($content['title']): ?>
	<?php print $content['title']; ?>
<?php endif; ?>

<?php if ($content['content_top']): ?>
	<div id="content_top">
		<?php print render($content['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($content['help'])): ?>
	<?php print render($content['help']); ?>
<?php endif; ?>
<?php if (isset($content['action_links']) && !empty($content['action_links'])): ?>
	<ul class="action-links"><?php print render($content['action_links']); ?></ul>
<?php endif; ?>

<?php if (arg(1) != 29): ?>
<div class="blue_bar desktop">
<?php if (isset($content['breadcrumb_section'])): ?>
		<?php print render($content['breadcrumb_section']); ?>
<?php endif; ?>
</div>
<?php endif; ?>

<div class="padded_body">
	<?php print $content['content'] ; ?>
</div>

<?php print $content['bottom'] ; ?>



<script type="text/javascript">
  /* define $ as jQuery just in case */
  ( function( $ ){

    /* doc ready */
    $( function( )
    {
      /* initiate the news item list */
      var news_list	= $( '.view-news ul.link_list');

      if (news_list.length) {
        init_news_list( );
      }

      function init_news_list( )
      {
        /* set vars */

        var news_items 	= news_list.find( 'li' );
        var item_count	= news_items.length;
        var num_shown	= 12;
        var remaining	= parseInt( item_count - num_shown );
        var speed		= 200;

        /* insert the More News link if remainder is greater than 0 */
        if ( remaining > 0 )
        {
          $( '<div class="more_link_wrap"><a href="#" class="more_news_link">More News</a></div>' ).appendTo( news_list );
        }

        /* loop through the news items - hide all but the first N items */
        $.each( news_items, function( i, el )
        {
          if( i > parseInt( num_shown - 1 ) )
          {
            $( el ).hide( );
          }
        });

        /* more news click event */
        $( news_list ).on( 'click', 'a.more_news_link', function( e )
        {
          var hidden_items 	= news_list.find( 'li:hidden' );
          var hidden_count	= hidden_items.length;
          var show_count		= num_shown > hidden_count ? hidden_count : num_shown;
          if ( num_shown > hidden_count )
          {
            $( '.more_news_link' ).hide( );
          }
          for ( var x = 0; x < show_count; ++x )
          {
            $( hidden_items[x] ).slideDown( speed ).fadeIn( speed );
          }
          e.preventDefault( );
        });
      }

    });
  })( jQuery );
</script>