<?php
$path_to_zeroclipboard = libraries_get_path("zeroclipboard");
drupal_add_js("$path_to_zeroclipboard/ZeroClipboard.js");


drupal_add_js(array('shurtape_media_infographic' => array('zero_path' => $path_to_zeroclipboard)), 'setting');

drupal_add_js(drupal_get_path('module', 'shurtape_media_infographic') . '/script_infographic.js');

?>

<div class="blue_bar promotion_product_header">&nbsp;</div>

<div class="content_wrapper">
	<div id="media_box">
		<?php if (!empty($content['img_with_link'])): ?>
			<?php print render($content['img_with_link']); ?>
		<?php endif; ?>
	</div>
	<div id="details_box">
		<div class="padded_body">
			<?php if (!empty($content['title'])): ?>
				<div class="asset-title">
					<?php print render($content['title']); ?>
				</div>
			<?php endif; ?>
			<?php if (!empty($content['download_link'])): ?>
				<div class="download_link">
					<?php print render($content['download_link']); ?>
				</div>
			<?php endif; ?>
			<?php if (!empty($content['content'])): ?>
				<?php print $content['content'] ; ?>
			<?php endif; ?>
			<div class="social-share-container">
				<span>Share This Media</span>
				<div class="recommend">
					<a class="social-share-mail" href="<?php print "mailto:?subject=$email_subject&body=$email_body" ?>">Mail</a>
				</div>
				<?php print render($content["social_share"]); ?>
			</div>
			<div class="asset-embed">
				<span><b>Embed</b></span> <i>(click below to copy to your clipboard)</i>
				<p>
					<?php echo $embed_text; ?>
				</p>
			</div>
		</div>
	</div>
</div>