<?php

/**
 * @file
 * Defines a naked avis layout.
 */

$plugin = array(
  'title' => 'Page Product',
  'category' => 'Avis',
  'css' => 'page_product.css',
  'icon' => 'page_product.png',
  'theme' => 'page-product',
  'regions' => array(
      'product_images_gallery' => 'Product Images Gallery',
      'green_point' => 'Green Point',
      'title' => 'Title',
      'subtitle' => 'Subtitle',
      'body' => 'Body',
      'product_detail_links' => 'Product Detail Links',
      'product_popout_region' => 'Product Popout Region',
      'related_products' => 'Related Products',
      'product_properties_tab_content' => 'Product Properties Tab',
      'product_markets' => 'Product Markets',
      'product_applications' => 'Product Applications',
      'product_sizes_colors_tab_content' => 'Sizes and Colors Tab',
      'product_downloads_tab_content' => 'Product Downloads Tab',
      'product_videos' => 'Product Videos',
      'product_testimonial' => 'Product Testimonial',
  ),
);