<?= $content['before_header'] ?>
<div id="header" class="clearfix desktop" role="banner">
  <div class="fixed_wrap clearfix">
    <?= $content['header'] ?>
  </div>
</div>
