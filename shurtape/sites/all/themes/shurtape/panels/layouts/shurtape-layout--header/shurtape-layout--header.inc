<?php

// Plugin definition.
$plugin = [
  'title' => t('Default layout header'),
  'category' => t('Shurtape content layouts'),
  'theme' => 'shurtape-layout--header',
  'icon' => 'shurtape-layout--header.png',
  'css' => 'shurtape-layout--header.css',
  'regions' => [
    'before_header' => 'BeforeHeader',
    'header' => 'Header',
  ],
];
