About Shurtape Theme
====================
Shurtape is a fixed width (980px) theme. The theme is not dependent on any 
core theme. Its very light weight for fast loading with modern look.
  Simple and clean design
  Fixed width (980px)
  Drupal standards compliant
  Custom front-page with 4 block regions
  Implementation of a JS Slideshow
  Multi-level drop-down menus
  Use of Google Web Fonts
  Custom front-page with 4 block regions
  Footer with 4 regions
  A total of 12 regions
  Compatible and tested on IE7, IE8, IE9+, Opera, Firefox, Chrome browsers

Browser compatibility:
=====================
The theme has been tested on following browsers. IE7+, Firefox, Google Chrome, Opera.

Drupal compatibility:
=====================
This theme is compatible with Drupal 7.x.x

Developed by
============
Tenth Wave development team 

Developers
===============
Sajjad Khan, Michael lonergan, Frank Cefalu.

Mentor
==========
Marco Lara.