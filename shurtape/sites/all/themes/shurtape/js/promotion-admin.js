( function( $ ){

    /* doc ready */
    $( function( ){
        //override drupal search autocomplete to avoid caching items
        Drupal.ACDB.prototype.search = function (searchString) {
            var db = this;
            this.searchString = searchString;

            // See if this string needs to be searched for anyway.
            searchString = searchString.replace(/^\s+|\s+$/, '');
            if (searchString.length <= 0 ||
                searchString.charAt(searchString.length - 1) == ',') {
                return;
            }

            // See if this key has been searched for before.
            //  if (this.cache[searchString]) {
            //    return this.owner.found(this.cache[searchString]);
            //  }

            // Initiate delayed search.
            if (this.timer) {
                clearTimeout(this.timer);
            }
            this.timer = setTimeout(function () {
                db.owner.setStatus('begin');

                // Ajax GET request for autocompletion. We use Drupal.encodePath instead of
                // encodeURIComponent to allow autocomplete search terms to contain slashes.
                $.ajax({
                    type: 'GET',
                    url: db.uri + '/' + Drupal.encodePath(searchString),
                    dataType: 'json',
                    success: function (matches) {
                        if (typeof matches.status == 'undefined' || matches.status != 0) {
                            db.cache[searchString] = matches;
                            // Verify if these are still the matches the user wants to see.
                            if (db.searchString == searchString) {
                                db.owner.found(matches);
                            }
                            db.owner.setStatus('found');
                        }
                    },
                    error: function (xmlhttp) {
                        alert(Drupal.ajaxError(xmlhttp, db.uri));
                    }
                });
            }, this.delay);
        };

        initialize_taxonomy_search();
        $("#edit-field-promotion-product-category-und").change(function(element){
            var selected_option_value = $(this).find("option:selected").val();           
            $("#field-promotion-products-values div.form-type-textfield input.form-autocomplete").each(function(index,value){
                $(this).val("");
                $(this).attr('disabled','disabled');                                           
            });
            set_taxonomy_to_filter_products(selected_option_value);               
        });
    });
    function initialize_taxonomy_search(){
        var selected_value = $("#edit-field-promotion-product-category-und option:selected").val();
        if( selected_value != "_none"){
            set_taxonomy_to_filter_products(selected_value);
        }
    }
    function set_taxonomy_to_filter_products(term_id){        
        $.ajax({
            url: "/promotion/ajax/change_product_category_session/"+term_id,
            success:function( data ) {                        
                $("#field-promotion-products-values div.form-type-textfield input.form-autocomplete").each(function(index,value){
                    $(this).removeAttr('disabled');
                });
            }                            
        });
    }	
})( jQuery );


