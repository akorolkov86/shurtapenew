<?php
/**
 * Created by PhpStorm.
 * User: punk_undead
 * Date: 16.10.15
 * Time: 19:40
 */

namespace d\Twig\BEM;

use Twig_Extension;
use Twig_SimpleFunction;


class Functions extends Twig_Extension {
  public function getFunctions() {
    return [
      new Twig_SimpleFunction('e', [$this, 'e'], ['needs_context' => TRUE]),
      new Twig_SimpleFunction('b', [$this, 'b'], ['needs_context' => TRUE]),
    ];
  }

  public function b($c) {
    return $c['block'];
  }

  public function e($c, $e, $m = NULL) {
    $class = "{$c['block']}__{$e}";
    if ($m) {
      if (is_array($m)) {
        $class .= " $class--" . implode(" $class--", $m);
      }
      else {
        $class .= " $class--$m";
      }
    }
    return $class;
  }

  public function getName() {
    return __CLASS__;
  }
}