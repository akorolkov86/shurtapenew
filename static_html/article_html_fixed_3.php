<p>HICKORY, N.C. (May 2013) � Shurtape is pleased to announce that Wayne Helton, senior vice president of manufacturing, has been named president of the Pressure Sensitive Tape Council (PSTC) by the organization�s 26 member companies.</p>

<p>Helton has been active with PSTC � a North American trade association for tape manufacturers and affiliate suppliers � for 10 years, serving nine of those years on the board in various roles, including treasurer and vice president, prior to being named president.</p>

<p>As president, Helton is responsible for leading various initiatives to fruition, including the organization�s Responsible Tape Manufacturer (RTM) program � a program in which PSTC members will certify their manufacturing sites and practices against numerous regulations, laws and best practices to ensure compliance within three categories: Environment, Personnel and Quality.</p>

<p>Helton was voted to the position in November 2012 and will serve as president for the 2013 term; he is eligible for a second term in 2014.</p>