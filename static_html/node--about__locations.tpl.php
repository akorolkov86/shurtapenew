<div class="blue_bar">&nbsp;</div>

<?php print render( $content ); ?>

<div class="locations">
	<div class="map text-center">
		<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/photo_map_locations.jpg" alt="Shurtape office locations">
	</div>

	<div class="blue_bar row">
		<div class="col-md-6"><h3>CORPORATE HEADQUARTERS</h3></div>
		<div class="col-md-6"><h3>AVON CORPORATE OFFICE</h3></div>
	</div>
	<div class="offices row">
		<div class="col-md-6 item first">
			<h4>1712 8th Street Drive SE</h4>
			Hickory, NC 28602 USA<br />
			Phone: (828) 322-2700<br />
			Toll Free: (888) 442-8273
		</div>
		<div class="col-md-6 item last">
			<h4>ShurTech Brands, LLC</h4>
			32150 Just Imagine Drive<br />
			Avon, OH 44011<br />
			Phone: (440) 937-7000<br />
			Fax: (440-937-7077<br />
			Toll Free: (800) 321-1733
		</div>
	</div>

	<h2>Sales Offices</h2>
	<div class="blue_bar row">
		<div class="col-md-6"><h3>UNITED STATES</h3></div>
		<div class="col-md-3"><h3>CANADA</h3></div>
		<div class="col-md-3"><h3>MEXICO</h3></div>
	</div>
	<div class="offices row">
		<div class="col-md-3 item first">
			<h4>North Carolina</h4>
			1712 8th Street Drive SE<br />
			Hickory, NC 28602 USA<br />
			Phone: (828) 322-2700<br />
			Toll Free: (888) 442-8273<br />
			Fax: (828) 322-4029<br />
			Email: anine@shurtape.com
		</div>
		<div class="col-md-3 item">
			<h4>Arizona</h4>
			First Commerce Center<br />
			725 N 73rd Avenue, Suite 112<br />
			Phoenix, AZ 85043 USA<br />
			Phone: (623) 463-5886<br />
			Toll Free: (800) 342-8273<br />
			Fax: (623) 463-5809<br />
			Email: kking@shurtape.com 
		</div>
		<div class="col-md-3 item">
			<h4>Shurtape Technologies Co.</h4>
			615 Bowes Road<br />
			Concord, ON<br />
			Canada L4K 1J5<br />
			Phone: (905) 669-4881<br />
			Toll-Free: (800)463-8273<br />
			Fax: (905) 669-2330<br />
			Email: jdiaz@shurtape.com 
		</div>
		<div class="col-md-3 item last">
			<h4>Shurtape Mexico, S. de R.L. de C.V.</h4>
			Paseo de la Constitucion #11<br />
			Fracc. Ind. Penuelas<br />
			CP 76148, Queretaro, Qro. Mexico<br />
			Phone: +52 (44) 2220-6367<br />
			Fax: +52 (44) 2220-6366<br />
			Email: shurtmex@prodigy.net.mx
		</div>
	</div>

	<div class="blue_bar row">
		<div class="col-md-3"><h3>SOUTH AMERICA</h3></div>
		<div class="col-md-3"><h3>EUROPE</h3></div>
		<div class="col-md-3"><h3>MIDDLE EAST</h3></div>
		<div class="col-md-3"><h3>ASIA</h3></div>
	</div>

	<div class="offices row">
		<div class="col-md-3 item first">
			<h4>Shurtape Perú S.A.</h4>
			Calle  Minería 141- Santa Anita<br />
			43 Lima, Perú<br />
			Phone: +51 (1) 416-3400<br />
			Fax: +51 (1) 362-7472<br />
			Email: ventas@shurtapeperu.com<br />
			www.shurtapeperu.com
		</div>
		<div class="col-md-3 item">
			<h4>Kip GmbH</h4>
			Schlavenhorst 9 (Industriepark)<br />
			D-46395 Bocholt, Germany<br />
			Phone: +49 2871-23466-27<br />
			Fax: +49 2871-23466-32<br />
			Email: export@kip-tape.com<br />
			www.kip-tape.com
		</div>
		<div class="col-md-3 item">
			<h4>Shurtape Ved Ltd.</h4>
			PO Box 61397<br />
			Dubai, United Arab Emirates<br />
			Phone: +971 (4) 883-5189<br />
			Fax: +971 (4) 887-6277<br />
			Email: shurtape@emirates.net.ae
		</div>
		<div class="col-md-3 item last">
			<h4>Shurtape Technologies (Changshu) Co. Ltd.</h4>
			No.301 Yintong Road<br />
			Changshu Southeast Economic Development Zone<br />
			Changshu, 215500<br />
			Jiangsu, China<br />
			tel: +86 (512) 5235-9500<br />
			fax: +86 (512) 5235-9599<br />
			info@shurtape.com.cn
		</div>
	</div>
	 
	<h2>Distribution Centers</h2>
	<div class="blue_bar row">
		<div class="col-md-9"><h3>UNITED STATES</h3></div>
		<div class="col-md-3"><h3>CANADA</h3></div>
	</div>
	<div class="offices row">
		<div class="col-md-3 item first">
			<h4>North Carolina</h4>
			3389 Catawba Industrial Place<br />
			Catawba, NC 28609 USA<br />
			Phone: (828) 322-2700 
		</div>
		<div class="col-md-3 item">
			<h4>Arizona</h4>
			First Commerce Center<br />
			725 N 73rd Avenue, Suite 112<br />
			Phoenix, AZ 85043 USA<br />
			Phone: (623) 463-5886<br />
			Toll Free: (800) 342-8273<br />
			Fax: (623) 463-5809
		</div>
		<div class="col-md-3 item">
			<h4>Oklahoma</h4>
			4800 Partnership Drive<br />
			Oklahoma City, OK 73131<br />
			Phone: (405) 475-0202<br />
			Fax: (405) 475-0211
		</div>
		<div class="col-md-3 item last">
			<h4>Ontario</h4>
			6685 Kennedy Rd. Suite 3<br />
			Mississauga, ON<br />
			Canada L5T 3A5<br />
			Phone: (905) 696-0800<br />
			Fax: (905) 696-8176
		</div>
	</div>
</div>