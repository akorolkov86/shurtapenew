<p>
	Working at Shurtape is about more than just making the world's best tape. It's also about how great people, good ideas and pride in a job well done make our friends and our competitors sit up and take notice. We're about keeping our promises and building relationships that last – with our customers and with our work force. We ask for the best from our employees and offer our best in return, by providing an exceptional work environment and a thoughtful, competitive compensation package. Interested?
</p>

<h3>Global Locations</h3>
<p>
	We have approximately 1,500 employees in 14 facilities across the United States, Canada, Germany, United Kingdom, Mexico, Peru, United Arab Emirates and China.
</p>

<h3>OUTLOOK</h3>  
<p>
	At Shurtape we insist on being the best. We have the best tapes in the world, state-of-the-art manufacturing and distribution facilities, the latest equipment, innovative thinking and strong relationships with suppliers and customers. We know that all of these strengths are possible only through the performance of Shurtape's employees – employees who make the world's best tape.
</p>
<p>
	So at Shurtape, we know that being the world's best means making our company a great place to work. We've successfully accomplished this task in a number of ways. First, we hire the best people. We hire people who are capable, who have a passion to learn and imagine possibilities, who like teamwork, and who are willing to believe that they are the best in the business. Second, we work hard to provide compensation and benefits that are competitive within the industry and within the communities where we have facilities. Third, we've built a collaborative and supportive work environment through respect for and loyalty to each other and to our customers and suppliers.  In other words, we stick together!
</p>
<h3>Mission &amp; Core Beliefs</h3>
<p>
	Shurtape’s mission is to build brands that are among the most highly respected by consumers, partners and trade buyers by becoming the quality, innovation and marketing leader in the product categories that we offer.
</p>
<p>
	Our core beliefs are:
	<ul>
		<li>Being reliable</li>
		<li>Being customer focused</li>
		<li>Being solutions oriented</li>
		<li>Being progressive</li>
		<li>Being fact based and data driven</li>
		<li>Being trustworthy</li>
		<li>Being flexible</li>
		<li>Being creative and imaginative</li>
		<li>Being ethical in everything we do</li>
	</ul>
</p>

<h3>BENEFITS</h3> 

<h3>Why Work at Shurtape?</h3>
<p>
	Unique Culture – We have a unique culture.  Every piece of tape, every design, every application is important.  Every “how may I help you?” is driven from a passion for excellence.  We care about our products, our people and our brand.  We are constantly using our imagination to think of ways to make ordinary products extraordinary!  Would you like to be a part of this culture?
</p>
<p>
	Benefits &amp; Rewards – Your compensation is much more than just your pay. We offer competitive wages and bonuses as well as health, welfare and education benefits that help our employees effectively manage the challenges of their jobs and their lives.  We call these benefits ShurCare – because we care about our number one resource: our people!
</p>

<h4>Pay</h4>
<p>
	Competitive pay? Yes, we provide that at Shurtape. But there's more to it than that. We share our business successes with all employees.  We do this by offering above average bonus opportunities for everyone.  We also provide a 401(k) retirement program. In addition to these benefits, we also provide paid vacation and holidays. 
</p>

<h4>ShurHealth</h4>
<p>
	We want Shurtape employees to be healthy, so we call our progressive Health benefit program ShurHealth because it is good for you and good for business.  It also makes working here more fun and more fulfilling for everyone.
</p>
<p>
	We provide group insurance – health, hospitalization, life, dental, temporary disability and long term disability. We provide physical exams for employees and their covered dependents. Employees are also eligible for additional life and accident insurance.
</p>
<p>
	To promote a healthy work environment, we have nurses on staff to provide health care and advice. They treat work and non-work related illness and injuries, offer pre-screening assessments, manage an OSHA-regulated hearing conservation program, help you manage worker's compensation claims and work with physicians and insurance companies to advise and manage illnesses.
</p>
<p>
	We have also developed some other health benefits that help working families; for example, our Employee Assistance Program (EAP). The EAP allows for free, confidential counseling for marriage, finance, stress, alcohol and drug abuse, mental health, work performance and family life.
</p>

<h4>ShurHealth Wellness</h4>

<p>
	Our wellness program rewards employees for being healthy. The goal is to help our employees adopt and maintain a healthy lifestyle. ShurHealth Wellness activities include weight loss, nutrition, stress management, exercise, tobacco cessation, and health information. Our employees actively participate in ShurHealth Wellness by joining Shurtape walking groups, attending health and nutrition workshops, and participating in weight loss programs. ShurHealth Wellness promotes healthy behaviors and also helps lower health care costs.  Shurtape realizes that healthy life equals healthy business.
</p>

<p>
	To view open positions within the company or to apply online, please visit our <a href="#">Careers page</a>.
</p>
<p>
	Corporate Contact<br />
	Human Resources<br />
	PO Box 1530<br />
	Hickory, NC 28603<br />
	Phone Number: (855) 67-8369
</p>