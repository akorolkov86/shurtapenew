<p>HICKORY, N.C. (June 2013) – The key to HVAC installation is creating a permanent, airtight seal with no leaks. According to the Air Diffusion Council’s estimate, more than 600 million feet of insulated (all R-values), non-insulated, metallic and non-metallic flexible duct was produced in 2012. With so many feet in the market, it’s imperative that seals be done right...the first time.</p>

<p><img src="http://twshurtape.s3.amazonaws.com/imce/MakeEveryJobCode.png" width="308" height="218" style="float: left; padding-right: 20px; padding-bottom: 20px;" /></p>

<p>While many HVAC contractors rely upon cloth duct tape for their installs, film tapes, such as <a href="http://www.shurtape.com/tabid/79/default.aspx?ProductID=30&amp;DisplayType=1&amp;Level1=15&amp;Level2=18">DC 181 from Shurtape®</a>, are gaining traction thanks to their cost-effectiveness and flexibility. DC 181 is a film tape designed for sealing Class 1 Flex duct – and is UL 181B-FX Listed, helping to make every job code-complaint.</p>

<p>Today, building codes require the use of a tape that is UL 181B-FX Listed for sealing flex duct connections. Code compliance against UL 181B-FX requirements ensures the tape has the proper adhesion and extended-life shear strength to stay in place effectively for long periods of time. If the right tape isn’t used, the HVAC system will fail the building inspection.&nbsp;</p>

<p>DC 181 is designed for connecting, joining, sealing and patching flexible air ductwork. An aggressive, yet conformable tape solution, DC 181 is easy-to-unwind, hand-tearable, and features a printed BOPP film and water-based acrylic adhesive. It’s ideal for creating airtight, permanent and waterproof connections on Class 1 Flex duct to prevent air loss.&nbsp;</p>

<p>“The key here is air leakage,” said Roy Cox, market manager – contractor markets, Shurtape. “DC 181 allows you to seal connections and seams so that air doesn’t leak out of the system and ruin the system’s efficiency. Why install a high-efficiency heat pump if you’re going to lose all that energy through leaks in the ductwork?”</p>

<p>DC 181 is tested in accordance with UL 723 and is also a Shurtape Green Point Contributor Product – meaning it contributes to LEED®* certification points when combined with other eligible building materials.&nbsp;</p>

<p>For more information about Shurtape’s DC 181 film tape or other HVAC tapes, please visit www.shurtape.com.</p>